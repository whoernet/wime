# Find libidn library

# Idn_INCLUDE_DIRS
# Idn_LDFLAGS
# Idn_CFLAGS
# Idn_FOUND

if(WIN32)
  if(MSVC)
    message(FATAL_ERROR "We currently support only MinGW")
  endif()
endif()

find_package(PkgConfig)

pkg_search_module(Idn libidn)

# Fallback use direct search for headers and library
if(NOT Idn_FOUND)
  find_path(Idn_INCLUDE_DIRS
    stringprep.h
    PATHS
    ${IDN_ROOT}/include
  )  

  if(Idn_INCLUDE_DIRS)
    find_library(Idn_LDFLAGS NAMES idn PATHS ${IDN_ROOT}/lib)
    get_filename_component(Idn_LIBRARY_DIRS ${Idn_LDFLAGS} DIRECTORY)
  endif()

  if(Idn_INCLUDE_DIRS AND Idn_LDFLAGS)
    set(Idn_FOUND 1)
  endif()
endif()


if(Idn_FOUND)
  message(STATUS "Found the libidn library at ${Idn_LIBRARY_DIRS}")
  message(STATUS "Found the libidn headers at ${Idn_INCLUDE_DIRS}")
else()
  if(Idn_FIND_REQUIRED)
    message(FATAL_ERROR "Could NOT find required libidn library, aborting. Try to set IDN_ROOT.")
  else()
    message(STATUS "Could NOT find libidn")
  endif()
endif()
