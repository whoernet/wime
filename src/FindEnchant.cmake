# Find enchant library

# Enchant_INCLUDE_DIRS
# Enchant_LDFLAGS
# Enchant_CFLAGS
# Enchant_FOUND

if(WIN32)
  if(MSVC)
    message(FATAL_ERROR "We currently support only MinGW")
  endif()
endif()

find_package(PkgConfig)

pkg_search_module(Enchant enchant)

# Fallback use direct search for headers and library
if(NOT Enchant_FOUND)
  find_path(Enchant_INCLUDE_DIRS
    enchant.h
    PATHS
    ${ENCHANT_ROOT}/include/enchant
  )  

  if(Enchant_INCLUDE_DIRS)
    find_library(Enchant_LDFLAGS NAMES enchant PATHS ${ENCHANT_ROOT}/lib)
    get_filename_component(Enchant_LIBRARY_DIRS ${Enchant_LDFLAGS} DIRECTORY)
  endif()

  if(Enchant_INCLUDE_DIRS AND Enchant_LDFLAGS)
    set(Enchant_FOUND 1)
  endif()
endif()


if(Enchant_FOUND)
  message(STATUS "Found the enchant library at ${Enchant_LIBRARY_DIRS}")
  message(STATUS "Found the enchant headers at ${Enchant_INCLUDE_DIRS}")
else()
  if(Enchant_FIND_REQUIRED)
    message(FATAL_ERROR "Could NOT find required enchant library, aborting. Try to set ENCHANT_ROOT.")
  else()
    message(STATUS "Could NOT find enchant")
  endif()
endif()
