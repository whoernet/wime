/*
 * aboutdlg.cpp
 * Copyright (C) 2001-2003  Justin Karneges, Michail Pishchagin
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include <QTextStream>
#include <QFile>
#include <QtCrypto>

#include "applicationinfo.h"
#include "aboutdlg.h"
#include "iconset.h"

AboutDlg::AboutDlg(QWidget* parent)
	: QDialog(parent)
{
	setAttribute(Qt::WA_DeleteOnClose);
	ui_.setupUi(this);
	setWindowIcon(IconsetFactory::icon("psi/psiplus_logo").icon());

	setModal(false);

	QString version = QString("<style>"
							  "h3 {"
							  "margin-bottom: 0;"
						      "}"
							  "p {"
							  "margin-top: 0;"
						      "}"
						      "</style>"
							  "<h3>%1</h3><p>v%2 (%3)</p>")
					  .arg(ApplicationInfo::name())
					  .arg(ApplicationInfo::version())
					  .arg(QDate::fromString(ApplicationInfo::compilationDate(), Qt::ISODate).toString(Qt::DefaultLocaleLongDate));
	ui_.lb_name->setText(version);
	ui_.te_license->setText ( loadText(":/COPYING") );
}

QString AboutDlg::loadText( const QString & fileName )
{
	QString text;

	QFile f(fileName);
	if(f.open(QIODevice::ReadOnly)) {
		QTextStream t(&f);
		while(!t.atEnd())
			text += t.readLine() + '\n';
		f.close();
	}

	return text;
}


QString AboutDlg::details( QString name, QString email, QString jabber, QString www, QString desc )
{
	QString ret;
	const QString nbsp = "&nbsp;&nbsp;";
	ret += name + "<br>\n";
	if ( !email.isEmpty() )
		ret += nbsp + "E-mail: " + "<a href=\"mailto:" + email + "\">" + email + "</a><br>\n";
	if ( !jabber.isEmpty() )
		ret += nbsp + "XMPP: " + "<a href=\"xmpp:" + jabber + "?join\">" + jabber + "</a><br>\n";
	if ( !www.isEmpty() )
		ret += nbsp + "WWW: " + "<a href=\"" + www + "\">" + www + "</a><br>\n";
	if ( !desc.isEmpty() )
		ret += nbsp + desc + "<br>\n";
	ret += "<br>\n";

	return ret;
}

