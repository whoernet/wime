#include "edbsqlite.h"

#include <QSqlQuery>
#include <QSqlDatabase>

class EDBSqLite::SqlQuery : public QSqlQuery
{
public:
	SqlQuery();
	SqlQuery(QSqlDatabase db);
	bool exec();
	bool exec(const QString &query);
};

class EDBSqLite::QueryStorage
{
public:
	QueryStorage();
	~QueryStorage();
	EDBSqLite::SqlQuery *getPreparedQuery(QueryType type, bool allAccounts, bool allContacts);

private:
	QString getQueryString(QueryType type, bool allAccounts, bool allContacts);

private:
	QHash<QueryProperty, EDBSqLite::SqlQuery*> queryList;
};

