set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR})  # to find modules

set(CMAKE_AUTOMOC OFF)

if(QT5_BUILD)
  find_package(Qt5Core REQUIRED)
  find_package(Qt5Gui REQUIRED)
  find_package(Qt5Xml REQUIRED)
  find_package(Qt5Network REQUIRED)
  find_package(Qt5WebKit REQUIRED)
  find_package(Qt5WebKitWidgets REQUIRED)
  find_package(Qt5Concurrent REQUIRED)
  find_package(Qt5Multimedia REQUIRED)
  find_package(Qt5Sql REQUIRED)
  find_package(Qt5Svg REQUIRED)
  find_package(Qt5LinguistTools REQUIRED)
  find_package(Qt5Multimedia REQUIRED)
  find_package(Qca-qt5 REQUIRED)

  if(LINUX)
    find_package(Qt5DBus REQUIRED)
    find_package(Qt5X11Extras REQUIRED)
    find_package(XCB REQUIRED)

    include_directories(${LIBXCB_INCLUDE_DIR})
    add_definitions(${LIBXCB_DEFINITIONS})
  elseif(APPLE)
    find_package(Qt5MacExtras REQUIRED)
    find_package(Sparkle REQUIRED)
    include_directories(${SPARKLE_INCLUDE_DIR})
  endif()

  add_definitions(-DHAVE_QT5 ${LIBXCB_DEFINITIONS})

  macro(QT4_CREATE_TRANSLATION)
    QT5_CREATE_TRANSLATION(${ARGN})
  endmacro()

  macro(QT4_ADD_TRANSLATION)
    qt5_add_translation(${ARGN})
  endmacro()

  macro(qt4_add_resources)
    qt5_add_resources(${ARGN})
  endmacro()

  macro(qt4_wrap_ui)
    qt5_wrap_ui(${ARGN})
  endmacro()

  macro(qt4_generate_moc)
    qt5_generate_moc(${ARGN})
  endmacro()

  set(QT_LUPDATE_EXECUTABLE ${Qt5_LUPDATE_EXECUTABLE})
  set(QT_LIBRARIES Qt5::Core Qt5::Network Qt5::Xml Qt5::WebKit Qt5::WebKitWidgets Qt5::Concurrent Qt5::Multimedia Qt5::Svg Qt5::Sql)
  if(LINUX)
    list(APPEND QT_LIBRARIES Qt5::DBus Qt5::X11Extras ${LIBXCB_LIBRARIES})
  elseif(APPLE)
    list(APPEND QT_LIBRARIES Qt5::MacExtras)
  endif()
else()
  if(LINUX)
    set(QT_EXTRA_MODULES QtDBus)
  endif()
  find_package(Qt4 REQUIRED QtCore QtGui QtNetwork QtXml QtWebKit QtSql QtSvg ${QT_EXTRA_MODULES})
  unset(QT_EXTRA_MODULES)

  find_package(Qca REQUIRED)
  include(${QT_USE_FILE})
  find_package(QJSON REQUIRED)
  include_directories(${QJSON_INCLUDE_DIR})
endif()

if(LINUX)
  find_package(X11 REQUIRED)
  include_directories(${X11_INCLUDE_DIR})
endif()

if(LINUX)
  find_package(Enchant REQUIRED)
elseif(WIN32)
  find_package(Hunspell REQUIRED)
endif()

macro(qt_wrap_cpp)
  set(_SOURCES ${ARGN})
  list(REMOVE_AT _SOURCES 0)
  foreach(SOURCE ${_SOURCES})
    if(SOURCE MATCHES "^(.*/|)([^/.]+)\\.h$")
      set(DEST "moc_${CMAKE_MATCH_2}.cpp")
      qt4_generate_moc(${SOURCE} ${DEST} ${PROJECT_NAME})
      list(APPEND ${ARGV0} ${DEST})
    elseif(SOURCE MATCHES "^(.*/|)([^/.]+)\\.cpp$")
      set(DEST "${CMAKE_MATCH_2}.moc")
      qt4_generate_moc(${SOURCE} ${DEST} TARGET ${PROJECT_NAME})
      list(APPEND ${ARGV0} ${DEST})
    elseif(APPLE AND SOURCE MATCHES "^(.*/|)([^/.]+)\\.mm$")
      set(DEST "${CMAKE_MATCH_2}.moc")
      qt4_generate_moc(${SOURCE} ${DEST} TARGET ${PROJECT_NAME})
      list(APPEND ${ARGV0} ${DEST})
    else()
      message(WARNING "Wrong source ${SOURCE}")
    endif()
  endforeach()
  unset(_SOURCES)
endmacro()

include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_CURRENT_BINARY_DIR}
)

if(LINUX)
  if(LIB_INSTALL_DIR)
    set(PSI_LIBDIR "${LIB_INSTALL_DIR}/${PROJECT_NAME}")
  else()
    set(PSI_LIBDIR "${CMAKE_INSTALL_PREFIX}/lib${LIB_SUFFIX}/${PROJECT_NAME}")
  endif()

  if(SHARE_INSTALL_PREFIX)
    set(PSI_DATADIR "${SHARE_INSTALL_PREFIX}/${PROJECT_NAME}")
  else()
    set(PSI_DATADIR "${CMAKE_INSTALL_PREFIX}/share/${PROJECT_NAME}")
  endif()
endif()

add_custom_target(update_version ALL
  COMMAND ${CMAKE_COMMAND} -DPSI_LIBDIR="${PSI_LIBDIR}"
                           -DPSI_DATADIR="${PSI_DATADIR}"
                           -DAPP_VERSION="v${APP_VERSION}"
                           -DAPP_SOURCE_DIR="${CMAKE_SOURCE_DIR}"
                           -P "${CMAKE_SOURCE_DIR}/src/Version.cmake"
)

add_definitions(
  -DQT_STATICPLUGIN
  -DHAVE_CONFIG
)

if(WIN32)
  add_definitions(
    -DUNICODE
    -D_UNICODE
  )
endif()

include(src.cmake)

# Only headers or very small sources
include(irisprotocol/irisprotocol.cmake)
include(protocol/protocol.cmake)
include(plugins/plugins.cmake)

add_subdirectory(options)
add_subdirectory(tabs)
add_subdirectory(privacy)
add_subdirectory(capabilities)
add_subdirectory(Certificates)
add_subdirectory(avcall)
add_subdirectory(psimedia)
add_subdirectory(contactmanager)
add_subdirectory(tools)
add_subdirectory(libpsi/dialogs)
add_subdirectory(libpsi/tools)
add_subdirectory(widgets)
add_subdirectory(sxe)
add_subdirectory(whiteboarding)

set(TRANSLATIONS
  ../translations/psi_ar.ts
  ../translations/psi_be.ts
  ../translations/psi_bg.ts
  ../translations/psi_ca.ts
  ../translations/psi_cs.ts
  ../translations/psi_de.ts
  ../translations/psi_en.ts
  ../translations/psi_eo.ts
  ../translations/psi_es.ts
  ../translations/psi_et.ts
  ../translations/psi_fa.ts
  ../translations/psi_fi.ts
  ../translations/psi_fr.ts
  ../translations/psi_he.ts
  ../translations/psi_hu.ts
  ../translations/psi_it.ts
  ../translations/psi_ja.ts
  ../translations/psi_kk.ts
  ../translations/psi_mk.ts
  ../translations/psi_nl.ts
  ../translations/psi_pl.ts
  ../translations/psi_pt_BR.ts
  ../translations/psi_pt.ts
  ../translations/psi_ru.ts
  ../translations/psi_sk.ts
  ../translations/psi_sl.ts
  ../translations/psi_sr@latin.ts
  ../translations/psi_sv.ts
  ../translations/psi_sw.ts
  ../translations/psi_uk.ts
  ../translations/psi_ur_PK.ts
  ../translations/psi_vi.ts
  ../translations/psi_zh_CN.ts
  ../translations/psi_zh_TW.ts
)

set_property(SOURCE ${TRANSLATIONS} PROPERTY OUTPUT_LOCATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/translations)

add_custom_target(lupdate)
foreach(_LANG ${TRANSLATIONS})
  get_filename_component(_SHORT_LANG ${_LANG} NAME_WE)
  string(REPLACE "@" "_" _SHORT_LANG ${_SHORT_LANG})
  add_custom_target(${_SHORT_LANG}
    COMMAND ${QT_LUPDATE_EXECUTABLE} "-no-obsolete" "-recursive" "." "../iris" "-ts" ${_LANG}
    WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}" VERBATIM)
  add_dependencies(lupdate ${_SHORT_LANG})
endforeach()

QT4_ADD_TRANSLATION(QM ${TRANSLATIONS})

set(RESOURCES ../psi.qrc ../iconsets.qrc)
qt4_add_resources(QRC_SOURCES ${RESOURCES})

qt4_wrap_ui(UI_FORMS ${FORMS})
qt_wrap_cpp(MOC_SOURCES ${SOURCES} ${HEADERS})

if(WIN32)
  # resource compilation for MinGW
  add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/psi_win32.o
                     COMMAND ${CMAKE_RC_COMPILER} -I${CMAKE_CURRENT_SOURCE_DIR} -i${CMAKE_SOURCE_DIR}/win32/psi_win32.rc
                             -o ${CMAKE_CURRENT_BINARY_DIR}/psi_win32.o)
  set(RC_FILE ${CMAKE_CURRENT_BINARY_DIR}/psi_win32.o)
endif()

add_executable(${PROJECT_NAME} WIN32 MACOSX_BUNDLE ${SOURCES} ${HEADERS} ${MOC_SOURCES} ${UI_FORMS} ${PLAIN_SOURCES} ${PLAIN_HEADERS} ${QRC_SOURCES} ${QM} ${RC_FILE})

add_dependencies(${PROJECT_NAME} update_version)

if(NOT QT5_BUILD)
  target_link_libraries(${PROJECT_NAME} qjson)
endif()

if(APPLE)
  list(APPEND EXTRA_LDFLAGS ${SPARKLE_LIBRARY} "-framework Carbon" "-framework AppKit" "-framework IOKit" "-framework CoreAudio" "-framework AudioToolbox")
endif()

target_link_libraries(${PROJECT_NAME} ${QT_LIBRARIES} ${EXTRA_LDFLAGS} ${X11_LIBRARIES} ${Qca_LIBRARY} iris)
target_link_libraries(${PROJECT_NAME} options capabilities Certificates avcall psimedia contactmanager whiteboarding sxe tools libpsi_dialogs libpsi_tools widgets privacy tabs logs)

if(LINUX)
  install(TARGETS ${PROJECT_NAME} DESTINATION bin)
endif()

add_subdirectory(plugins)

if(WIN32)
  # Get Qt installation path
  string(REGEX REPLACE "([^ ]+)[/\\].*" "\\1" QT_BIN_DIR_TMP "${QT_MOC_EXECUTABLE}")
  string(REGEX REPLACE "\\\\" "/" QT_BIN_DIR "${QT_BIN_DIR_TMP}")
  unset(QT_BIN_DIR_TMP)

  get_filename_component(QT_DIR ${QT_BIN_DIR} DIRECTORY)
  set(QT_PLUGINS_DIR ${QT_DIR}/plugins)
  set(QT_TRANSLATIONS_DIR ${QT_DIR}/translations)

  find_program(ZIP_PATH zip.exe)
  get_filename_component(OTHER_DIR ${ZIP_PATH} DIRECTORY)

  if(QT5_BUILD)
    # required libraries
    copy("${QT_BIN_DIR}/icudt54.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/icuin54.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/icuuc54.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)

    # Qt5 libraries
    copy("${QT_BIN_DIR}/Qt5Core${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Gui${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Widgets${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Svg${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Network${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Svg${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Script${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Xml${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5XmlPatterns${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Sql${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5WebKit${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5WebKitWidgets${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Qml${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Quick${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Positioning${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5WebChannel${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Multimedia${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5MultimediaWidgets${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Concurrent${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5Sensors${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5OpenGL${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/Qt5PrintSupport${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)

    # platforms
    copy("${QT_PLUGINS_DIR}/platforms/qminimal${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/platforms/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/platforms/qoffscreen${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/platforms/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/platforms/qwindows${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/platforms/" prepare-bin-libs)

    # bearer
    copy("${QT_PLUGINS_DIR}/bearer/qgenericbearer${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/bearer/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/bearer/qnativewifibearer${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/bearer/" prepare-bin-libs)

    # imageformats
    copy("${QT_PLUGINS_DIR}/imageformats/qdds${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qgif${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qicns${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qico${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qjp2${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qjpeg${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qmng${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qsvg${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qtga${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qtiff${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qwbmp${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qwebp${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)

    # sqldrivers
    copy("${QT_PLUGINS_DIR}/sqldrivers/qsqlite${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/sqldrivers/" prepare-bin-libs)

    # Qt translations
    file(GLOB QT_TRANSLATIONS "${QT_TRANSLATIONS_DIR}/qt_*.qm")
    foreach(FILE ${QT_TRANSLATIONS})
      if(NOT FILE MATCHES "_help_")
        copy(${FILE} "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/translations/" prepare-bin-libs)
      endif()
    endforeach()

    file(GLOB QT_TRANSLATIONS "${QT_TRANSLATIONS_DIR}/qtbase_*.qm")
    foreach(FILE ${QT_TRANSLATIONS})
      copy(${FILE} "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/translations/" prepare-bin-libs)
    endforeach()

    get_filename_component(OTHER_DIR ${ZIP_PATH} DIRECTORY)
    copy("${OTHER_DIR}/libqca-qt5${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  else()
    # Qt4 libs
    copy("${QT_BIN_DIR}/QtCore${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/QtGui${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/QtNetwork${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/QtSvg${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/QtXml${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/QtSql${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${QT_BIN_DIR}/QtWebKit${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)

    # bearer
    copy("${QT_PLUGINS_DIR}/bearer/qgenericbearer${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/bearer/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/bearer/qnativewifibearer${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/bearer/" prepare-bin-libs)

    # plugins
    copy("${QT_PLUGINS_DIR}/imageformats/qgif${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qico${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qjpeg${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qmng${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qtga4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qtiff${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)
    copy("${QT_PLUGINS_DIR}/imageformats/qsvg${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/imageformats/" prepare-bin-libs)

    # sqldrivers
    copy("${QT_PLUGINS_DIR}/sqldrivers/qsqlite${D}4.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/sqldrivers/" prepare-bin-libs)

    # Qt translations
    file(GLOB QT_TRANSLATIONS "${QT_TRANSLATIONS_DIR}/qt_*.qm")
    foreach(FILE ${QT_TRANSLATIONS})
      if(NOT FILE MATCHES "_help_")
        copy(${FILE} "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/translations/" prepare-bin-libs)
      endif()
    endforeach()

    copy("${OTHER_DIR}/libqca${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
    copy("${OTHER_DIR}/libqjson${D}.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  endif()

  copy("${QT_BIN_DIR}/libwinpthread-1.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy("${QT_BIN_DIR}/libgcc_s_dw2-1.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy("${QT_BIN_DIR}/libstdc++-6.dll" "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)

  # psimedia
  find_program(PSIMEDIA_PATH libgstprovider${D}.dll)
  copy(${PSIMEDIA_PATH} "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)

  # psimedia deps
  find_program(PSIMEDIA_DEPS_PATH libgstvideo-0.10-0.dll)
  get_filename_component(PSIMEDIA_DEPS_DIR ${PSIMEDIA_DEPS_PATH} DIRECTORY)
  copy(${PSIMEDIA_DEPS_DIR}/libjpeg-9.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgettextlib-0-19-6.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libogg-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libtheoradec-1.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgettextpo-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/liborc-0.4-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libtheoraenc-1.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libasprintf-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgettextsrc-0-19-6.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/liborc-test-0.4-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libvorbis-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libcharset-1.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgio-2.0-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libspeex-1.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libvorbisenc-2.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libglib-2.0-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgthread-2.0-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libspeexdsp-1.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libvorbisfile-3.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libffi-6.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgmodule-2.0-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgobject-2.0-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libintl-8.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libtheora-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstapp-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstaudio-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstbase-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstcdda-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstcontroller-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstdataprotocol-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstfft-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstinterfaces-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstnet-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstnetbuffer-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstpbutils-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstreamer-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstriff-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstrtp-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstrtsp-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstsdp-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgsttag-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${PSIMEDIA_DEPS_DIR}/libgstvideo-0.10-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)

  # streamer plugins
  get_filename_component(GSTREAMER_PLUGINS_DIR ${PSIMEDIA_DEPS_DIR} DIRECTORY)
  set(GSTREAMER_PLUGINS_DIR "${GSTREAMER_PLUGINS_DIR}/lib/gstreamer-0.10")
  file(GLOB GSTREAMER_PLUGINS "${GSTREAMER_PLUGINS_DIR}/*.dll")
  foreach(FILE ${GSTREAMER_PLUGINS})
    copy(${FILE} "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/gstreamer-0.10/" prepare-bin-libs)
  endforeach()

  # other libs and executables
  copy(${OTHER_DIR}/zip.exe "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/libgcrypt-20.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/libotr.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/gpg.exe "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/zlib1.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/libgpg-error-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/libqjdns${D}.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/libjdns${D}.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/libidn-11.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/libiconv-2.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/libintl-8.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/libhunspell-1.4-0.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/libeay32.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)
  copy(${OTHER_DIR}/ssleay32.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/" prepare-bin-libs)

  # qca plugins
  get_filename_component(QCA_PLUGINS_DIR ${OTHER_DIR} DIRECTORY)
  set(QCA_PLUGINS_DIR "${QCA_PLUGINS_DIR}/lib/qca/crypto")
  copy(${QCA_PLUGINS_DIR}/libqca-ossl${D}.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/crypto/" prepare-bin-libs)
  copy(${QCA_PLUGINS_DIR}/libqca-gnupg${D}.dll "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/crypto/" prepare-bin-libs)
endif()

if(APPLE)
  find_program(GPG_PATH gpg)
  if(NOT GPG_PATH)
    message(FATAL_ERROR "Couldn't find gpg")
  endif()
  message(STATUS "gpg found at ${GPG_PATH}")

  install(PROGRAMS ${GPG_PATH}
    DESTINATION "${BUNDLE_DIR_BASE_NAME}.app/Contents/MacOS"
    COMPONENT Runtime
  )

  install(TARGETS ${PROJECT_NAME} BUNDLE DESTINATION . COMPONENT Runtime)

  set_target_properties(${PROJECT_NAME} PROPERTIES
    OUTPUT_NAME "${BUNDLE_DIR_BASE_NAME}"
  )
  configure_file("${CMAKE_SOURCE_DIR}/mac/Info.plist.in" "${CMAKE_BINARY_DIR}/Info.plist.in")

  # Install needed Qt plugins by copying each pluging separately
  find_package(Qt5PrintSupport REQUIRED)

  set(PLATFORM_PLUGINS Qt5::QCocoaIntegrationPlugin Qt5::QMinimalIntegrationPlugin Qt5::QOffscreenIntegrationPlugin)
  set(IMAGE_PLUGINS
    Qt5::QICNSPlugin
    Qt5::QGifPlugin
    Qt5::QICOPlugin
    Qt5::QJp2Plugin
    Qt5::QJpegPlugin
    Qt5::QMngPlugin
    Qt5::QTgaPlugin
    Qt5::QTiffPlugin
    Qt5::QWbmpPlugin
    Qt5::QWebpPlugin
    Qt5::QSvgPlugin
  )

  set(AUDIO_PLUGINS Qt5::CoreAudioPlugin)
  set(PRINTSUPPORT_PLUGINS Qt5::QCocoaPrinterSupportPlugin)
  set(SQL_PLUGINS Qt5::QSQLiteDriverPlugin)
  set(ICONENGINES_PLUGINS Qt5::QSvgIconPlugin)
  set(PLUGINS_DEST_PATH "${BUNDLE_DIR_BASE_NAME}.app/Contents/PlugIns")
  set(GST_PLUGINS_DEST_PATH "${BUNDLE_DIR_BASE_NAME}.app/Contents/PlugIns/gstreamer")
  set(WIME_PLUGINS_DEST_PATH "${BUNDLE_DIR_BASE_NAME}.app/Contents/Resources/plugins")

  foreach(plugin ${PLATFORM_PLUGINS})
    get_target_property(_loc ${plugin} LOCATION)
    install(FILES "${_loc}" DESTINATION ${PLUGINS_DEST_PATH}/platforms/ COMPONENT Runtime)
  endforeach()

  foreach(plugin ${IMAGE_PLUGINS})
    get_target_property(_loc ${plugin} LOCATION)
    install(FILES "${_loc}" DESTINATION ${PLUGINS_DEST_PATH}/imageformats/ COMPONENT Runtime)
  endforeach()

  foreach(plugin ${AUDIO_PLUGINS})
    get_target_property(_loc ${plugin} LOCATION)
    install(FILES "${_loc}" DESTINATION ${PLUGINS_DEST_PATH}/audio/ COMPONENT Runtime)
  endforeach()

  foreach(plugin ${PRINTSUPPORT_PLUGINS})
    get_target_property(_loc ${plugin} LOCATION)
    install(FILES "${_loc}" DESTINATION ${PLUGINS_DEST_PATH}/printsupport/ COMPONENT Runtime)
  endforeach()

  foreach(plugin ${SQL_PLUGINS})
    get_target_property(_loc ${plugin} LOCATION)
    install(FILES "${_loc}" DESTINATION ${PLUGINS_DEST_PATH}/sqldrivers/ COMPONENT Runtime)
  endforeach()


  foreach(plugin ${ICONENGINES_PLUGINS})
    get_target_property(_loc ${plugin} LOCATION)
    install(FILES "${_loc}" DESTINATION ${PLUGINS_DEST_PATH}/iconengines/ COMPONENT Runtime)
  endforeach()

  # Install public key for Sparkle Updater
  # install(FILES "${CMAKE_SOURCE_DIR}/dsa_pub.pem" DESTINATION "${BUNDLE_DIR_BASE_NAME}.app/Contents/Resources" COMPONENT Runtime)

  # Now the work of copying dependencies into the bundle/package
  # The quotes are escaped and variables to use at install time have their $ escaped
  # An alternative is the do a configure_file() on a script and use install(SCRIPT  ...).
  # Note that the image plugins depend on QtSvg and QtXml, and it got those copied
  # over.
  get_target_property(QT_LIBRARY_DIR Qt5::Core LOCATION)
  while(NOT ${QT_LIBRARY_DIR} MATCHES "lib$")
    get_filename_component(QT_LIBRARY_DIR ${QT_LIBRARY_DIR} DIRECTORY
  )
  endwhile()
  set(APPS "\${CMAKE_INSTALL_PREFIX}/${BUNDLE_DIR_BASE_NAME}.app")
  install(CODE "
    set(GPG_PATH \"${APPS}/Contents/MacOS/gpg\")
    file(GLOB_RECURSE QTPLUGINS
      \"\${CMAKE_INSTALL_PREFIX}/${PLUGINS_DEST_PATH}/*${CMAKE_SHARED_LIBRARY_SUFFIX}\")
    file(GLOB_RECURSE GSTREAMERPLUGINS
      \"\${CMAKE_INSTALL_PREFIX}/${GST_PLUGINS_DEST_PATH}/*.so\")
    file(GLOB_RECURSE WIMEPLUGINS
      \"\${CMAKE_INSTALL_PREFIX}/${WIME_PLUGINS_DEST_PATH}/*.so\")
    include(BundleUtilities)
    fixup_bundle(\"${APPS}\" \"\${GPG_PATH};\${QTPLUGINS};\${GSTREAMERPLUGINS};\${WIMEPLUGINS}\" \"${QT_LIBRARY_DIR}\")
    file(GLOB BINARIES \"${APPS}/Contents/Frameworks/*.framework\"
                       \"${APPS}/Contents/MacOS/*.dylib\"
                       \"${APPS}/Contents/PlugIns/*.dylib\"
                       \"${APPS}/Contents/PlugIns/*/*.dylib\")
    execute_process(COMMAND codesign --deep -f -s \"Wime\" \${BINARIES} \"${APPS}\" WORKING_DIRECTORY \"${APPS}\")
    " COMPONENT Runtime
  )

  # To Create a package, one can run "cpack -G DragNDrop CPackConfig.cmake" on Mac OS X
  # where CPackConfig.cmake is created by including CPack
  # And then there's ways to customize this as well
  set(CPACK_GENERATOR "DragNDrop;Bundle")
  set(CPACK_PROJECT_CONFIG_FILE "${CMAKE_BINARY_DIR}/ProjectConfig.cmake")
  set(CPACK_DMG_VOLUME_NAME ${PROJECT_NAME})
  set(CPACK_DMG_DS_STORE_SETUP_SCRIPT "${CMAKE_SOURCE_DIR}/mac/CMakeDMGSetup.scpt")
  set(CPACK_DMG_BACKGROUND_IMAGE "${CMAKE_SOURCE_DIR}/mac/DS_Background.png")

  set(CPACK_BUNDLE_NAME "Portable ${PROJECT_NAME}")
  set(CPACK_BUNDLE_ICON "${CMAKE_SOURCE_DIR}/mac/application.icns")
  set(CPACK_BUNDLE_PLIST "${CMAKE_SOURCE_DIR}/mac/PortableInfo.plist")
  set(CPACK_BUNDLE_STARTUP_COMMAND "${CMAKE_SOURCE_DIR}/mac/Portable Wime")

  include(CPack)

  add_custom_target("dmg"
    COMMAND ${CMAKE_COMMAND} -DAPP_NAME=${PROJECT_NAME} -DAPP_VERSION=${APP_VERSION} -P "${CMAKE_SOURCE_DIR}/mac/Dmg.cmake"
    DEPENDS "${PROJECT_NAME}"
    WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
  )
endif()
