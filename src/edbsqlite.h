#pragma once

#include "eventdb.h"

#include <QSqlQuery>
#include <QSqlDatabase>
#include <QSqlRecord>

class EDBSqLite : public EDB
{
	Q_OBJECT

public:
	enum InsertMode {Normal, Import};

	EDBSqLite(PsiCon *psi);
	~EDBSqLite();

	int get(const QString &accId, const XMPP::Jid &jid, const QDateTime date, int direction, int start, int len);
	int find(const QString &accId, const QString &str, const XMPP::Jid &jid, const QDateTime date, int direction);
	int append(const QString &accId, const XMPP::Jid &jid, const PsiEvent::Ptr &e, int type);
	int erase(const QString &accId, const XMPP::Jid &jid);
	QList<ContactItem> contacts(const QString &accId, int type);
	quint64 eventsCount(const QString &accId, const XMPP::Jid &jid);
	QString getStorageParam(const QString &key);
	void setStorageParam(const QString &key, const QString &val);

	void setInsertingMode(InsertMode mode);
	void setMirror(EDBFlatFile *mirr);
	EDBFlatFile *mirror() const;

private:
	class QueryStorage;
	class SqlQuery;

	struct item_query_req
	{
		QString accId;
		XMPP::Jid j;
		int jidType;
		int type; // 0 = latest, 1 = oldest, 2 = random, 3 = write
		int start;
		int len;
		int dir;
		int id;
		QDateTime date;
		QString findStr;
		PsiEvent::Ptr event;

		enum Type {
			Type_get,
			Type_append,
			Type_find,
			Type_erase
		};
	};
	bool commited;
	unsigned int transactionsCounter;
	QDateTime lastCommitTime;
	unsigned int maxUncommitedRecs;
	int maxUncommitedSecs;
	unsigned int commitByTimeoutSecs;
	QTimer *commitTimer;
	EDBFlatFile *mirror_;
	QList<item_query_req*> rlist;
	QHash<QString, qint64>jidsCache;
	QueryStorage *queryes;

private:
	bool appendEvent(const QString &accId, const XMPP::Jid &, const PsiEvent::Ptr &, int);
	PsiEvent::Ptr getEvent(const QSqlRecord &record);
	qint64 ensureJidRowId(const QString &accId, const XMPP::Jid &jid, int type);
	int  rowCount(const QString &accId, const XMPP::Jid &jid, const QDateTime before);
	bool eraseHistory(const QString &accId, const XMPP::Jid &);
	bool transaction(bool now);
	bool rollback();
	bool importExecute();

private slots:
	void performRequests();
	bool commit();

};
