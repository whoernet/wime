/*
 * chatview_webkit.cpp - Webkit based chatview
 * Copyright (C) 2010  Rion
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "chatview_webkit.h"

#include <QObject>

class ChatViewJSObject : public QObject
{
	Q_OBJECT

public:
	ChatViewJSObject(ChatView *view);

public slots:
	QString mucNickColor(const QString &nick, bool isSelf, const QStringList &validList = QStringList()) const;
	bool isMuc() const;
	QString chatName() const;
	QString jid() const;
	QString account() const;
	void signalInited();
	void signalAuth();
	void signalDeny();
	void signalQuote(const QString &text);
	QString getFont() const;
	void setTransparent();
	
signals:
	void inited();
	void doAuth();
	void doDeny();
	void doQuote(const QString &text);

private:
	ChatView *_view;
};
