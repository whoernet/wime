/*
 * eventdb.h - asynchronous I/O event database
 * Copyright (C) 2001, 2002  Justin Karneges
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#ifndef EVENTDB_H
#define EVENTDB_H

#include "psicon.h"
#include "psievent.h"
#include "xmpp_jid.h"

#include <QDateTime>
#include <QFile>
#include <QHash>
#include <QObject>
#include <QSharedPointer>
#include <QTimer>
#include <QVariant>

struct item_file_req
{
	Jid j;
	int type; // 0 = latest, 1 = oldest, 2 = random, 3 = write
	int start;
	int len;
	int dir;
	int id;
	QDateTime date;
	QString findStr;
	PsiEvent::Ptr event;

	enum Type {
		Type_get,
		Type_append,
		Type_find,
		Type_erase
	};
};

class EDBItem
{
public:
	EDBItem(const PsiEvent::Ptr &, const QString &id);
	~EDBItem();

	PsiEvent::Ptr event() const;
	const QString & id() const;

private:
	QString v_id;
	PsiEvent::Ptr e;
};

typedef QSharedPointer<EDBItem> EDBItemPtr;
typedef QList<EDBItemPtr> EDBResult;

class EDB;
class EDBHandle : public QObject
{
	Q_OBJECT
public:
	enum { Read, Write, Erase };
	EDBHandle(EDB *);
	~EDBHandle();

	// operations
	void get(const QString &accId, const XMPP::Jid &jid, const QDateTime date, int direction, int begin, int len);
	void find(const QString &accId, const QString &, const XMPP::Jid &, const QDateTime date, int direction);
	void append(const QString &accId, const XMPP::Jid &, const PsiEvent::Ptr &, int);
	void erase(const QString &accId, const XMPP::Jid &);

	bool busy() const;
	const EDBResult result() const;
	bool writeSuccess() const;
	int lastRequestType() const;
	int beginRow() const;

signals:
	void finished();

private:
	class Private;
	Private *d;

	friend class EDB;
	void edb_resultReady(EDBResult);
	void edb_writeFinished(bool);
	int listeningFor() const;
};

class EDB : public QObject
{
	Q_OBJECT
public:
	enum { Forward, Backward };
	enum { Contact = 1, GroupChatContact = 2 };
	struct ContactItem
	{
		QString   accId;
		XMPP::Jid jid;
		ContactItem(const QString &aId, XMPP::Jid j) { accId = aId; jid = j; }
	};

	EDB(PsiCon *psi);
	virtual ~EDB()=0;
	virtual QList<ContactItem> contacts(const QString &accId, int type) = 0;
	virtual quint64 eventsCount(const QString &accId, const XMPP::Jid &jid) = 0;
	virtual QString getStorageParam(const QString &key) = 0;
	virtual void setStorageParam(const QString &key, const QString &val) = 0;

protected:
	int genUniqueId() const;
	virtual int get(const QString &accId, const XMPP::Jid &jid, const QDateTime date, int direction, int start, int len)=0;
	virtual int append(const QString &accId, const XMPP::Jid &, const PsiEvent::Ptr &, int)=0;
	virtual int find(const QString &accId, const QString &, const XMPP::Jid &, const QDateTime date, int direction)=0;
	virtual int erase(const QString &accId, const XMPP::Jid &)=0;
	void resultReady(int, EDBResult, int);
	void writeFinished(int, bool);
	PsiCon *psi();

private:
	class Private;
	Private *d;

	friend class EDBHandle;
	void reg(EDBHandle *);
	void unreg(EDBHandle *);

	int op_get(const QString &accId, const XMPP::Jid &, const QDateTime date, int direction, int start, int len);
	int op_find(const QString &accId, const QString &, const XMPP::Jid &, const QDateTime date, int direction);
	int op_append(const QString &accId, const XMPP::Jid &, const PsiEvent::Ptr &, int);
	int op_erase(const QString &accId, const XMPP::Jid &);
};

class EDBFlatFile : public EDB
{
	Q_OBJECT
public:
	EDBFlatFile(PsiCon *psi);
	~EDBFlatFile();

	int get(const QString &accId, const XMPP::Jid &jid, const QDateTime date, int direction, int start, int len);
	int find(const QString &accId, const QString &, const XMPP::Jid &, const QDateTime date, int direction);
	int append(const QString &accId, const XMPP::Jid &, const PsiEvent::Ptr &, int);
	int erase(const QString &accId, const XMPP::Jid &);
	QList<ContactItem> contacts(const QString &accId, int type);
	quint64 eventsCount(const QString &accId, const XMPP::Jid &jid);
	QString getStorageParam(const QString &) {return QString();}
	void setStorageParam(const QString &, const QString &) {}

	class File;

private slots:
	void performRequests();
	void file_timeout();

private:
	class Private;
	Private *d;

	File *findFile(const XMPP::Jid &) const;
	File *ensureFile(const XMPP::Jid &);
	bool deleteFile(const XMPP::Jid &);
};

class EDBFlatFile::File : public QObject
{
	Q_OBJECT
public:
	File(const XMPP::Jid &_j);
	~File();

	int total() const;
	int getId(QDateTime &date, int dir, int offset);
	void touch();
	PsiEvent::Ptr get(int);
	bool append(const PsiEvent::Ptr &);

	static QString jidToFileName(const XMPP::Jid &);
	static QString strToFileName(const QString &);
	static QList<EDB::ContactItem> contacts(int type);

signals:
	void timeout();

private slots:
	void timer_timeout();

public:
	XMPP::Jid j;
	QString fname;
	QFile f;
	bool valid;
	QTimer *t;

	class Private;
	Private *d;

private:
	PsiEvent::Ptr lineToEvent(const QString &);
	QString eventToLine(const PsiEvent::Ptr&);
	void ensureIndex();
	int findIdByNearDate(QDateTime &date, int start, int end);
	QString getLine(int);
	QDateTime getLineDate(const QString &line) const;
};

enum QueryType {
	QueryContactsList,
	QueryLatest, QueryOldest,
	QueryDateForward, QueryDateBackward,
	QueryFindText,
	QueryRowCount, QueryRowCountBefore,
	QueryJidRowId,
	QueryInsertEvent
};

struct QueryProperty
{
	QueryType type;
	bool      allAccounts;
	bool      allContacts;
	QueryProperty(QueryType tp, bool allAcc, bool allCont) {
		type        = tp;
		allAccounts = allAcc;
		allContacts = allCont;
	}
	bool operator==(const QueryProperty &other) const {
		return (type        == other.type &&
				allAccounts == other.allAccounts &&
				allContacts == other.allContacts);
	}
};
uint qHash(const QueryProperty &struc);

#endif
