/*
 * anglebutton.cpp
 * Copyright (C) 2016 Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "anglebutton.h"

#include <QStylePainter>
#include <QStyleOption>
#include <QTimer>
#include <QCursor>
#include <QIcon>

AngleButton::AngleButton(QWidget *parent)
    : QWidget(parent)
    , _direction(AngleButton::Left)
{
	setFixedSize(10, 10);
}

void AngleButton::setDirection(AngleButton::Direction direction)
{
	_direction = direction;
	update();
}

AngleButton::Direction AngleButton::direction() const
{
	return _direction;
}

void AngleButton::paintEvent(QPaintEvent *event)
{
	QWidget::paintEvent(event);

	QStyleOption options;
	options.init(this);

	QPoint point = mapFromGlobal(QCursor::pos());
	if (rect().contains(point))
		options.state |= QStyle::State_MouseOver;
	else
		options.state &= ~QStyle::State_MouseOver;

    QStylePainter p(this);

	QStyle::PrimitiveElement element;
	switch (_direction) {
	case Left: element = QStyle::PE_IndicatorArrowLeft; break;
	case Right: element = QStyle::PE_IndicatorArrowRight; break;
	case Up: element = QStyle::PE_IndicatorArrowUp; break;
	case Down: element = QStyle::PE_IndicatorArrowDown; break;
	}

	p.drawPrimitive(element, options);
}

void AngleButton::mouseReleaseEvent(QMouseEvent *event)
{
	QWidget::mouseReleaseEvent(event);
	emit clicked();
	QTimer::singleShot(0, this, SLOT(update()));
}

void AngleButton::enterEvent(QEvent *event)
{
	QWidget::enterEvent(event);
	update();
}

void AngleButton::leaveEvent(QEvent *event)
{
	QWidget::leaveEvent(event);
	update();
}
