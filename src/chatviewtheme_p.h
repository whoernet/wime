/*
 * chatviewtheme.cpp - theme for webkit based chatview
 * Copyright (C) 2010 Rion (Sergey Ilinyh)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "chatviewtheme.h"

#include "webview.h"

#include <QStringList>
#include <QPointer>
#include <QObject>
#include <QVariantMap>

class ChatViewTheme::Private
{
public:
	QString html;
	QString jsNamespace;
	QString adapterPath;
	QStringList scripts;
	QPointer<QObject> jso;
	QPointer<WebView> wv;
};

class ChatViewThemeJS : public QObject
{
	Q_OBJECT

public:
	ChatViewThemeJS(ChatViewTheme *theme_);

public slots:
	void setMetaData(const QVariantMap &map);
	QString psiOption(const QString &option) const;
	QString colorOption(const QString &option) const;
	void setCaseInsensitiveFS(bool state = true);
	QString getFileContents(const QString &name) const;
	QString getFileContentsFromAdapterDir(const QString &name) const;
	void setHtml(const QString &h);
	QString formatDate(const QDateTime &dt, const QString &format) const;
	QString strftime(const QDateTime &dt, const QString &format) const;
	QString getPaletteColor(const QString &name) const;
	void console(const QString &text) const;
	void toCache(const QString &name, const QVariant &data);
	QVariant cache(const QString &name) const;
	void setDefaultAvatar(const QString &filename, const QString &host = QString());
	void setAvatarSize(int width, int height);
	void setJSNamespace(const QString &ns);
	const QString jsNamespace() const;
	const QString themeId() const;
	QString status2text(int status) const;
	QString hex2rgba(const QString &hex, float opacity);

private:
	ChatViewTheme *theme;
	QMap<QString,QVariant> _cache;
};
