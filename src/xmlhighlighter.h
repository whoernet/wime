#pragma once

#include <QSyntaxHighlighter>

class XmlHighlighter : public QSyntaxHighlighter
{
	Q_OBJECT
public:
	XmlHighlighter(QObject *parent);
	XmlHighlighter(QTextDocument *parent);

protected:
	void highlightBlock(const QString &text) override;

private:
	void highlightByRegex(const QTextCharFormat &format, const QRegExp &regex, const QString &text);

	void setRegexes();
	void setFormats();

private:
	QTextCharFormat     _xmlKeywordFormat;
	QTextCharFormat     _xmlElementFormat;
	QTextCharFormat     _xmlAttributeFormat;
	QTextCharFormat     _xmlValueFormat;
	QTextCharFormat     _xmlCommentFormat;

	QList<QRegExp>      _xmlKeywordRegexes;
	QRegExp             _xmlElementRegex;
	QRegExp             _xmlAttributeRegex;
	QRegExp             _xmlValueRegex;
	QRegExp             _xmlCommentRegex;
};

