#include "multiplesend_p.h"
#include "ui_multiplesend.h"
#include "psicontact.h"
#include "psiiconset.h"
#include "userlist.h"

#include <QDebug>
#include <QTreeWidgetItem>
#include <QRegExp>

MultipleSend::MultipleSend(PsiAccount *account, QWidget *parent)
	: QDialog(parent)
	, ui(new Ui::MultipleSend)
	, d(new Private)
{
    ui->setupUi(this);
	setAttribute(Qt::WA_DeleteOnClose);

	d->account = account;

	QList<int> sizes;
	sizes << 2000 << 200;
	ui->splitter->setSizes(sizes);
	ui->splitter->setStretchFactor(0, 1);
	ui->splitter->setStretchFactor(1, 0);

	updateContacts(false);
}

void MultipleSend::setJids(const QStringList &jids)
{
	QStringList bareJids = jids;
	bareJids.replaceInStrings(QRegExp(QLatin1String("/[^/]*$")), "");

	for (int i = 0; i < ui->twContacts->topLevelItemCount(); ++i) {
		QTreeWidgetItem *topLevelItem = ui->twContacts->topLevelItem(i);
		for (int j = 0; j < topLevelItem->childCount(); ++j) {
			QTreeWidgetItem *child = topLevelItem->child(j);
			QString jid = child->data(0, QTreeWidgetItem::UserType + 1).toString();
			if (bareJids.contains(jid)) {
				child->setCheckState(0, Qt::CheckState::Checked);
			}
		}
	}
}

MultipleSend::~MultipleSend()
{
	delete d;
    delete ui;
}

void MultipleSend::updateCounter()
{
	ui->lblCounter->setText(QString::number(ui->pteMessage->toPlainText().size()));
}

bool lessThan(QTreeWidgetItem *left, QTreeWidgetItem *right)
{
	if (left->data(0, QTreeWidgetItem::UserType).toInt() < right->data(0, QTreeWidgetItem::UserType).toInt()) {
		return true;
	}
	else if (left->data(0, QTreeWidgetItem::UserType).toInt() > right->data(0, QTreeWidgetItem::UserType).toInt()) {
		return false;
	}
	else {
		return left->text(0).toLower() < right->text(0).toLower();
	}
}

void MultipleSend::updateContacts(bool b)
{
	ui->twContacts->clear();

	// Fill contacts tree widget
	QList<PsiContact*> contacts = d->account->contactList();
	QList<QTreeWidgetItem*> topItems;

	foreach (PsiContact *contact, contacts) {
		if (b && contact->status().type() == XMPP::Status::Offline)
			continue;

		QTreeWidgetItem *item = new QTreeWidgetItem;
		item->setText(0, contact->name());
		item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
		item->setCheckState(0, Qt::Unchecked);
		int status = contact->status().type();
		item->setData(0, QTreeWidgetItem::UserType, status == XMPP::Status::Offline ? 1000 : status);
		item->setData(0, QTreeWidgetItem::UserType + 1, contact->jid().bare());
		item->setIcon(0, PsiIconset::instance()->statusPtr(contact->jid(), contact->status().type())->icon());

		QStringList groups = contact->groups();
		groups.removeAll(QString(""));
		bool isGeneral = false;
		bool isNotInList = false;

		if (groups.isEmpty() && contact->inList()) {
			isGeneral = true;
			groups << PsiContact::generalGroupName();
		}
		else if (groups.isEmpty() && !contact->inList()) {
			isNotInList = true;
			groups << PsiContact::notInListGroupName();
		}

		foreach (const QString &group, groups) {
			QTreeWidgetItem *groupItem = 0;
			foreach (QTreeWidgetItem *item, topItems) {
				if (isGeneral && item->data(0, QTreeWidgetItem::UserType) == 0) {
					groupItem = item;
					break;
				}
				else if (isNotInList && item->data(0, QTreeWidgetItem::UserType) == 2) {
					groupItem = item;
					break;
				}
				else if (item->text(0) == group) {
					groupItem = item;
					break;
				}
			}

			if (!groupItem) {
				groupItem = new QTreeWidgetItem();
				groupItem->setText(0, group);
				groupItem->setFlags(item->flags() | Qt::ItemIsUserCheckable);
				groupItem->setCheckState(0, Qt::Unchecked);

				if (isNotInList)
					groupItem->setData(0, QTreeWidgetItem::UserType, 2);
				else if(isGeneral)
					groupItem->setData(0, QTreeWidgetItem::UserType, 0);
				else
					groupItem->setData(0, QTreeWidgetItem::UserType, 1);

				topItems << groupItem;
			}
			groupItem->addChild(item);
		}
	}

	foreach (QTreeWidgetItem *item, topItems) {
		QList<QTreeWidgetItem*> children = item->takeChildren();
		qSort(children.begin(), children.end(), lessThan);
		item->addChildren(children);
	}

	qSort(topItems.begin(), topItems.end(), lessThan);
	ui->twContacts->addTopLevelItems(topItems);
}

void MultipleSend::sendMessage()
{
	QStringList jids;
	for (int i = 0; i < ui->twContacts->topLevelItemCount(); ++i) {
		QTreeWidgetItem *topLevelItem = ui->twContacts->topLevelItem(i);
		for (int j = 0; j < topLevelItem->childCount(); ++j) {
			QTreeWidgetItem *child = topLevelItem->child(j);
			if (child->checkState(0) == Qt::Checked) {
				QString jid = child->data(0, QTreeWidgetItem::UserType + 1).toString();
				if (!jid.isEmpty()) {
					jids << jid;
				}
			}
		}
	}

	QString message = ui->pteMessage->toPlainText();
	emit needSendMultiple(d->account, jids, message);
	close();
}

void MultipleSend::updateContactsSelection(QTreeWidgetItem *item, int column)
{
	Q_UNUSED(column);

	Qt::CheckState checkState = item->checkState(0);
	if (checkState == Qt::PartiallyChecked)
		return;

	for (int i = 0; i < item->childCount(); ++i) {
		QTreeWidgetItem *child = item->child(i);
		child->setCheckState(0, checkState);
	}

	QTreeWidgetItem *parent = item->parent();
	if (!parent)
		return;

	for (int i = 0; i < parent->childCount(); ++i) {
		QTreeWidgetItem *child = parent->child(i);
		if (child->checkState(0) != checkState) {
			checkState =  Qt::PartiallyChecked;
			break;
		}
	}

	parent->setCheckState(0, checkState);
}
