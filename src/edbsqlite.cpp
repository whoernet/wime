#include "edbsqlite_p.h"

#include "debug.h"
#include "historyimp.h"
#include "psicontactlist.h"

#include <QSqlError>
#include <QSqlDriver>
#ifndef HAVE_QT5
#include <qjson/parser.h>
#include "qjson/serializer.h"
#else
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#endif

#define FAKEDELAY 0
#define COMMIT_INTERVAL 1000 /* 1 sec */
#define DB QSqlDatabase::database("history")

//----------------------------------------------------------------------------
// EDBSqLite::SqlQuery
//----------------------------------------------------------------------------

EDBSqLite::SqlQuery::SqlQuery()
	: QSqlQuery()
{
}

EDBSqLite::SqlQuery::SqlQuery(QSqlDatabase db)
	: QSqlQuery(db)
{
}

bool EDBSqLite::SqlQuery::exec()
{
	bool res = QSqlQuery::exec();
	if (!res) {
		EDB_CRITICAL() << "Query:" << executedQuery()
					   << "\nError type:" << lastError().type()
#ifdef HAVE_QT5
					   << "\nError native code:" << lastError().nativeErrorCode()
#else
					   << "\nError native code:" << lastError().number()
#endif
					   << "\nError text:" << lastError().text();
	}
	else {
		EDB_DEBUG() << "Query:" << executedQuery();
	}
	return res;
}

bool EDBSqLite::SqlQuery::exec(const QString &query)
{
	bool res = QSqlQuery::exec(query);
	if (!res) {
		EDB_CRITICAL() << "Query:" << executedQuery()
					   << "\nError type:" << lastError().type()
#ifdef HAVE_QT5
					   << "\nError native code:" << lastError().nativeErrorCode()
#else
					   << "\nError native code:" << lastError().number()
#endif
					   << "\nError text:" << lastError().text();
	}
	else {
		EDB_DEBUG() << "Query:" << executedQuery();
	}
	return res;
}

//----------------------------------------------------------------------------
// EDBSqLite
//----------------------------------------------------------------------------

EDBSqLite::EDBSqLite(PsiCon *psi)
	: EDB(psi)
	, commited(true)
	, transactionsCounter(0)
	, lastCommitTime(QDateTime::currentDateTime())
	, commitTimer(new QTimer(this))
	, mirror_(nullptr)
	, queryes(new QueryStorage)
{
	QString path = ApplicationInfo::historyDir() + "/history.db";
	QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "history");
	db.setDatabaseName(path);
	if (!db.open()) {
		EDB_FATAL() << "EDBSqLite::EDBSqLite(): Can't open base.\n" << db.lastError().text();
		return;
	}

	if (!db.driver()->hasFeature(QSqlDriver::DriverFeature::Transactions)) {
		EDB_FATAL() << "qsqlite driver hasn't transaction feauture";
	}

	SqlQuery query(db);
	query.exec("PRAGMA foreign_keys = ON;");
	setInsertingMode(Normal);
	if (db.tables(QSql::Tables).size() == 0) {
		// no tables found.

		EDB_DEBUG() << "Initialize history db";

		if (db.transaction()) {
			query.exec("CREATE TABLE `system` ("
				"`key` TEXT, "
				"`value` TEXT"
				");");
			query.exec("CREATE TABLE `accounts` ("
				"`id` TEXT, "
				"`lifetime` INTEGER"
				");");
			query.exec("CREATE TABLE `contacts` ("
				"`id` INTEGER NOT NULL PRIMARY KEY ASC, "
				"`acc_id` TEXT, "
				"`type` INTEGER, "
				"`jid` TEXT, "
				"`lifetime` INTEGER"
				");");
			query.exec("CREATE TABLE `events` ("
				"`id` INTEGER NOT NULL PRIMARY KEY ASC, "
				"`contact_id` INTEGER NOT NULL REFERENCES `contacts`(`id`) ON DELETE CASCADE, "
				"`resource` TEXT, "
				"`date` TEXT, "
				"`type` INTEGER, "
				"`direction` INTEGER, "
				"`subject` TEXT, "
				"`m_text` TEXT, "
				"`lang` TEXT, "
				"`extra_data` TEXT"
				");");
			query.exec("CREATE INDEX `key` ON `system` (`key`);");
			query.exec("CREATE INDEX `jid` ON `contacts` (`jid`);");
			query.exec("CREATE INDEX `contact_id` ON `events` (`contact_id`);");
			query.exec("CREATE INDEX `date` ON `events` (`date`);");
			if (db.commit()) {
				setStorageParam("version", "0.1");
				setStorageParam("import_start", "yes");
			}
			else {
				EDB_FATAL() << "Can't commit history db transaction";
			}
		}
		else {
			EDB_FATAL() << "Can't start history db transaction";
		}
	}

	connect(commitTimer, SIGNAL(timeout()), this, SLOT(commit()));
	commitTimer->setSingleShot(true);
	commitTimer->setInterval(COMMIT_INTERVAL);

	if (!getStorageParam("import_start").isEmpty()) {
		if (!importExecute()) {
			EDB_FATAL() << "Can't import plain text history";
		}
	}

	setMirror(new EDBFlatFile(psi));
}

EDBSqLite::~EDBSqLite()
{
	commit();
	DB.close();
	QSqlDatabase::removeDatabase("history");
	delete queryes;
}

int EDBSqLite::get(const QString &accId, const XMPP::Jid &jid, QDateTime date, int direction, int start, int len)
{
	item_query_req *r = new item_query_req;
	r->accId = accId;
	r->j     = jid;
	r->type  = item_query_req::Type_get;
	r->start = start;
	r->len   = len < 1 ? 1 : len;
	r->dir   = direction;
	r->date  = date;
	r->id    = genUniqueId();
	rlist.append(r);

	QTimer::singleShot(FAKEDELAY, this, SLOT(performRequests()));
	return r->id;
}

int EDBSqLite::find(const QString &accId, const QString &str, const XMPP::Jid &jid, const QDateTime date, int direction)
{
	item_query_req *r = new item_query_req;
	r->accId   = accId;
	r->j       = jid;
	r->type    = item_query_req::Type_find;
	r->len     = 1;
	r->dir     = direction;
	r->findStr = str;
	r->date    = date;
	r->id      = genUniqueId();
	rlist.append(r);

	QTimer::singleShot(FAKEDELAY, this, SLOT(performRequests()));
	return r->id;
}

int EDBSqLite::append(const QString &accId, const XMPP::Jid &jid, const PsiEvent::Ptr &e, int type)
{
	item_query_req *r = new item_query_req;
	r->accId   = accId;
	r->j       = jid;
	r->jidType = type;
	r->type    = item_file_req::Type_append;
	r->event   = e;
	if (!r->event) {
		EDB_CRITICAL() << "EDBSqLite::append(): Attempted to append incompatible type";
		delete r;
		return 0;
	}
	r->id      = genUniqueId();
	rlist.append(r);

	QTimer::singleShot(FAKEDELAY, this, SLOT(performRequests()));

	if (mirror_)
		mirror_->append(accId, jid, e, type);

	return r->id;
}

int EDBSqLite::erase(const QString &accId, const XMPP::Jid &jid)
{
	item_query_req *r = new item_query_req;
	r->accId = accId;
	r->j     = jid;
	r->type  = item_query_req::Type_erase;
	r->id    = genUniqueId();
	rlist.append(r);

	QTimer::singleShot(FAKEDELAY, this, SLOT(performRequests()));
	return r->id;
}

QList<EDB::ContactItem> EDBSqLite::contacts(const QString &accId, int type)
{
	QList<ContactItem> res;
	EDBSqLite::SqlQuery *query = queryes->getPreparedQuery(QueryContactsList, accId.isEmpty(), true);
	query->bindValue(":type", type);
	if (!accId.isEmpty())
		query->bindValue(":acc_id", accId);
	if (query->exec()) {
		while (query->next()) {
			const QSqlRecord &rec = query->record();
			res.append(ContactItem(rec.value("acc_id").toString(), XMPP::Jid(rec.value("jid").toString())));
		}
		query->finish();
	}
	return res;
}

quint64 EDBSqLite::eventsCount(const QString &accId, const XMPP::Jid &jid)
{
	quint64 res = 0;
	bool fAccAll  = accId.isEmpty();
	bool fContAll = jid.isEmpty();
	EDBSqLite::SqlQuery *query = queryes->getPreparedQuery(QueryRowCount, fAccAll, fContAll);
	if (!fAccAll)
		query->bindValue(":acc_id", accId);
	if (!fContAll)
		query->bindValue(":jid", jid.full());
	if (query->exec()) {
		if (query->next())
			res = query->record().value("count").toULongLong();
		query->finish();
	}
	return res;
}

QString EDBSqLite::getStorageParam(const QString &key)
{
	SqlQuery query(DB);
	query.prepare("SELECT `value` FROM `system` WHERE `key` = :key;");
	query.bindValue(":key", key);
	if (query.exec() && query.next())
		return query.record().value("value").toString();
	return QString();
}

void EDBSqLite::setStorageParam(const QString &key, const QString &val)
{
	transaction(true);
	SqlQuery query(DB);
	if (val.isEmpty()) {
		query.prepare("DELETE FROM `system` WHERE `key` = :key;");
		query.bindValue(":key", key);
		query.exec();
	}
	else {
		query.prepare("SELECT COUNT(*) AS `count` FROM `system` WHERE `key` = :key;");
		query.bindValue(":key", key);
		if (query.exec() && query.next() && query.record().value("count").toULongLong() != 0) {
			query.prepare("UPDATE `system` SET `value` = :val WHERE `key` = :key;");
			query.bindValue(":key", key);
			query.bindValue(":val", val);
			query.exec();
		}
		else {
			query.prepare("INSERT INTO `system` (`key`, `value`) VALUES (:key, :val);");
			query.bindValue(":key", key);
			query.bindValue(":val", val);
			query.exec();
		}
	}
	commit();
}

void EDBSqLite::setInsertingMode(InsertMode mode)
{
	// in the case of a flow of new records
	if (mode == Import) {
		// Commit after 10000 inserts and every 5 seconds
		maxUncommitedRecs = 10000;
		maxUncommitedSecs = 5;
	}
	else {
		// Commit after 3 inserts and every 1 second
		maxUncommitedRecs = 3;
		maxUncommitedSecs = 1;
	}
	// Commit if there were no new additions for 1 second
	commitByTimeoutSecs = 1;
	//--
	commit();
}

void EDBSqLite::setMirror(EDBFlatFile *mirr)
{
	if (mirr != mirror_) {
		delete mirror_;
		mirror_ = mirr;
	}
}

EDBFlatFile *EDBSqLite::mirror() const
{
	return mirror_;
}

void EDBSqLite::performRequests()
{
	if (rlist.isEmpty())
		return;

	item_query_req *r = rlist.takeFirst();
	const int type = r->type;

	if (type == item_query_req::Type_append) {
		bool b = appendEvent(r->accId, r->j, r->event, r->jidType);
		writeFinished(r->id, b);
	}

	else if (type == item_query_req::Type_get) {
		commit();
		bool fContAll = r->j.isEmpty();
		bool fAccAll  = r->accId.isEmpty();
		QueryType queryType;
		if (r->date.isNull()) {
			if (r->dir == Forward) {
				queryType = QueryOldest;
			}
			else {
				queryType = QueryLatest;
			}
		}
		else {
			if (r->dir == Backward) {
				queryType = QueryDateBackward;
			}
			else {
				queryType = QueryDateForward;
			}
		}
		EDBSqLite::SqlQuery *query = queryes->getPreparedQuery(queryType, fAccAll, fContAll);
		if (!fContAll)
			query->bindValue(":jid", r->j.full());
		if (!fAccAll)
			query->bindValue(":acc_id", r->accId);
		if (!r->date.isNull())
			query->bindValue(":date", r->date);
		query->bindValue(":start", r->start);
		query->bindValue(":cnt", r->len);
		EDBResult result;
		if (query->exec()) {
			while (query->next()) {
				PsiEvent::Ptr e(getEvent(query->record()));
				if (e) {
					QString id = query->record().value("id").toString();
					result.append(EDBItemPtr(new EDBItem(e, id)));
				}
			}
			query->finish();
		}
		int beginRow;
		if (r->dir == Forward && r->date.isNull()) {
			beginRow = r->start;
		}
		else {
			int cnt = rowCount(r->accId, r->j, r->date);
			if (r->dir == Backward) {
				beginRow = cnt - r->len + 1;
				if (beginRow < 0) {
					beginRow = 0;
				}
			}
			else {
				beginRow = cnt + 1;
			}
		}
		resultReady(r->id, result, beginRow);

	}
	else if(type == item_file_req::Type_find) {
		commit();
		bool fContAll = r->j.isEmpty();
		bool fAccAll  = r->accId.isEmpty();
		EDBSqLite::SqlQuery *query = queryes->getPreparedQuery(QueryFindText, fAccAll, fContAll);
		if (!fContAll)
			query->bindValue(":jid", r->j.full());
		if (!fAccAll)
			query->bindValue(":acc_id", r->accId);
		EDBResult result;
		if (query->exec()) {
			QString str = r->findStr.toLower();
			while (query->next()) {
				const QSqlRecord rec = query->record();
				if (!rec.value("m_text").toString().toLower().contains(str, Qt::CaseSensitive))
					continue;
				PsiEvent::Ptr e(getEvent(rec));
				if (e) {
					QString id = rec.value("id").toString();
					EDBItemPtr eip = EDBItemPtr(new EDBItem(e, id));
					result.append(eip);
				}
			}
			query->finish();
		}
		resultReady(r->id, result, 0);

	}
	else if(type == item_file_req::Type_erase) {
		writeFinished(r->id, eraseHistory(r->accId, r->j));
	}

	delete r;
}

bool EDBSqLite::appendEvent(const QString &accId, const XMPP::Jid &jid, const PsiEvent::Ptr &e, int jidType)
{
	const qint64 contactId = ensureJidRowId(accId, jid, jidType);
	if (contactId == 0)
		return false;

	QDateTime dTime;
	int nType = 0;

	if (e->type() == PsiEvent::Message) {
		MessageEvent::Ptr me = e.staticCast<MessageEvent>();
		const Message &m = me->message();
		dTime = m.timeStamp();
		if (m.type() == "chat")
			nType = 1;
		else if(m.type() == "error")
			nType = 4;
		else if(m.type() == "headline")
			nType = 5;

	}
	else if (e->type() == PsiEvent::Auth) {
		AuthEvent::Ptr ae = e.staticCast<AuthEvent>();
		dTime = ae->timeStamp();
		QString subType = ae->authType();
		if(subType == "subscribe")
			nType = 3;
		else if(subType == "subscribed")
			nType = 6;
		else if(subType == "unsubscribe")
			nType = 7;
		else if(subType == "unsubscribed")
			nType = 8;
	}
	else {
		EDB_CRITICAL() << "Wrong event type";
		return false;
	}

	int nDirection = e->originLocal() ? 1 : 2;
	if (!transaction(false)) {
		return false;
	}

	SqlQuery *query = queryes->getPreparedQuery(QueryInsertEvent, false, false);
	query->bindValue(":contact_id", contactId);
	query->bindValue(":resource", (jidType != GroupChatContact) ? jid.resource() : "");
	query->bindValue(":date", dTime);
	query->bindValue(":type", nType);
	query->bindValue(":direction", nDirection);
	if (nType == 0 || nType == 1 || nType == 4 || nType == 5) {
		MessageEvent::Ptr me = e.staticCast<MessageEvent>();
		const Message &m = me->message();
		QString lang = m.lang();
		query->bindValue(":subject", m.subject(lang));
		query->bindValue(":m_text", m.body(lang));
		query->bindValue(":lang", lang);
		QString extraData;
		const UrlList &urls = m.urlList();
		if (!urls.isEmpty()) {
			QVariantMap xepList;
			QVariantList urlList;
			for (const Url &url: urls)
				if (!url.url().isEmpty()) {
					QVariantList urlItem;
					urlItem.append(QVariant(url.url()));
					if (!url.desc().isEmpty())
						urlItem.append(QVariant(url.desc()));
					urlList.append(QVariant(urlItem));
				}
			xepList["jabber:x:oob"] = QVariant(urlList);
#ifndef HAVE_QT5
			QJson::Serializer serializer;
			extraData = QString::fromUtf8(serializer.serialize(xepList));
#else
			QJsonDocument doc(QJsonObject::fromVariantMap(xepList));
			extraData = QString::fromUtf8(doc.toBinaryData());
#endif

		}
		query->bindValue(":extra_data", extraData);
	}
	else {
		query->bindValue(":subject", QVariant(QVariant::String));
		query->bindValue(":m_text", QVariant(QVariant::String));
		query->bindValue(":lang", QVariant(QVariant::String));
		query->bindValue(":extra_data", QVariant(QVariant::String));
	}
	bool res = query->exec();
	return res;
}

PsiEvent::Ptr EDBSqLite::getEvent(const QSqlRecord &record)
{
	PsiAccount *pa = psi()->contactList()->getAccount(record.value("acc_id").toString());

	int type = record.value("type").toInt();

	if(type == 0 || type == 1 || type == 4 || type == 5) {
		Message m;
		m.setTimeStamp(record.value("date").toDateTime());
		if(type == 1)
			m.setType("chat");
		else if(type == 4)
			m.setType("error");
		else if(type == 5)
			m.setType("headline");
		else
			m.setType("");
		m.setFrom(Jid(record.value("jid").toString()));
		QVariant text = record.value("m_text");
		if (!text.isNull()) {
			m.setBody(text.toString());
			m.setLang(record.value("lang").toString());
			m.setSubject(record.value("subject").toString());
		}
		m.setSpooled(true);
		QString extraStr = record.value("extra_data").toString();
		if (!extraStr.isEmpty()) {
#ifndef HAVE_QT5
			QJson::Parser parser;
#endif
			bool fOk;
#ifndef HAVE_QT5
			QVariantMap extraData = parser.parse(extraStr.toUtf8(), &fOk).toMap();
#else
			QJsonDocument doc = QJsonDocument::fromJson(extraStr.toUtf8());
			fOk = !doc.isNull();
			QVariantMap extraData = doc.object().toVariantMap();
#endif
			if (fOk) {
				for (const QVariant &urlItem: extraData["jabber:x:oob"].toList()) {
					QVariantList itemList = urlItem.toList();
					if (!itemList.isEmpty()) {
						QString url = itemList.at(0).toString();
						QString desc;
						if (itemList.size() > 1)
							desc = itemList.at(1).toString();
						m.urlAdd(Url(url, desc));
					}
				}
			}
		}
		MessageEvent::Ptr me(new MessageEvent(m, pa));
		me->setOriginLocal((record.value("direction").toInt() == 1));
		return me.staticCast<PsiEvent>();
	}

	if(type == 2 || type == 3 || type == 6 || type == 7 || type == 8) {
		QString subType = "subscribe";
		// if(type == 2) { // Not used (stupid "system message" from Psi <= 0.8.6)
		if(type == 3)
			subType = "subscribe";
		else if(type == 6)
			subType = "subscribed";
		else if(type == 7)
			subType = "unsubscribe";
		else if(type == 8)
			subType = "unsubscribed";

		AuthEvent::Ptr ae(new AuthEvent(Jid(record.value("jid").toString()), subType, pa));
		ae->setTimeStamp(record.value("date").toDateTime());
		return ae.staticCast<PsiEvent>();
	}
	return PsiEvent::Ptr();
}

qint64 EDBSqLite::ensureJidRowId(const QString &accId, const XMPP::Jid &jid, int type)
{
	if (jid.isEmpty())
		return 0;
	QString sJid = (type == GroupChatContact) ? jid.full() : jid.bare();
	QString sKey = accId + "|" + sJid;
	qint64 id = jidsCache.value(sKey, 0);
	if (id != 0)
		return id;

	EDBSqLite::SqlQuery *query = queryes->getPreparedQuery(QueryJidRowId, false, false);
	query->bindValue(":jid", sJid);
	query->bindValue(":acc_id", accId);
	if (query->exec()) {
		if (query->first()) {
			id = query->record().value("id").toLongLong();
		}
		else {
			//
			SqlQuery queryIns(DB);
			queryIns.prepare("INSERT INTO `contacts` (`acc_id`, `type`, `jid`, `lifetime`)"
				" VALUES (:acc_id, :type, :jid, -1);");
			queryIns.bindValue(":acc_id", accId);
			queryIns.bindValue(":type", type);
			queryIns.bindValue(":jid", sJid);
			if (queryIns.exec()) {
				id  = queryIns.lastInsertId().toLongLong();
			}
		}
		query->finish();
		if (id != 0) {
			jidsCache[sKey] = id;
		}
	}
	return id;
}

int EDBSqLite::rowCount(const QString &accId, const XMPP::Jid &jid, QDateTime before)
{
	bool fAccAll  = accId.isEmpty();
	bool fContAll = jid.isEmpty();
	QueryType type;
	if (before.isNull())
		type = QueryRowCount;
	else
		type = QueryRowCountBefore;
	SqlQuery *query = queryes->getPreparedQuery(type, fAccAll, fContAll);
	if (!fContAll)
		query->bindValue(":jid", jid.full());
	if (!fAccAll)
		query->bindValue(":acc_id", accId);
	if (!before.isNull())
		query->bindValue(":date", before);
	int res = 0;
	if (query->exec()) {
		if (query->next()) {
			res = query->record().value("count").toInt();
		}
		query->finish();
	}
	return res;
}

bool EDBSqLite::eraseHistory(const QString &accId, const XMPP::Jid &jid)
{
	bool res = false;
	if (!transaction(true))
		return false;

	if (accId.isEmpty() && jid.isEmpty()) {
		SqlQuery query(DB);
		//if (query.exec("DELETE FROM `events`;"))
		if (query.exec("DELETE FROM `contacts`;")) {
			jidsCache.clear();
			res = true;
		}
	}
	else {
		SqlQuery *query = queryes->getPreparedQuery(QueryJidRowId, false, false);
		query->bindValue(":jid", jid.full());
		query->bindValue(":acc_id", accId);
		if (query->exec()) {
			if (query->next()) {
				const qint64 id = query->record().value("id").toLongLong();
				SqlQuery query2(DB);
				query2.prepare("DELETE FROM `events` WHERE `contact_id` = :id;");
				query2.bindValue(":id", id);
				if (query2.exec()) {
					res = true;
					query2.prepare("DELETE FROM `contacts` WHERE `id` = :id AND `lifetime` = -1;");
					query2.bindValue(":id", id);
					if (query2.exec()) {
						if (query2.numRowsAffected() > 0)
							jidsCache.clear();
					}
					else {
						res = false;
					}
				}
			}
			query->finish();
		}
	}

	if (res)
		res = commit();
	else
		rollback();

	return res;
}

bool EDBSqLite::transaction(bool now)
{
	if (now || transactionsCounter >= maxUncommitedRecs || lastCommitTime.secsTo(QDateTime::currentDateTime()) >= maxUncommitedSecs) {
		if (!commit()) {
			EDB_CRITICAL() << "Can't start transaction";
			return false;
		}
	}

	if (commited) {
		if (!DB.transaction()) {
			EDB_CRITICAL() << "Can't start transaction";
			return false;
		}
		commited = false;
	}
	++transactionsCounter;

	commitTimer->start();

	return true;
}

bool EDBSqLite::commit()
{
	if (commited || DB.commit()) {
		transactionsCounter = 0;
		lastCommitTime = QDateTime::currentDateTime();
		commited = true;
		commitTimer->stop();
		return true;
	}

	EDB_CRITICAL() << "Can't commit transaction";
	return false;
}

bool EDBSqLite::rollback()
{
	if (!commited && DB.rollback()) {
		transactionsCounter = 0;
		lastCommitTime = QDateTime::currentDateTime();
		commited = true;
		commitTimer->stop();
		return true;
	}

	EDB_CRITICAL() << "Can't rollback transaction";
	return false;
}

bool EDBSqLite::importExecute()
{
	bool res = true;
	HistoryImport *imp = new HistoryImport(psi(), this);
	if (imp->isNeeded() && imp->exec() != HistoryImport::ResultNormal) {
		res = false;
	}
	delete imp;
	return res;
}

// ****************** class PreparedQueryes ********************

EDBSqLite::QueryStorage::QueryStorage()
{
}

EDBSqLite::QueryStorage::~QueryStorage()
{
	qDeleteAll(queryList);
}

EDBSqLite::SqlQuery *EDBSqLite::QueryStorage::getPreparedQuery(QueryType type, bool allAccounts, bool allContacts)
{
	QueryProperty queryProp(type, allAccounts, allContacts);
	EDBSqLite::SqlQuery *q = queryList.value(queryProp, NULL);
	if (q != NULL)
		return q;

	q = new EDBSqLite::SqlQuery(DB);
	q->setForwardOnly(true);
	q->prepare(getQueryString(type, allAccounts, allContacts));
	queryList[queryProp] = q;
	return q;
}

QString EDBSqLite::QueryStorage::getQueryString(QueryType type, bool allAccounts, bool allContacts)
{
	QString queryStr;
	switch (type)
	{
		case QueryContactsList:
			queryStr = "SELECT `acc_id`, `jid` FROM `contacts` WHERE `type` = :type";
			if (!allAccounts)
				queryStr.append(" AND `acc_id` = :acc_id");
			queryStr.append(" ORDER BY `jid`;");
			break;

		case QueryLatest:
		case QueryOldest:
		case QueryDateBackward:
		case QueryDateForward:
			queryStr = "SELECT `acc_id`, `events`.`id`, `jid`, `date`, `events`.`type`, `direction`, `subject`, `m_text`, `lang`, `extra_data`"
				" FROM `events`, `contacts`"
				" WHERE `contacts`.`id` = `contact_id`";
			if (!allContacts)
				queryStr.append(" AND `jid` = :jid");
			if (!allAccounts)
				queryStr.append(" AND `acc_id` = :acc_id");
			if (type == QueryDateBackward)
				queryStr.append(" AND `date` < :date");
			else if (type == QueryDateForward)
				queryStr.append(" AND `date` >= :date");
			if (type == QueryLatest || type == QueryDateBackward)
				queryStr.append(" ORDER BY `date` DESC");
			else
				queryStr.append(" ORDER BY `date` ASC");
			queryStr.append(" LIMIT :start, :cnt;");
			break;

		case QueryRowCount:
		case QueryRowCountBefore:
			queryStr = "SELECT count(*) AS `count`"
				" FROM `events`, `contacts`"
				" WHERE `contacts`.`id` = `contact_id`";
			if (!allContacts)
				queryStr.append(" AND `jid` = :jid");
			if (!allAccounts)
				queryStr.append(" AND `acc_id` = :acc_id");
			if (type == QueryRowCountBefore)
				queryStr.append(" AND `date` < :date");
			queryStr.append(";");
			break;

		case QueryJidRowId:
			queryStr = "SELECT `id` FROM `contacts` WHERE `jid` = :jid AND acc_id = :acc_id;";
			break;

		case QueryFindText:
			queryStr = "SELECT `acc_id`, `events`.`id`, `jid`, `date`, `events`.`type`, `direction`, `subject`, `m_text`, `lang`, `extra_data`\n"
				" FROM `events`, `contacts`\n"
				" WHERE `contacts`.`id` = `contact_id`\n";
			if (!allContacts)
				queryStr.append(" AND `jid` = :jid\n");
			if (!allAccounts)
				queryStr.append(" AND `acc_id` = :acc_id\n");
			queryStr.append(" AND `m_text` IS NOT NULL\n");
			queryStr.append(" ORDER BY `date`;");
			break;

		case QueryInsertEvent:
			queryStr = "INSERT INTO `events` (\n"
				"`contact_id`, `resource`, `date`, `type`, `direction`, `subject`, `m_text`, `lang`, `extra_data`\n"
				") VALUES (\n"
				":contact_id, :resource, :date, :type, :direction, :subject, :m_text, :lang, :extra_data\n"
				");";
			break;
	}
	return queryStr;
}


