# Find hunspell library

# Hunspell_INCLUDE_DIRS
# Hunspell_LDFLAGS
# Hunspell_CFLAGS
# Hunspell_FOUND

if(WIN32)
  if(MSVC)
    message(FATAL_ERROR "We currently support only MinGW")
  endif()
endif()

find_package(PkgConfig)

pkg_search_module(Hunspell hunspell)

# Fallback use direct search for headers and library
if(NOT Hunspell_FOUND)
  find_path(Hunspell_INCLUDE_DIRS
    hunspell.hxx
    PATHS
    ${HUNSPELL_ROOT}/include/hunspell
  )  

  if(Hunspell_INCLUDE_DIRS)
    find_library(Hunspell_LDFLAGS NAMES hunspell-1.3 hunspell PATHS ${ENCHANT_ROOT}/lib)
    get_filename_component(Hunspell_LIBRARY_DIRS ${Hunspell_LDFLAGS} DIRECTORY)
  endif()

  if(Hunspell_INCLUDE_DIRS AND Hunspell_LDFLAGS)
    set(Hunspell_FOUND 1)
  endif()
endif()


if(Hunspell_FOUND)
  message(STATUS "Found the hunspell library at ${Hunspell_LIBRARY_DIRS}")
  message(STATUS "Found the hunspell headers at ${Hunspell_INCLUDE_DIRS}")
else()
  if(Hunspell_FIND_REQUIRED)
    message(FATAL_ERROR "Could NOT find required hunspell library, aborting. Try to set HUNSPELL_ROOT.")
  else()
    message(STATUS "Could NOT find hunspell")
  endif()
endif()
