#pragma once

#include <QTreeView>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QToolButton>

class PsiOptions;
class OptionAccessingHost;

namespace extendedoptions {

class PsiOptionsEditor : public QWidget
{
	Q_OBJECT
public:
	PsiOptionsEditor(OptionAccessingHost *options, QWidget *parent=0);

private slots:
	void tv_edit(const QModelIndex &idx);
	void selectionChanged(const QModelIndex &idx);
	void updateWidth();
	void add();
	void edit();
	void deleteit();
	void resetit();

private:
	OptionAccessingHost *o_;
	QTreeView* tv_;
	int tv_colWidth;
	QAbstractItemModel *tm_;
	QCheckBox* cb_;
	QLabel*	lb_type;
	QLabel* lb_path;
	QLabel* lb_comment;
	QPushButton *pb_delete;
	QPushButton *pb_reset;
	QPushButton *pb_edit;
	QPushButton *pb_new;
};

} // namespace extendedoptions
