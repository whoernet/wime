#pragma once

#include <psiplugin.h>
#include <optionaccessor.h>
#include <optionaccessinghost.h>
#include <activetabaccessor.h>
#include <activetabaccessinghost.h>
#include <accountinfoaccessor.h>
#include <accountinfoaccessinghost.h>
#include <plugininfoprovider.h>
#include <psiaccountcontrollinghost.h>
#include <psiaccountcontroller.h>
#include <eventfilter.h>

#include <QSpinBox>
#include <QCheckBox>
#include <QTextEdit>
#include <QDomElement>
#include <QComboBox>
#include <QLabel>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDateTime>
\
class AutoReply : public QObject
				, public PsiPlugin
				, public OptionAccessor
				, public ActiveTabAccessor
				, public AccountInfoAccessor
				, public PluginInfoProvider
				, public PsiAccountController
				, public EventFilter
{
	Q_OBJECT
#ifdef HAVE_QT5
	Q_PLUGIN_METADATA(IID "com.psi-plus.AutoReplyPlugin")
#endif
	Q_INTERFACES(
			PsiPlugin
			OptionAccessor
			ActiveTabAccessor
			AccountInfoAccessor
			PluginInfoProvider
			PsiAccountController
			EventFilter
	)

public:
	AutoReply();
	virtual QString name() const;
	virtual QString shortName() const;
	virtual QString version() const;
	virtual QWidget* options();
	virtual bool enable();
	virtual bool disable();
	virtual void applyOptions();
	virtual void restoreOptions();
	virtual void setOptionAccessingHost(OptionAccessingHost* host);
	virtual void optionChanged(const QString& option);
	virtual void setActiveTabAccessingHost(ActiveTabAccessingHost* host);
	virtual void setAccountInfoAccessingHost(AccountInfoAccessingHost* host);
	void setPsiAccountControllingHost(PsiAccountControllingHost *host);
	virtual QString pluginInfo();
	virtual QPixmap icon() const;


	// EventFilter
	bool processEvent(int accountIndex, QDomElement &e);
	bool processMessage(int accountIndex, const QString &contact,
						const QString &body, const QString &subject);
	bool processOutgoingMessage(int accountIndex, const QString &contact,
								QString &body, const QString &type,
								QString &subject);
	void logout(int accountIndex);

private:
	bool enabled;
	AccountInfoAccessingHost* AccInfoHost;
	ActiveTabAccessingHost* ActiveTabHost;
	PsiAccountControllingHost *accountHost;
	OptionAccessingHost* psiOptions;
	QTextEdit *messageWidget;
	QTextEdit *disableforWidget;
	QString Message;
	QString DisableFor;
	QSpinBox *spinWidget;
	QSpinBox *resetWidget;
	QCheckBox *activetabWidget;
	QComboBox *enabledisableWidget;
	QTextEdit *DisableForAccWidget;
	QCheckBox *sonlineWidget;
	QCheckBox *sawayWidget;
	QCheckBox *sdndWidget;
	QCheckBox *sxaWidget;
	QCheckBox *schatWidget;
	QCheckBox *sinvisWidget;
	QCheckBox *NotInRosterWidget;
	bool NotInRoster;
	int EnableDisable;
	struct Base {
		int Account;
		QString Jid;
		int count;
		QDateTime LastMes;
	};
	QVector<Base> Counter;
	int Times;
	int ResetTime;
	bool ActiveTabIsEnable;
	bool SOnline;
	bool SAway;
	bool SDnd;
	bool SXa;
	bool SChat;
	bool SInvis;
	QString DisableForAcc;
	bool FindAcc(int account, QString jid, int &i);

private slots:
	void setEnableDisableText(int Arg);
	void sendMessage();
};
