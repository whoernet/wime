/*
 * massdelivery.cpp
 *
 * Copyright (C) 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "massdelivery.h"

#include "options.h"

#include <psiaccountcontrollinghost.h>
#include <iconfactoryaccessinghost.h>
#include <activetabaccessinghost.h>
#include <accountinfoaccessinghost.h>

#include <QFile>
#include <QDir>
#include <QInputDialog>
#include <QProcess>
#include <QMessageBox>
#include <QDebug>

namespace massdelivery {

#ifndef HAVE_QT5
Q_EXPORT_PLUGIN2(MassDelivery, MassDelivery);
#endif

#define TIMEOUT 30 * 1000 /* 30 seconds */

MassDelivery::MassDelivery()
	: _enabled(false)
	, _accountHost(nullptr)
	, _accountInfo(nullptr)
	, _appInfo(nullptr)
	, _iconFactory(nullptr)
	, _stanzaSending(nullptr)
	, _contactInfoHost(nullptr)
	, _counter(0)
	, _accountId(0)
	, _jidToSend()
	, _optionsForm(nullptr)
{
}


MassDelivery::~MassDelivery()
{
}

QWidget *MassDelivery::options()
{
	if (!_enabled) {
		return nullptr;
	}

	_optionsForm = new Options();
	_optionsForm->setAccountHost(_accountHost);
	_optionsForm->setAccountInfoHost(_accountInfo);
	_optionsForm->setContactInfoAccessingHost(_contactInfoHost);
	return qobject_cast<QWidget*>(_optionsForm);
}

bool MassDelivery::enable()
{
	_enabled = true;

	QFile file(":/icons/massdelivery.png");
	file.open(QIODevice::ReadOnly);
	QByteArray image = file.readAll();
	_iconFactory->addIcon("command/icon", image);
	file.close();

	return _enabled;
}

bool MassDelivery::disable()
{
	_enabled = false;
	return true;
}

void MassDelivery::applyOptions()
{
}

void MassDelivery::restoreOptions()
{
}

QPixmap MassDelivery::icon() const
{
	return QPixmap(":/icons/massdelivery.png");
}

QString MassDelivery::pluginInfo()
{
	return QString();
}

void MassDelivery::setApplicationInfoAccessingHost(ApplicationInfoAccessingHost *host)
{
	 _appInfo = host;
	 QString cacheDir =_appInfo->appHomeDir(ApplicationInfoAccessingHost::HomedirType::CacheLocation);
}

} // namespace massdelivery
