/*
 * options.h
 *
 * Copyright (C) 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma once

#include <QWidget>

class PsiAccountControllingHost;
class AccountInfoAccessingHost;
class ContactInfoAccessingHost;

class QTimer;

namespace massdelivery {

namespace Ui { class Options; }

class Options : public QWidget
{
    Q_OBJECT

public:
    explicit Options(QWidget *parent = nullptr);
    ~Options();

	void setAccountHost(PsiAccountControllingHost *accountHost);
	void setAccountInfoHost(AccountInfoAccessingHost *accountInfoHost);
	void setContactInfoAccessingHost(ContactInfoAccessingHost *contactInfoHost);

private slots:
	void send();
	void sendMessage();
	void showContacts();
	void updateJidsToSend();

private:
	void startSending(bool b);

    Ui::Options *ui;
	PsiAccountControllingHost *_accountHost;
	AccountInfoAccessingHost *_accountInfoHost;
	ContactInfoAccessingHost *_contactInfoHost;
	QTimer *_timer;
	QStringList _jidsToSend;
	int _account;
	QString _message;
};

} // namespace massdelivery
