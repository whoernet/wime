/*
 * options.cpp
 *
 * Copyright (C) 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "options.h"
#include <ui_options.h>

#include "contacts.h"

#include <psiaccountcontrollinghost.h>
#include <contactinfoaccessinghost.h>
#include <accountinfoaccessinghost.h>

#include <QMessageBox>
#include <QTimer>

namespace massdelivery {

Options::Options(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::Options)
	, _accountHost(nullptr)
	, _accountInfoHost(nullptr)
	, _contactInfoHost(nullptr)
	, _timer(new QTimer(this))
	, _jidsToSend()
	, _account(0)
	, _message()
{
    ui->setupUi(this);

	connect(_timer, SIGNAL(timeout()), SLOT(sendMessage()));

	startSending(false);
}

Options::~Options()
{
	delete ui;
}

void Options::setAccountHost(PsiAccountControllingHost *accountHost)
{
	Q_ASSERT(accountHost);

	_accountHost = accountHost;
}

void Options::setAccountInfoHost(AccountInfoAccessingHost *accountInfoHost)
{
	Q_ASSERT(accountInfoHost);

	_accountInfoHost = accountInfoHost;
	int i = 0;
	while (!_accountInfoHost->getName(i).isEmpty()) {
		ui->cmbAccount->addItem(_accountInfoHost->getName(i++));
	}
}

void Options::setContactInfoAccessingHost(ContactInfoAccessingHost *contactInfoHost)
{
	Q_ASSERT(contactInfoHost);

	_contactInfoHost = contactInfoHost;
}

void Options::send()
{
	if (_timer->isActive()) {
		startSending(false);
	}
	else {
		_message = ui->pteMessage->toPlainText();
		_message.replace(QLatin1Char('\n'), QLatin1String("<br/>"));
		updateJidsToSend();

		if (_accountInfoHost->getStatus(_account) == QLatin1String("offline") || _message.isEmpty() || _jidsToSend.isEmpty())
			return;

		startSending(true);
	}
}

void Options::sendMessage()
{
	if (_accountInfoHost->getStatus(_account) == QLatin1String("offline")) {
		startSending(false);
	}

	QString jid = _jidsToSend.takeFirst();

	QString jidName = QString(QLatin1String("%1 (%2)")).arg(_contactInfoHost->name(_account, jid)).arg(jid);
	ui->lblJid->setText(jidName);
	_accountHost->appendMsg(_account, jid, _message);

	ui->progressBar->setValue(ui->progressBar->value() + 1);

	if (_jidsToSend.isEmpty()) {
		startSending(false);
	}
}

void Options::showContacts()
{
	updateJidsToSend();

	QString str;

	for (const auto &jid: _jidsToSend) {
		str += QString(QLatin1String("%1 (%2)\n")).arg(_contactInfoHost->name(_account, jid)).arg(jid);
	}
	str = str.trimmed();

	Contacts contacts(str, this);
	contacts.exec();
}

void Options::updateJidsToSend()
{
	_account = ui->cmbAccount->currentIndex();

	_jidsToSend = _accountInfoHost->getRoster(_account);

	QStringList excludeContacts = ui->pteContacts->toPlainText().split(QLatin1Char('\n'));
	excludeContacts.removeAll(QString());
	for (const auto &jid: excludeContacts) {
		_jidsToSend.removeAll(jid);
	}

	QStringList excludeGroups = ui->pteGroups->toPlainText().split(QLatin1Char('\n'));
	excludeGroups.removeAll(QString());

	for (int i = _jidsToSend.size() - 1; i >= 0; --i) {
		QString jid = _jidsToSend.at(i);
		QStringList groups = _contactInfoHost->groups(_account, jid);
		for (const auto &group: excludeGroups) {
			groups.removeAll(group);
		}

		if (groups.isEmpty())
			_jidsToSend.removeAt(i);
	}

}

void Options::startSending(bool b)
{
	if (b) {
		_timer->start(ui->spnInterval->value());
		ui->btnSend->setText(tr("Cancel"));
		ui->progressBar->setValue(0);
		ui->progressBar->setMaximum(_jidsToSend.size());
		ui->progressBar->show();
		ui->lblJid->show();
	}
	else {
		_timer->stop();
		ui->progressBar->hide();
		ui->btnSend->setText(tr("Send"));
		ui->lblJid->setText(QString());
		ui->lblJid->hide();
	}
}

} // namespace massdelivery
