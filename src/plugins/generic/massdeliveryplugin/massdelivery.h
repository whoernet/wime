/*
 * massdelivery.h
 *
 * Copyright (C) 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma once

#include <psiplugin.h>
#include <accountinfoaccessor.h>
#include <applicationinfoaccessinghost.h>
#include <applicationinfoaccessinghost.h>
#include <applicationinfoaccessor.h>
#include <contactinfoaccessor.h>
#include <contactstateaccessinghost.h>
#include <iconfactoryaccessor.h>
#include <plugininfoprovider.h>
#include <psiaccountcontroller.h>
#include <stanzasender.h>
#include <stanzasendinghost.h>

#include <QTimer>

namespace massdelivery {

class Options;

class MassDelivery : public QObject
				   , public PsiPlugin
				   , public PluginInfoProvider
				   , public PsiAccountController
				   , public AccountInfoAccessor
				   , public ApplicationInfoAccessor
				   , public IconFactoryAccessor
				   , public StanzaSender
				   , public ContactInfoAccessor
{
	Q_OBJECT
#ifdef HAVE_QT5
	Q_PLUGIN_METADATA(IID "com.psi-plus.MassDelivery")
#endif
	Q_INTERFACES(PsiPlugin
				 PluginInfoProvider
				 PsiAccountController
				 AccountInfoAccessor
				 ApplicationInfoAccessor
				 IconFactoryAccessor
				 StanzaSender
				 ContactInfoAccessor)
public:
	MassDelivery();
	~MassDelivery();

	// from PsiPlugin
	QString name() const { return "Mass Delivery"; }
	QString shortName() const { return "massdelivery"; }
	QString version() const { return "0.0.2"; }

	QWidget *options();
	bool enable();
	bool disable();
	void applyOptions();
	void restoreOptions();
	QPixmap icon() const;

	// from PluginInfoProvider
	QString pluginInfo();

	// from PsiAccountController
	void setPsiAccountControllingHost(PsiAccountControllingHost *host) { _accountHost = host; }

	// from AccountInfoAccessor
	void setAccountInfoAccessingHost(AccountInfoAccessingHost* host) { _accountInfo = host; }

	// from ApplicationInfoAccessor
	void setApplicationInfoAccessingHost(ApplicationInfoAccessingHost *host);

	// from IconFactoryAccessor
	void setIconFactoryAccessingHost(IconFactoryAccessingHost *host) { _iconFactory = host; }

	// from StanzaSender
	void setStanzaSendingHost(StanzaSendingHost *host) { _stanzaSending = host; }

	// from ContactInfoAccessor
	void setContactInfoAccessingHost(ContactInfoAccessingHost *host) override { _contactInfoHost = host; }

private:

	bool _enabled;
	PsiAccountControllingHost *_accountHost;
	AccountInfoAccessingHost *_accountInfo;
	ApplicationInfoAccessingHost *_appInfo;
	IconFactoryAccessingHost *_iconFactory;
	StanzaSendingHost *_stanzaSending;
	ContactInfoAccessingHost *_contactInfoHost;

	int _counter;
	int _accountId;
	QString _jidToSend;
	Options *_optionsForm;
};

} // namespace massdelivery
