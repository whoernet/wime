/*
 * psiotrconfig.h - Configuration dialogs
 *
 * Off-the-Record Messaging plugin for Psi+
 * Copyright (C) 2007-2011  Timo Engel (timo-e@freenet.de)
 *                    2011  Florian Fieber
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef PSIOTRCONFIG_H_
#define PSIOTRCONFIG_H_

#include "otrmessaging.h"

#include <QWidget>
#include <QVariant>

class OptionAccessingHost;
class AccountInfoAccessingHost;
class QButtonGroup;
class QComboBox;
class QCheckBox;
class QStandardItemModel;
class QTableView;
class QPoint;

namespace Ui { class ConfigOtrWidget; }

// ---------------------------------------------------------------------------

namespace psiotr
{

// ---------------------------------------------------------------------------

const QString  OPTION_POLICY            = "otr-policy";
const QVariant DEFAULT_POLICY           = QVariant(OTR_POLICY_ENABLED);

const QString  OPTION_END_WHEN_OFFLINE  = "end-session-when-offline";
const QVariant DEFAULT_END_WHEN_OFFLINE = QVariant(false);

const QString  OPTION_END_WHEN_LOGOUT   = "end-session-when-logout";
const QVariant DEFAULT_END_WHEN_LOGOUT  = QVariant(false);

const QString  OPTION_ENABLE_AUTOREPLAY_ENCRYPTED   = "enable-autoreplay-encrypted";
const QVariant DEFAULT_ENABLE_AUTOREPLAY_ENCRYPTED  = QVariant(false);

const QString  OPTION_AUTOREPLAY_ENCRYPTED_TEXT   = "autoreplay-encrypted-text";
const QVariant DEFAULT_AUTOREPLAY_ENCRYPTED_TEXT  = QVariant(QString());

class ConfigOtrWidget;

// ---------------------------------------------------------------------------

/**
 * This dialog appears in the 'Plugins' section of the Psi configuration.
 */
class ConfigDialog : public QWidget
{
Q_OBJECT

public:
    ConfigDialog(OtrMessaging* otr, OptionAccessingHost* optionHost,
                 AccountInfoAccessingHost* accountInfo,
                 QWidget* parent = 0);

	QSize sizeHint() const;

	void saveSettings();

private:
    OtrMessaging*             m_otr;
    OptionAccessingHost*      m_optionHost;
    AccountInfoAccessingHost* m_accountInfo;
	ConfigOtrWidget *_configTab;
};

// ---------------------------------------------------------------------------

/**
 * Configure OTR policy.
 */
class ConfigOtrWidget : public QWidget
{
Q_OBJECT

public:
    ConfigOtrWidget(OptionAccessingHost* optionHost,
                    OtrMessaging* otr,
                    QWidget* parent = 0);

	~ConfigOtrWidget();

	void saveSettings();

private slots:
	void hack();

private:
	Ui::ConfigOtrWidget *ui;
	OptionAccessingHost *m_optionHost;
	OtrMessaging *m_otr;
	QButtonGroup *m_policy;
};

// ---------------------------------------------------------------------------

/**
 * Show fingerprint of your contacts.
 */
class FingerprintWidget : public QWidget
{
Q_OBJECT

public:
    FingerprintWidget(OtrMessaging* otr, QWidget* parent = 0);

protected:
    void updateData();

private:
    OtrMessaging*       m_otr;
    QTableView*         m_table;
    QStandardItemModel* m_tableModel;
    QList<Fingerprint>  m_fingerprints;

private slots:
    void deleteFingerprint();
    void verifyFingerprint();
    void copyFingerprint();
    void contextMenu(const QPoint& pos);
};

// ---------------------------------------------------------------------------

/**
 * Display a table with account and fingerprint of private key.
 */
class PrivKeyWidget : public QWidget
{
Q_OBJECT

public:
    PrivKeyWidget(AccountInfoAccessingHost* accountInfo,
                  OtrMessaging* otr, QWidget* parent);

protected:
    void updateData();

private:
    AccountInfoAccessingHost* m_accountInfo;
    OtrMessaging*             m_otr;
    QTableView*               m_table;
    QStandardItemModel*       m_tableModel;
    QHash<QString, QString>   m_keys;
    QComboBox*                m_accountBox;

private slots:
    void deleteKey();
    void generateKey();
    void copyFingerprint();
    void contextMenu(const QPoint& pos);
};

//-----------------------------------------------------------------------------

} // namespace psiotr

#endif
