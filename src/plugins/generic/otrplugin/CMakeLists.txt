set(PLUGIN otrplugin)

if(NOT WIN32)
    set(LIB_SUFFIX "" CACHE STRING "Define suffix of directory name (32/64)")
    set(PLUGINS_PATH "lib${LIB_SUFFIX}/psi-plus/plugins" CACHE STRING "Install suffix for plugins")
else()
    set(PLUGINS_PATH "psi-plus/plugins" CACHE STRING "Install suffix for plugins")
endif()

if(${PROJECT_SOURCE_DIR})
    set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH}" "${PROJECT_SOURCE_DIR}/cmake/modules" )
else()
    set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH}" "${CMAKE_CURRENT_SOURCE_DIR}/../../../../cmake/modules")
endif()

find_package(LibGcrypt REQUIRED)
find_package(LibOtr REQUIRED 4.0.0)
find_package(LibTidy REQUIRED)
find_package(LibGpgError REQUIRED)

add_definitions(
    -DQT_PLUGIN
    ${LIBOTR_DEFINITIONS}
    ${LIBTIDY_DEFINITIONS}
    ${LIBGCRYPT_DEFINITIONS}
    ${LIBGPGERROR_DEFINITIONS}
)

include_directories(
    ${CMAKE_SOURCE_DIR}
    ${CMAKE_CURRENT_BINARY_DIR}
    ${LIBOTR_INCLUDE_DIR}
    ${LIBTIDY_INCLUDE_DIR}
    ${LIBGCRYPT_INCLUDE_DIR}
    ${LIBGPGERROR_INCLUDE_DIR}
    ${TIDY_INCLUDES}
    ../../include
    ./src
    .
)

set(_UIS
    src/configotrwidget.ui
)

set(_HDRS
    src/psiotrplugin.h
    src/psiotrconfig.h
    src/psiotrclosure.h
)

set(_PLAIN_HDRS
    src/otrinternal.h
    src/otrlextensions.h
    src/otrmessaging.h
    src/htmltidy.h
)

set(_SRCS
    src/psiotrplugin.cpp
    src/otrmessaging.cpp
    src/otrinternal.cpp
    src/psiotrconfig.cpp
    src/psiotrclosure.cpp
    src/htmltidy.cpp
    src/otrlextensions.c
)

set(_RSCS
    ${PLUGIN}.qrc
)

if(QT5_BUILD)
    find_package(Qt5Core REQUIRED)
    find_package(Qt5Gui REQUIRED)
    find_package(Qt5Widgets REQUIRED)
    find_package(Qt5Xml REQUIRED)
    find_package(Qt5Concurrent REQUIRED)

    add_definitions(-DHAVE_QT5)

    macro(qt4_add_resources)
        qt5_add_resources(${ARGN})
    endmacro()

    macro(qt4_wrap_cpp)
        qt5_wrap_cpp(${ARGN})
    endmacro()

    macro(qt4_wrap_ui)
        qt5_wrap_ui(${ARGN})
    endmacro()
    set(QT_DEPLIBS Qt5::Core Qt5::Gui Qt5::Widgets Qt5::Xml Qt5::Concurrent)
else()
    find_package(Qt4 REQUIRED)
    include(${QT_USE_FILE})
    set(QT_DEPLIBS ${QT_LIBRARIES})
endif()

qt4_wrap_cpp(MOCS ${_HDRS})
qt4_wrap_ui(UIS ${_UIS})
qt4_add_resources(RSCS ${_RSCS})

add_library(${PLUGIN} MODULE ${_SRCS} ${_HDRS} ${_PLAIN_HDRS} ${UIS} ${RSCS} ${_RSCS} ${MOCS})

target_link_libraries(
    ${PLUGIN}
    ${QT_DEPLIBS}
    ${LIBOTR_LIBRARY}
    ${LIBTIDY_LIBRARY}
    ${LIBGCRYPT_LIBRARY}
    ${LIBGPGERROR_LIBRARY}
)

if( UNIX AND NOT( APPLE OR CYGWIN ) )
    install(
        TARGETS
        ${PLUGIN}
        LIBRARY
        DESTINATION
        ${CMAKE_INSTALL_PREFIX}/${PLUGINS_PATH}
        RUNTIME DESTINATION
        ${CMAKE_INSTALL_PREFIX}/${PLUGINS_PATH}
    )
endif()

if(WIN32)
    install(
        TARGETS
        ${PLUGIN}
        LIBRARY
        DESTINATION ${CMAKE_INSTALL_PREFIX}/${PLUGINS_PATH}
        RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/${PLUGINS_PATH}
    )
endif()
