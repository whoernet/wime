/*
 * watcheditem.cpp - plugin
 * Copyright (C) 2010  Khryukin Evgeny
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "watcheditem.h"

namespace watcher {

WatchedItem::WatchedItem(QListWidget *parent)
	: QListWidgetItem(parent)
	, jid_("")
	, text_("")
	, sFile_("")
	, aUse_(false)
	, groupChat_(false)
	, _scriptEnabled(false)
	, _scriptPath()
	, _scriptArgument()
	, _scriptExtraArgs(0)
{
}

WatchedItem::WatchedItem(const QString &jid, const QString &text, const QString &sFile, bool aUse, QListWidget *parent)
	: QListWidgetItem(parent)
	, jid_(jid)
	, text_(text)
	, sFile_(sFile)
	, aUse_(aUse)
	, groupChat_(false)
	, _scriptEnabled(false)
	, _scriptPath()
	, _scriptArgument()
	, _scriptExtraArgs(0)
	, _templateEnabled(false)
	, _templateText()
	, _templateDelay(0)
	, _forwardEnabled(false)
	, _forwardJid()
	, _forwardArgs(0)
{
}

QString WatchedItem::settingsString() const
{
	QStringList l = QStringList() << jid_ << text_ << sFile_;
	l << (aUse_ ? "1" : "0");
	l << (groupChat_ ? "1" : "0");
	l << (_scriptEnabled ? "1" : "0") << _scriptPath << _scriptArgument << QString::number(_scriptExtraArgs);
	l << (_templateEnabled ? "1" : "0") << _templateText << QString::number(_templateDelay);
	l << (_forwardEnabled ? "1" : "0") << _forwardJid << QString::number(_forwardArgs);
	return l.join(splitStr);
}

void WatchedItem::setSettings(const QString &settings)
{
	QStringList l = settings.split(splitStr);
	if(!l.isEmpty())
		jid_ = l.takeFirst();
	if(!l.isEmpty())
		text_ = l.takeFirst();
	if(!l.isEmpty())
		sFile_ = l.takeFirst();
	if(!l.isEmpty())
		aUse_ = l.takeFirst().toInt();
	if(!l.isEmpty())
		groupChat_ = l.takeFirst().toInt();
	if(!l.isEmpty()) {
		_scriptEnabled = l.takeFirst().toInt();
		_scriptPath = l.takeFirst();
		_scriptArgument = l.takeFirst();
	}
	if (!l.isEmpty())
		_scriptExtraArgs = l.takeFirst().toInt();
	if(!l.isEmpty()) {
		_templateEnabled = l.takeFirst().toInt();
		_templateText = l.takeFirst();
		_templateDelay = l.takeFirst().toInt();
	}
	if(!l.isEmpty()) {
		_forwardEnabled = l.takeFirst().toInt();
		_forwardJid = l.takeFirst();
		_forwardArgs = l.takeFirst().toInt();
	}

}

void WatchedItem::setScriptEnabled(bool b)
{
	_scriptEnabled = b;
}

bool WatchedItem::scriptEnabled() const
{
	return _scriptEnabled;
}

void WatchedItem::setScriptPath(const QString &path)
{
	_scriptPath = path;
}

QString WatchedItem::scriptPath() const
{
	return _scriptPath;
}

void WatchedItem::setScriptArgument(const QString &argument)
{
	_scriptArgument = argument;
}

QString WatchedItem::scriptArgument() const
{
	return _scriptArgument;
}

void WatchedItem::setScriptExtraArgs(int flags)
{
	_scriptExtraArgs = flags;
}

int WatchedItem::scriptExtraArgs() const
{
	return _scriptExtraArgs;
}

bool WatchedItem::hasScriptSender() const
{
	return _scriptExtraArgs & static_cast<int>(ScriptArg::SenderArg);
}

bool WatchedItem::hasScriptMessage() const
{
	return _scriptExtraArgs & static_cast<int>(ScriptArg::MessageArg);
}

bool WatchedItem::hasScriptStatus() const
{
	return _scriptExtraArgs & static_cast<int>(ScriptArg::StatusArg);
}

void WatchedItem::setTemplateEnabled(bool b)
{
	_templateEnabled = b;
}

bool WatchedItem::templateEnabled() const
{
	return _templateEnabled;
}

void WatchedItem::setTemplateText(const QString &templateText)
{
	_templateText = templateText;
}

QString WatchedItem::templateText() const
{
	return _templateText;
}

void WatchedItem::setTemplateDelay(int sec)
{
	_templateDelay = sec;
}

int WatchedItem::templateDelay() const
{
	return _templateDelay;
}

void WatchedItem::setForwardEnabled(bool b)
{
	_forwardEnabled = b;
}

bool WatchedItem::forwardEnabled() const
{
	return _forwardEnabled;
}

void WatchedItem::setForwardJid(const QString &jid)
{
	_forwardJid = jid;
}

QString WatchedItem::forwardJid() const
{
	return _forwardJid;
}

void WatchedItem::setForwardArgs(int flags)
{
	_forwardArgs = flags;
}

int WatchedItem::forwardArgs() const
{
	return _forwardArgs;
}

bool WatchedItem::hasForwardSender() const
{
	return _forwardArgs & static_cast<int>(ForwardArg::SenderArg);
}

bool WatchedItem::hasForwardMessage() const
{
	return _forwardArgs & static_cast<int>(ForwardArg::MessageArg);
}

bool WatchedItem::hasForwardStatus() const
{
	return _forwardArgs & static_cast<int>(ForwardArg::StatusArg);
}

WatchedItem* WatchedItem::copy()
{
	WatchedItem *wi = new WatchedItem();
	wi->setWatchedText(text_);
	wi->setJid(jid_);
	wi->setUse(aUse_);
	wi->setSFile(sFile_);
	wi->setText(text());
	wi->setGroupChat(groupChat_);

	wi->setScriptEnabled(_scriptEnabled);
	wi->setScriptPath(_scriptPath);
	wi->setScriptArgument(_scriptArgument);
	wi->setScriptExtraArgs(_scriptExtraArgs);

	wi->setTemplateEnabled(_templateEnabled);
	wi->setTemplateText(_templateText);
	wi->setTemplateDelay(_templateDelay);

	wi->setForwardEnabled(_forwardEnabled);
	wi->setForwardJid(_forwardJid);
	wi->setForwardArgs(_forwardArgs);

	return wi;
}

} // namespace watcher
