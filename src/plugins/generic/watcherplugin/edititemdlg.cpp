/*
 * edititemdlg.cpp - plugin
 * Copyright (C) 2010  Khryukin Evgeny
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "edititemdlg.h"
#include "iconfactoryaccessinghost.h"
#include "optionaccessinghost.h"
#include "watcheditem.h"

#include <QFileDialog>

namespace watcher {

EditItemDlg::EditItemDlg(IconFactoryAccessingHost* icoHost, OptionAccessingHost *psiOptions_, const QStringList &jids, QWidget *p)
	: QDialog(p, Qt::Window)
	, psiOptions(psiOptions_)
{
	setAttribute(Qt::WA_DeleteOnClose);
	//setModal(true);
	ui_.setupUi(this);
	ui_.tb_open->setIcon(icoHost->getIcon("psi/browse"));
	ui_.tb_test->setIcon(icoHost->getIcon("psi/play"));

	ui_.tbScriptPath->setIcon(icoHost->getIcon("psi/browse"));

#ifdef Q_OS_WIN
	ui_.gbScript->hide();
#endif

	connect(ui_.tb_test, SIGNAL(pressed()), SLOT(doTestSound()));
	connect(ui_.tb_open, SIGNAL(pressed()), SLOT(getFileName()));

	connect(ui_.tbScriptPath, SIGNAL(pressed()), SLOT(chooseScriptPath()));

	for (const auto &jid: jids) {
		ui_.cmbForwardJid->addItem(jid);
	}
}

void EditItemDlg::init(const QString &settings)
{
	QStringList l = settings.split(splitStr);
	if(!l.isEmpty()) {
		ui_.le_jid->setText(l.takeFirst());
		ui_.le_jid->setEnabled(!ui_.le_jid->text().isEmpty());
		ui_.rb_text->setChecked(ui_.le_jid->text().isEmpty());
	}
	if(!l.isEmpty()) {
		ui_.te_text->setText(l.takeFirst());
		ui_.te_text->setEnabled(!ui_.te_text->toPlainText().isEmpty());
		ui_.rb_jid->setChecked(ui_.te_text->toPlainText().isEmpty());
	}
	if(!l.isEmpty())
		ui_.le_sound->setText(l.takeFirst());
	if(!l.isEmpty())
		ui_.cb_always_play->setChecked(l.takeFirst().toInt());
	if(!l.isEmpty())
		ui_.rb_groupchat->setChecked(l.takeFirst().toInt());
	if(!l.isEmpty()) {
		ui_.gbScript->setChecked(l.takeFirst().toInt());
		ui_.lneScriptPath->setText(l.takeFirst());
		ui_.lneArgument->setText(l.takeFirst());
	}
	if(!l.isEmpty()) {
		int extraArgs = l.takeFirst().toInt();
		ui_.cbScriptSender->setChecked(extraArgs & static_cast<int>(WatchedItem::ScriptArg::SenderArg));
		ui_.cbScriptMessage->setChecked(extraArgs & static_cast<int>(WatchedItem::ScriptArg::MessageArg));
		ui_.cbScriptStatus->setChecked(extraArgs & static_cast<int>(WatchedItem::ScriptArg::StatusArg));
	}

	// Template
	if(!l.isEmpty()) {
		ui_.gbTemplate->setChecked(l.takeFirst().toInt());
		ui_.pteTemplate->setPlainText(l.takeFirst());
		ui_.spnTemplateDelay->setValue(l.takeFirst().toInt());
	}

	// Forward to
	if(!l.isEmpty()) {
		ui_.gbForward->setChecked(l.takeFirst().toInt());
		ui_.cmbForwardJid->setEditText(l.takeFirst());

		int args = l.takeFirst().toInt();
		ui_.cbForwardSender->setChecked(args & static_cast<int>(WatchedItem::ScriptArg::SenderArg));
		ui_.cbForwardMessage->setChecked(args & static_cast<int>(WatchedItem::ScriptArg::MessageArg));
		ui_.cbForwardStatus->setChecked(args & static_cast<int>(WatchedItem::ScriptArg::StatusArg));
	}
}

void EditItemDlg::getFileName()
{
	QString fileName = QFileDialog::getOpenFileName(0,tr("Choose a sound file"),
													psiOptions->getPluginOption(constLastFile, QVariant("")).toString(),
													tr("Sound (*.wav)"));
	if(fileName.isEmpty())
		return;
	QFileInfo fi(fileName);
	psiOptions->setPluginOption(constLastFile, QVariant(fi.absolutePath()));
	ui_.le_sound->setText(fileName);
}

void EditItemDlg::doTestSound()
{
	emit testSound(ui_.le_sound->text());
}

void EditItemDlg::chooseScriptPath()
{
	QString path = ui_.lneScriptPath->text();
	path = QFileDialog::getOpenFileName(0, tr("Choose a shell script"), path, tr("Shell script"));
	ui_.lneScriptPath->setText(path);
}

void EditItemDlg::accept()
{
	QString str = (ui_.rb_jid->isChecked() ? ui_.le_jid->text() : "") + splitStr;
	str += (ui_.rb_text->isChecked() ? ui_.te_text->toPlainText() : "") + splitStr;
	str += ui_.le_sound->text() + splitStr;
	str += (ui_.cb_always_play->isChecked() ? "1" : "0") + splitStr;
	str += (ui_.rb_groupchat->isChecked() ? "1" : "0") + splitStr;
	str += (ui_.gbScript->isChecked() ? "1" : "0") + splitStr;
	str += ui_.lneScriptPath->text() + splitStr;
	str += ui_.lneArgument->text() + splitStr;
	int flags = 0;
	if (ui_.cbScriptSender->isChecked())
		flags |= static_cast<int>(WatchedItem::ScriptArg::SenderArg);
	if (ui_.cbScriptMessage->isChecked())
		flags |= static_cast<int>(WatchedItem::ScriptArg::MessageArg);
	if (ui_.cbScriptStatus->isChecked())
		flags |= static_cast<int>(WatchedItem::ScriptArg::StatusArg);
	str += QString::number(flags) + splitStr;

	// Template
	str += (ui_.gbTemplate->isChecked() ? "1" : "0") + splitStr;
	str += ui_.pteTemplate->toPlainText() + splitStr;
	str += QString::number(ui_.spnTemplateDelay->value()) + splitStr;

	// Forward to
	str += (ui_.gbForward->isChecked() ? "1" : "0") + splitStr;
	str += ui_.cmbForwardJid->currentText() + splitStr;
	flags = 0;
	if (ui_.cbForwardSender->isChecked())
		flags |= static_cast<int>(WatchedItem::ForwardArg::SenderArg);
	if (ui_.cbForwardMessage->isChecked())
		flags |= static_cast<int>(WatchedItem::ForwardArg::MessageArg);
	if (ui_.cbForwardStatus->isChecked())
		flags |= static_cast<int>(WatchedItem::ForwardArg::StatusArg);
	str += QString::number(flags);

	emit dlgAccepted(str);
	close();
}

} // namespace watcher
