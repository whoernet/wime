/*
 * watcherplugin.cpp - plugin
 * Copyright (C) 2010  Khryukin Evgeny
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#include "watcherplugin.h"

#include "view.h"
#include "model.h"
#include "edititemdlg.h"

#include <QFileDialog>
#include <QDomElement>
#include <QHash>
#include <QTimer>
#include <QProcess>

#define constVersion "0.4.7"

#define constSoundFile "sndfl"
#define constInterval "intrvl"
#define constCount "count"
#define constSndFiles "sndfiles"
#define constJids "jids"
#define constEnabledJids "enjids"
#define constWatchedItems "watcheditem"
#define constDisableSnd "dsblsnd"
#define constDisablePopupDnd "dsblpopupdnd"
#define constShowInContext "showincontext"

#define POPUP_OPTION_NAME "Watcher"

#define constEnableScript "enablescript"
#define constScriptFile "scriptpath"

namespace watcher {

#ifndef HAVE_QT5
Q_EXPORT_PLUGIN2(Watcher, Watcher)
#endif

Watcher::Watcher()
	: psiOptions(0)
	, popup(0)
	, icoHost(0)
	, appInfoHost(0)
	, activeTab(0)
	, contactInfo(0)
	, accInfo(0)
	, sound_(0)
	, _psiAccount(nullptr)
	, enabled(false)
	, soundFile("sound/watcher.wav")
//, Interval(2)
	, model_(0)
	, isSndEnable(false)
	, disableSnd(true)
	, disablePopupDnd(true)
	, popupId(0)
	, _enabledScript(false)
	, _scriptPath()
{
}

QString Watcher::name() const
{
	return "Watcher";
}

QString Watcher::shortName() const
{
	return "watcher";
}

QString Watcher::version() const
{
	return constVersion;
}

bool Watcher::enable()
{
	if (psiOptions) {
		enabled = true;
		soundFile = psiOptions->getPluginOption(constSoundFile, QVariant(soundFile)).toString();
		disableSnd = psiOptions->getPluginOption(constDisableSnd, QVariant(disableSnd)).toBool();
		disablePopupDnd = psiOptions->getPluginOption(constDisablePopupDnd, QVariant(disablePopupDnd)).toBool();

		_enabledScript = psiOptions->getPluginOption(constEnableScript, QVariant(_enabledScript)).toBool();
		_scriptPath = psiOptions->getPluginOption(constScriptFile, QVariant(_scriptPath)).toString();

		int interval = psiOptions->getPluginOption(constInterval, QVariant(3000)).toInt()/1000;
		popupId = popup->registerOption(POPUP_OPTION_NAME, interval, "plugins.options."+shortName()+"."+constInterval);

		QStringList jids = psiOptions->getPluginOption(constJids, QVariant(QStringList())).toStringList();
		QStringList soundFiles = psiOptions->getPluginOption(constSndFiles, QVariant(QStringList())).toStringList();
		QStringList enabledJids = psiOptions->getPluginOption(constEnabledJids, QVariant(QStringList())).toStringList();
		if (enabledJids.isEmpty()) {
			for (int i = 0; i < jids.size(); i++) {
				enabledJids << "true";
			}
		}

		if (!model_) {
			model_ = new Model(jids, soundFiles, enabledJids, this);
			connect(model_, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(Hack()));
		}

		items_.clear();
		QStringList list = psiOptions->getPluginOption(constWatchedItems).toStringList();

		foreach (const QString& settings, list) {
			WatchedItem* wi = new WatchedItem();
			wi->setSettings(settings);
			items_.push_back(wi);
			if(!wi->jid().isEmpty())
				wi->setText(wi->jid());
			else if(!wi->watchedText().isEmpty())
				wi->setText(wi->watchedText());
			else
				wi->setText(tr("Empty item"));
		}

		QStringList files;
		files << "watcher_on" << "watcher";
		foreach (QString filename, files) {

			QFile file(":/icons/" + filename + ".png");
			file.open(QIODevice::ReadOnly);
			QByteArray image = file.readAll();
			icoHost->addIcon("watcher/" + filename, image);
			file.close();
		}

		showInContext_ = psiOptions->getPluginOption(constShowInContext, QVariant(true)).toBool();
	}

	return enabled;
}

bool Watcher::disable()
{
	delete model_;
	model_ = 0;

	qDeleteAll(items_);
	foreach (QAction* action, actions_) {
		action->disconnect();
		action->deleteLater();
	}
	items_.clear();
	actions_.clear();

	popup->unregisterOption(POPUP_OPTION_NAME);
	enabled = false;
	return true;
}

QWidget* Watcher::options()
{
	if (!enabled) {
		return 0;
	}
	optionsWid = new QWidget();
	connect(optionsWid, SIGNAL(destroyed()), this, SLOT(onOptionsClose()));

	ui_.setupUi(optionsWid);

	restoreOptions();

	ui_.cb_hack->setVisible(false);
	ui_.tb_open->setIcon(icoHost->getIcon("psi/browse"));
	ui_.tb_test->setIcon(icoHost->getIcon("psi/play"));
	ui_.tbScriptPath->setIcon(icoHost->getIcon("psi/browse"));

	// Hidden for a while
	// It may be needed in the future
	ui_.wdgScript->setVisible(false);

	ui_.pb_add->setIcon(icoHost->getIcon("psi/addContact"));
	ui_.pb_del->setIcon(icoHost->getIcon("psi/remove"));
	ui_.pb_add_item->setIcon(icoHost->getIcon("psi/addContact"));
	ui_.pb_delete_item->setIcon(icoHost->getIcon("psi/remove"));
	ui_.pb_edit_item->setIcon(icoHost->getIcon("psi/action_templates_edit"));

	ui_.tableView->setModel(model_);
	ui_.tableView->init(icoHost);

	ui_.cb_showInContext->setChecked(showInContext_);

	connect(ui_.tableView, SIGNAL(checkSound(QModelIndex)), this, SLOT(checkSound(QModelIndex)));
	connect(ui_.tableView, SIGNAL(getSound(QModelIndex)), this, SLOT(getSound(QModelIndex)));
	connect(ui_.tb_test, SIGNAL(pressed()), this, SLOT(checkSound()));
	connect(ui_.tb_open, SIGNAL(pressed()), this, SLOT(getSound()));
	connect(ui_.pb_add, SIGNAL(released()), this, SLOT(addLine()));
	connect(ui_.pb_del, SIGNAL(released()), this, SLOT(delSelected()));
	connect(ui_.cbScript, SIGNAL(toggled(bool)), SLOT(setScriptEnabled(bool)));
	connect(ui_.tbScriptPath, SIGNAL(pressed()), SLOT(chooseScriptPath()));

	connect(ui_.pb_add_item, SIGNAL(clicked()), this, SLOT(addItemAct()));
	connect(ui_.pb_delete_item, SIGNAL(clicked()), this, SLOT(delItemAct()));
	connect(ui_.pb_edit_item, SIGNAL(clicked()), this, SLOT(editItemAct()));
	connect(ui_.listWidget, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(editItemAct()));

	return optionsWid;
}

void Watcher::addLine()
{
	model_->addRow();
	Hack(); //activate apply button
}

void Watcher::delSelected()
{
	ui_.tableView->deleteSelected();
	Hack(); //activate apply button
}

void Watcher::applyOptions()
{
	soundFile = ui_.le_sound->text();
	psiOptions->setPluginOption(constSoundFile, QVariant(soundFile));

	disableSnd = ui_.cb_disable_snd->isChecked();
	psiOptions->setPluginOption(constDisableSnd, QVariant(disableSnd));

	//	Interval = ui_.sb_delay->value();
	//	psiOptions->setPluginOption(constInterval,QVariant(Interval));

	disablePopupDnd = ui_.cb_disableDnd->isChecked();
	psiOptions->setPluginOption(constDisablePopupDnd, QVariant(disablePopupDnd));


	psiOptions->setPluginOption(constEnableScript, QVariant(_enabledScript));
	psiOptions->setPluginOption(constScriptFile, QVariant(_scriptPath));

	model_->apply();
	psiOptions->setPluginOption(constEnabledJids, QVariant(model_->getEnabledJids()));
	psiOptions->setPluginOption(constJids, QVariant(model_->getWatchedJids()));
	psiOptions->setPluginOption(constSndFiles, QVariant(model_->getSounds()));

	foreach (WatchedItem *wi, items_)
		delete(wi);
	items_.clear();
	QStringList l;
	for (int i = 0; i < ui_.listWidget->count(); i++) {
		WatchedItem *wi = (WatchedItem*)ui_.listWidget->item(i);
		if(wi) {
			items_.push_back(wi->copy());
			l.push_back(wi->settingsString());
		}
	}

	psiOptions->setPluginOption(constWatchedItems, QVariant(l));

	showInContext_ = ui_.cb_showInContext->isChecked();

	psiOptions->setPluginOption(constShowInContext, QVariant(showInContext_));
}

void Watcher::restoreOptions()
{
	ui_.le_sound->setText(soundFile);
	//	ui_.sb_delay->setValue(Interval);
	ui_.cb_disable_snd->setChecked(disableSnd);
	ui_.cb_disableDnd->setChecked(disablePopupDnd);
	model_->reset();
	ui_.listWidget->clear();
	foreach (WatchedItem* wi, items_) {
		ui_.listWidget->addItem(wi->copy());
	}

	ui_.lneScriptPath->setText(_scriptPath);
	ui_.cbScript->setChecked(_enabledScript);
	setScriptEnabled(_enabledScript);
}

QPixmap Watcher::icon() const
{
	return QPixmap(":/icons/watcher.png");
}

bool Watcher::incomingStanza(int acc, const QDomElement &stanza)
{
	if (!enabled)
		return false;

	if (stanza.tagName() == "presence") {
		if (stanza.attribute("type") == "error")
			return false;

		QString from = stanza.attribute("from");
		if (from.isEmpty())
			return false;

		bool find = false;
		int index = model_->indexByJid(from);
		if (index >= 0) {
			if (model_->getEnabledJids().at(index) == "true") {
				find = true;
			}
		}
		else {
			from = from.split("/").takeFirst();
			index = model_->indexByJid(from);
			if (index >= 0) {
				if (model_->getEnabledJids().at(index) == "true") {
					find = true;
				}
			}
		}

		if (find) {
			QString status = stanza.firstChildElement("show").text();
			if (status.isEmpty()) {
				if (stanza.attribute("type") == "unavailable") {
					status = "offline";
				}
				else {
					status = "online";
					if (model_->statusByJid(from) != status && psiOptions->getGlobalOption("options.ui.notifications.sounds.enable").toBool()) {
						QString snd = model_->soundByJid(from);
						if(snd.isEmpty())
							snd = soundFile;
						playSound(snd);
					}
				}
			}
			if (model_->statusByJid(from) != status) {
				model_->setStatusForJid(from, status);
				status[0] = status[0].toUpper();
				from = stanza.attribute("from"); // нужно быть уверенным, что у нас полный джид
				const QString bare = from.split("/").first();
				QString nick = contactInfo->name(acc, bare);
				QString text;
				if (!nick.isEmpty())
					from = " [" + from + "]";
				text = nick + from + tr(" change status to ") + status;
				QMetaObject::invokeMethod(this, "showPopup", Qt::QueuedConnection,
										  Q_ARG(int, acc),
										  Q_ARG(const QString&, bare),
										  Q_ARG(QString, text));
				execScript("presence", bare, status);
			}
		}
	}

	return false;
}

bool Watcher::outgoingStanza(int /*account*/, QDomElement& /*xml*/)
{
	return false;
}

bool Watcher::checkWatchedItem(int accountIndex, const QString &from, const QString &body, WatchedItem *wi, bool activeTab, const QString &status)
{
	if (!wi->jid().isEmpty() && from.contains(QRegExp(wi->jid(), Qt::CaseInsensitive, QRegExp::Wildcard))) {
		triggerWatchedItem(accountIndex, from, body, wi, status);

		isSndEnable = psiOptions->getGlobalOption("options.ui.notifications.sounds.enable").toBool();
		if (!activeTab && (wi->alwaysUse() || isSndEnable)) {
			playSound(wi->sFile());
			return true;
		}
	}
	if (!wi->watchedText().isEmpty()) {
		foreach (QString txt, wi->watchedText().split(QRegExp("\\s+"), QString::SkipEmptyParts)) {
			if (body.contains(QRegExp(txt, Qt::CaseInsensitive, QRegExp::Wildcard)) ) {
				if (!activeTab) {
					playSound(wi->sFile());
				}

				triggerWatchedItem(accountIndex, from, body, wi, status);

				return true;
			}
		}
	}
	return false;
}

void Watcher::triggerWatchedItem(int accountIndex, const QString &from, const QString &body, WatchedItem *wi, const QString &status)
{
	if (wi->scriptEnabled()) {
		QStringList arguments;
		arguments << wi->scriptArgument();
		if (wi->hasScriptSender())
			arguments << from;
		if (wi->hasScriptMessage())
			arguments << body;
		if (wi->hasScriptStatus())
			arguments << status;

		execWiScript(wi->scriptPath(), arguments);
	}

	if (wi->templateEnabled()) {
		QString message = wi->templateText();
		message.replace(QLatin1Char('\n'), QLatin1String("<br/>"));
		QTimer *timer = new QTimer;
		timer->setProperty("slotAccount", accountIndex);
		timer->setProperty("slotFrom", from);
		timer->setProperty("slotMessage", message);

		connect(timer, SIGNAL(timeout()), SLOT(sendMessage()));
		timer->start(wi->templateDelay() * 1000);
	}

	if (wi->forwardEnabled() && !wi->forwardJid().isEmpty()) {
		QStringList message;
		if (wi->hasForwardSender())
			message << from;

		if (wi->hasForwardMessage()) {
			QString htmlBody = body;
			htmlBody.replace(QLatin1Char('\n'), QLatin1String("<br/>"));
			message << htmlBody;
		}

		if (wi->hasForwardStatus())
			message << status;

		_psiAccount->appendMsg(accountIndex, wi->forwardJid(), message.join(QLatin1String("<br/>---------------------------------------------------------<br/>")));
	}
}


void Watcher::enableGlobalSounds()
{
	psiOptions->setGlobalOption("options.ui.notifications.sounds.enable", QVariant(true));
}

void Watcher::setPopupAccessingHost(PopupAccessingHost* host)
{
	popup = host;
}

void Watcher::setIconFactoryAccessingHost(IconFactoryAccessingHost* host)
{
	icoHost = host;
}

void Watcher::setActiveTabAccessingHost(ActiveTabAccessingHost *host)
{
	activeTab = host;
}

void Watcher::setOptionAccessingHost(OptionAccessingHost *host)
{
	psiOptions = host;
}

void Watcher::setApplicationInfoAccessingHost(ApplicationInfoAccessingHost* host)
{
	appInfoHost = host;
}

void Watcher::setContactInfoAccessingHost(ContactInfoAccessingHost *host)
{
	contactInfo = host;
}

void Watcher::optionChanged(const QString &option)
{
	Q_UNUSED(option);
}

void Watcher::setAccountInfoAccessingHost(AccountInfoAccessingHost *host)
{
	accInfo = host;
}

void Watcher::setSoundAccessingHost(SoundAccessingHost *host)
{
	sound_ = host;
}

QAction* Watcher::createAction(QObject *parent, const QString &contact)
{
	QStringList jids = model_->getWatchedJids();
	QAction *action;
	if (jids.contains(contact, Qt::CaseInsensitive) && model_->jidEnabled(contact)) {
		action = new QAction(QIcon(":/icons/watcher_on.png"), tr("Don't watch for JID"), parent);
		action->setProperty("watch", true);
	}
	else {
		action = new QAction(QIcon(":/icons/watcher.png"), tr("Watch for JID"), parent);
		action->setProperty("watch", false);
	}

	action->setProperty("jid", contact);
	connect(action, SIGNAL(triggered()), SLOT(actionActivated()));

	return action;
}

void Watcher::setPsiAccountControllingHost(PsiAccountControllingHost *host)
{
	_psiAccount = host;
}

QAction* Watcher::getAction(QObject *parent, int /*account*/, const QString &contact)
{
	if (!enabled) {
		return 0;
	}

	if (!actions_.contains(contact)) {
		QAction *action = createAction(parent, contact);
		connect(action, SIGNAL(destroyed(QObject*)), SLOT(removeFromActions(QObject*)));
		actions_[contact] = action;
	}
	return actions_[contact];
}

bool Watcher::processEvent(int accountIndex, QDomElement &e)
{
	Q_UNUSED(accountIndex);

	if (!enabled)
		return false;

	QDomElement stanza = e.firstChildElement(QLatin1String("message"));

	if (stanza.isNull())
		return false;

	QString body = stanza.firstChildElement("body").text();
	if(!body.isEmpty()) {

		QString from = stanza.attribute("from");
		QString type = stanza.attribute("type");
		bool activeTab = false;
		if (disableSnd) {
			QString jid = this->activeTab->getJid();
			activeTab = jid.split("/").first().toLower() == from.split("/").first().toLower();
		}

		if (type == "groupchat") {
			foreach(WatchedItem *wi, items_) {
				if (!wi->groupChat())
					continue;

				if (checkWatchedItem(accountIndex, from, body, wi, activeTab, contactInfo->status(accountIndex, from)))
					break;
			}
		}
		else {
			foreach (WatchedItem *wi, items_) {
				if (wi->groupChat())
					continue;

				QString bare = from.split('/').first();
				if (checkWatchedItem(accountIndex, bare, body, wi, activeTab, contactInfo->status(accountIndex, bare)))
					break;
			}
		}
	}

	return false;
}

bool Watcher::processMessage(int accountIndex, const QString &contact, const QString &body, const QString &subject)
{
	Q_UNUSED(accountIndex);
	Q_UNUSED(contact);
	Q_UNUSED(body);
	Q_UNUSED(subject);

	return false;
}

bool Watcher::processOutgoingMessage(int accountIndex, const QString &contact, QString &body, const QString &type, QString &subject)
{
	Q_UNUSED(accountIndex);
	Q_UNUSED(contact);
	Q_UNUSED(body);
	Q_UNUSED(type);
	Q_UNUSED(subject);

	return false;
}

void Watcher::logout(int accountIndex)
{
	Q_UNUSED(accountIndex);
}

void Watcher::actionActivated()
{
	QAction *action = qobject_cast<QAction*>(sender());
	if (action->property("watch").toBool()) {
		action->setProperty("watch", false);
		action->setIcon(QIcon(":/icons/watcher.png"));
		action->setText(tr("Watch for JID"));
		model_->setJidEnabled(action->property("jid").toString(), false);
	}
	else {
		action->setProperty("watch", true);
		action->setIcon(QIcon(":/icons/watcher_on.png"));
		action->setText(tr("Don't watch for JID"));
		model_->setJidEnabled(action->property("jid").toString().split('/').first(), true);
	}
	model_->apply();
	psiOptions->setPluginOption(constEnabledJids, QVariant(model_->getEnabledJids()));
	psiOptions->setPluginOption(constJids, QVariant(model_->getWatchedJids()));
	psiOptions->setPluginOption(constSndFiles, QVariant(model_->getSounds()));
}

void Watcher::removeFromActions(QObject *object)
{
	actions_.remove(object->property("jid").toString());
}

void Watcher::sendMessage()
{
	int account = sender()->property("slotAccount").toInt();
	QString from = sender()->property("slotFrom").toString();
	QString message = sender()->property("slotMessage").toString();

	_psiAccount->appendMsg(account, from, message);

	sender()->deleteLater();
}

void Watcher::execScript(const QString &type, const QString &jid, const QString &message)
{
	if (!_enabledScript)
		return;

	if (!QFile::exists(_scriptPath))
		return;


#ifdef Q_OS_UNIX
	QString program("/bin/sh");
#else
	QString program("cmd");
#endif

	QStringList arguments;
	arguments << _scriptPath << type << jid << message;
	QProcess::startDetached(program, arguments);
}

void Watcher::execWiScript(const QString &path, const QStringList &arguments)
{
	if (!QFile::exists(path))
		return;


#ifdef Q_OS_UNIX
	QString program("/bin/sh");
#else
	QString program("cmd");
#endif

	QStringList processArguments;
	processArguments << path;
	processArguments += arguments;

	QProcess::startDetached(program, processArguments);
}

void Watcher::playSound(const QString& f)
{
	bool isSndEnable = psiOptions->getGlobalOption("options.ui.notifications.sounds.enable").toBool();

	// Disable global notifications to prevent two sounds in the same time
	if (isSndEnable)
		psiOptions->setGlobalOption("options.ui.notifications.sounds.enable", QVariant(false));

	sound_->playSound(f);

	if (isSndEnable)
		QTimer::singleShot(500, this, SLOT(enableGlobalSounds())); // enable global notifications again
}

void Watcher::getSound(QModelIndex index)
{
	if(ui_.tb_open->isDown()) {
		QString fileName = QFileDialog::getOpenFileName(0,tr("Choose a sound file"),
														psiOptions->getPluginOption(constLastFile, QVariant("")).toString(),
														tr("Sound (*.wav)"));
		if(fileName.isEmpty()) return;
		QFileInfo fi(fileName);
		psiOptions->setPluginOption(constLastFile, QVariant(fi.absolutePath()));
		ui_.le_sound->setText(fileName);
	}
	else {
		QString fileName = QFileDialog::getOpenFileName(0,tr("Choose a sound file"),
														psiOptions->getPluginOption(constLastFile, QVariant("")).toString(),
														tr("Sound (*.wav)"));
		if(fileName.isEmpty()) return;
		QFileInfo fi(fileName);
		psiOptions->setPluginOption(constLastFile, QVariant(fi.absolutePath()));
		const QModelIndex editIndex = model_->index(index.row(), 2, QModelIndex());
		model_->setData(editIndex, QVariant(fileName));
	}
}

void Watcher::checkSound(QModelIndex index)
{
	if(ui_.tb_test->isDown()) {
		playSound(ui_.le_sound->text());
	}
	else {
		playSound(model_->tmpSoundFile(index));
	}
}

void Watcher::showPopup(int account, const QString& jid, QString text)
{
	QVariant suppressDnd = psiOptions->getGlobalOption("options.ui.notifications.passive-popups.suppress-while-dnd");
	psiOptions->setGlobalOption("options.ui.notifications.passive-popups.suppress-while-dnd", disablePopupDnd);

	int interval = popup->popupDuration(POPUP_OPTION_NAME);
	if(interval) {
		const QString statusMes = contactInfo->statusMessage(account, jid);
		if(!statusMes.isEmpty()) {
			text += tr("<br>Status Message: %1").arg(statusMes);
		}
		popup->initPopupForJid(account, jid, text, tr("Watcher"), "psi/search", popupId);
	}
	psiOptions->setGlobalOption("options.ui.notifications.passive-popups.suppress-while-dnd", suppressDnd);
}

void Watcher::chooseScriptPath()
{
	_scriptPath = QFileDialog::getOpenFileName(0, tr("Choose a shell script"), _scriptPath, tr("Shell script"));
	ui_.lneScriptPath->setText(_scriptPath);
}

void Watcher::setScriptEnabled(bool enabled)
{
	_enabledScript = enabled;
	ui_.tbScriptPath->setEnabled(enabled);
	ui_.lneScriptPath->setEnabled(enabled);

}

void Watcher::Hack()
{
	if (!optionsWid.isNull()) {
		ui_.cb_hack->toggle();
	}
}

void Watcher::onOptionsClose()
{
	model_->reset();
}

QList < QVariantHash > Watcher::getAccountMenuParam()
{
	return QList < QVariantHash >();
}

QList < QVariantHash > Watcher::getContactMenuParam()
{
	return QList < QVariantHash >();
}

QAction* Watcher::getContactAction(QObject *p, int /*account*/, const QString &jid)
{
	if (!enabled || !showInContext_) {
		return 0;
	}

	return createAction(p, jid);
}

void Watcher::addItemAct()
{
	QStringList jids = accInfo->getRoster(0);
	jids.sort();
	EditItemDlg *eid = new EditItemDlg(icoHost, psiOptions, jids, optionsWid);
	connect(eid, SIGNAL(testSound(QString)), this, SLOT(playSound(QString)));
	connect(eid, SIGNAL(dlgAccepted(QString)), this, SLOT(addNewItem(QString)));
	eid->show();
}

void Watcher::addNewItem(const QString& settings)
{
	WatchedItem *wi = new WatchedItem(ui_.listWidget);
	wi->setSettings(settings);
	if (!wi->jid().isEmpty())
		wi->setText(wi->jid());
	else if (!wi->watchedText().isEmpty())
		wi->setText(wi->watchedText());
	else
		wi->setText(tr("Empty item"));
	Hack();
}

void Watcher::delItemAct()
{
	WatchedItem *wi = (WatchedItem*)ui_.listWidget->currentItem();
	if(wi) {
		int index = items_.indexOf(wi);
		if(index != -1)
			items_.removeAt(index);

		delete(wi);
		Hack();
	}
}

void Watcher::editItemAct()
{
	WatchedItem *wi = (WatchedItem*)ui_.listWidget->currentItem();
	if (wi) {
		QStringList jids = accInfo->getRoster(0);
		jids.sort();
		EditItemDlg *eid = new EditItemDlg(icoHost, psiOptions, jids, optionsWid);
		eid->init(wi->settingsString());
		connect(eid, SIGNAL(testSound(QString)), this, SLOT(playSound(QString)));
		connect(eid, SIGNAL(dlgAccepted(QString)), this, SLOT(editCurrentItem(QString)));
		eid->show();
	}
}

void Watcher::editCurrentItem(const QString& settings)
{
	WatchedItem *wi = (WatchedItem*)ui_.listWidget->currentItem();
	if (wi) {
		wi->setSettings(settings);
		if (!wi->jid().isEmpty())
			wi->setText(wi->jid());
		else if (!wi->watchedText().isEmpty())
			wi->setText(wi->watchedText());
		else
			wi->setText(tr("Empty item"));
		Hack();
	}
}

QString Watcher::pluginInfo()
{
	return tr("Author: ") +	 "Dealer_WeARE\n"
	+ tr("Email: ") + "wadealer@gmail.com\n\n"
	+ trUtf8("This plugin is designed to monitor the status of specific roster contacts, as well as for substitution of standard sounds of incoming messages.\n"
			 "On the first tab set up a list of contacts for the status of which is monitored. When the status of such contacts changes a popup window will be shown"
			 " and when the status changes to online a custom sound can be played."
			 "On the second tab is configured list of items, the messages are being monitored. Each element can contain a regular expression"
			 " to check for matches with JID, from which the message arrives, a list of regular expressions to check for matches with the text"
			 " of an incoming message, the path to sound file which will be played in case of coincidence, as well as the setting, whether the sound"
			 " is played always, even if the global sounds off. ");
}

} // namespace watcher
