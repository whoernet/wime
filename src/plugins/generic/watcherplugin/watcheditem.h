/*
 * watcheditem.h - plugin
 * Copyright (C) 2010  Khryukin Evgeny
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#pragma once

#include <QListWidgetItem>

namespace watcher {

const QString splitStr = "&split&";

class WatchedItem : public QListWidgetItem
{
public:
	WatchedItem(QListWidget *parent = 0);
	WatchedItem(const QString& jid, const QString& text = QString(), const QString& sFile = QString(), bool aUse = false, QListWidget *parent = 0);
	QString settingsString() const;
	void setSettings(const QString& settings);
	void setJid(const QString& jid) { jid_ = jid; };
	void setWatchedText(const QString& text) { text_ = text; };
	void setSFile(const QString& sFile) { sFile_ = sFile; };
	void setUse(bool use) { aUse_ = use; };
	void setGroupChat(bool gc) { groupChat_ = gc; };
	QString jid() const { return jid_; };
	QString watchedText() const { return text_; };
	QString sFile() const { return sFile_; };
	bool alwaysUse() const { return aUse_; };
	bool groupChat() const { return groupChat_; };

	void setScriptEnabled(bool b);
	bool scriptEnabled() const;

	void setScriptPath(const QString &path);
	QString scriptPath() const;

	void setScriptArgument(const QString &argument);
	QString scriptArgument() const;

	enum class ScriptArg {
		SenderArg  = 0x01,
		MessageArg = 0x02,
		StatusArg  = 0x04
	};

	void setScriptExtraArgs(int flags);
	int scriptExtraArgs() const;
	bool hasScriptSender() const;
	bool hasScriptMessage() const;
	bool hasScriptStatus() const;

	void setTemplateEnabled(bool b);
	bool templateEnabled() const;

	void setTemplateText(const QString &templateText);
	QString templateText() const;

	void setTemplateDelay(int sec);
	int templateDelay() const;

	void setForwardEnabled(bool b);
	bool forwardEnabled() const;

	void setForwardJid(const QString &jid);
	QString forwardJid() const;

	enum class ForwardArg {
		SenderArg  = 0x01,
		MessageArg = 0x02,
		StatusArg  = 0x04
	};

	void setForwardArgs(int flags);
	int forwardArgs() const;
	bool hasForwardSender() const;
	bool hasForwardMessage() const;
	bool hasForwardStatus() const;

	WatchedItem* copy();

private:
	QString jid_;
	QString text_;
	QString sFile_;
	bool aUse_;
	bool groupChat_;
	bool _scriptEnabled;
	QString _scriptPath;
	QString _scriptArgument;
	int _scriptExtraArgs;

	bool _templateEnabled;
	QString _templateText;
	int _templateDelay;

	bool _forwardEnabled;
	QString _forwardJid;
	int _forwardArgs;
};

} // namespace watcher
