/*
 * watcherplugin.h - plugin
 * Copyright (C) 2010  Khryukin Evgeny
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

#pragma once

#include "watcheditem.h"

#include <psiplugin.h>
#include <stanzafilter.h>
#include <popupaccessor.h>
#include <popupaccessinghost.h>
#include <optionaccessor.h>
#include <optionaccessinghost.h>
#include <iconfactoryaccessor.h>
#include <iconfactoryaccessinghost.h>
#include <menuaccessor.h>
#include <applicationinfoaccessor.h>
#include <applicationinfoaccessinghost.h>
#include <plugininfoprovider.h>
#include <activetabaccessinghost.h>
#include <activetabaccessor.h>
#include <contactinfoaccessinghost.h>
#include <contactinfoaccessor.h>
#include <accountinfoaccessinghost.h>
#include <accountinfoaccessor.h>
#include <soundaccessinghost.h>
#include <soundaccessor.h>
#include <toolbariconaccessor.h>
#include <eventfilter.h>
#include <psiaccountcontrollinghost.h>
#include <psiaccountcontroller.h>

#include <ui_options.h>

#include <QModelIndex>

namespace watcher {

class Model;

class Watcher : public QObject
			  , public PsiPlugin
			  , public PopupAccessor
			  , public MenuAccessor
			  , public PluginInfoProvider
			  , public OptionAccessor
			  , public StanzaFilter
			  , public IconFactoryAccessor
			  , public ApplicationInfoAccessor
			  , public ActiveTabAccessor
			  , public ContactInfoAccessor
			  , public AccountInfoAccessor
			  , public SoundAccessor
			  , public ToolbarIconAccessor
			  , public EventFilter
			  , public PsiAccountController
{
	Q_OBJECT
#ifdef HAVE_QT5
	Q_PLUGIN_METADATA(IID "com.psi-plus.Watcher")
#endif
	Q_INTERFACES(PsiPlugin PopupAccessor OptionAccessor StanzaFilter IconFactoryAccessor AccountInfoAccessor
				 PluginInfoProvider MenuAccessor ApplicationInfoAccessor ActiveTabAccessor ContactInfoAccessor
				 SoundAccessor ToolbarIconAccessor EventFilter PsiAccountController)
public:
	Watcher();
	virtual QString name() const;
	virtual QString shortName() const;
	virtual QString version() const;
	virtual QWidget* options();
	virtual bool enable();
	virtual bool disable();
	virtual void optionChanged(const QString& option);
	virtual void applyOptions();
	virtual void restoreOptions();
	virtual QPixmap icon() const;

	virtual void setPopupAccessingHost(PopupAccessingHost* host);
	virtual void setOptionAccessingHost(OptionAccessingHost* host);
	virtual bool incomingStanza(int account, const QDomElement& xml);
	virtual bool outgoingStanza(int account, QDomElement& xml);
	virtual void setIconFactoryAccessingHost(IconFactoryAccessingHost* host);
	QList < QVariantHash > getAccountMenuParam();
	QList < QVariantHash > getContactMenuParam();
	virtual QAction* getContactAction(QObject* , int , const QString& );
	virtual QAction* getAccountAction(QObject* , int ) { return 0; }
	virtual void setApplicationInfoAccessingHost(ApplicationInfoAccessingHost* host);
	virtual QString pluginInfo();
	virtual void setActiveTabAccessingHost(ActiveTabAccessingHost* host);
	virtual void setContactInfoAccessingHost(ContactInfoAccessingHost* host);
	virtual void setAccountInfoAccessingHost(AccountInfoAccessingHost* host);
	virtual void setSoundAccessingHost(SoundAccessingHost* host);

	QList<QVariantHash> getButtonParam() { return QList<QVariantHash>(); }
	QAction* getAction(QObject *parent, int account, const QString &contact);

	// EventFilter
	bool processEvent(int accountIndex, QDomElement &e);
	bool processMessage(int accountIndex, const QString &contact,
						const QString &body, const QString &subject);
	bool processOutgoingMessage(int accountIndex, const QString &contact,
								QString &body, const QString &type,
								QString &subject);
	void logout(int accountIndex);

	QAction* createAction(QObject *parent, const QString &contact);

	// PsiAccountController
	void setPsiAccountControllingHost(PsiAccountControllingHost *host);

private slots:
	void checkSound(QModelIndex index = QModelIndex());
	void getSound(QModelIndex index = QModelIndex());
	void addLine();
	void delSelected();
	void Hack();
	void onOptionsClose();
	void playSound(const QString& soundFile);
	void showPopup(int account, const QString& jid, QString text);

	void chooseScriptPath();
	void setScriptEnabled(bool enabled);

	void addItemAct();
	void delItemAct();
	void editItemAct();
	void addNewItem(const QString& settings);
	void editCurrentItem(const QString& setting);
	void enableGlobalSounds();
	void actionActivated();
	void removeFromActions(QObject *object);

	void sendMessage();

private:
	bool checkWatchedItem(int accountIndex, const QString& from, const QString& body, WatchedItem *wi, bool activeTab, const QString &status);
	void triggerWatchedItem(int accountIndex, const QString& from, const QString& body, WatchedItem *wi, const QString &status);
	void execScript(const QString &type, const QString &jid, const QString &message);
	void execWiScript(const QString &path, const QStringList &arguments);

	OptionAccessingHost *psiOptions;
	PopupAccessingHost* popup;
	IconFactoryAccessingHost* icoHost;
	ApplicationInfoAccessingHost* appInfoHost;
	ActiveTabAccessingHost* activeTab;
	ContactInfoAccessingHost* contactInfo;
	AccountInfoAccessingHost* accInfo;
	SoundAccessingHost* sound_;
	PsiAccountControllingHost *_psiAccount;
	bool enabled;
	QString soundFile;
	//int Interval;
	QPointer<QWidget> optionsWid;
	Model *model_;
	Ui::Options ui_;
	QList<WatchedItem*> items_;
	bool isSndEnable;
	bool disableSnd;
	bool disablePopupDnd;
	int popupId;
	QHash<QString, QAction*> actions_;
	bool showInContext_;

	bool _enabledScript;
	QString _scriptPath;
};

} // namespace watcher
