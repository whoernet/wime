/*
 * command.cpp
 *
 * Copyright (C) 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "command.h"

#include <psiaccountcontrollinghost.h>
#include <iconfactoryaccessinghost.h>
#include <activetabaccessinghost.h>
#include <accountinfoaccessinghost.h>

#include <QFile>
#include <QDir>
#include <QInputDialog>
#include <QProcess>
#include <QMessageBox>

#ifndef HAVE_QT5
Q_EXPORT_PLUGIN2(Command, Command);
#endif

#define TIMEOUT 30 * 1000 /* 30 seconds */

Command::Command()
	: _enabled(false)
	, _accountHost(nullptr)
	, _accountInfo(nullptr)
	, _appInfo(nullptr)
	, _iconFactory(nullptr)
	, _dirWatch(new QCA::DirWatch("", this))
	, _timer(new QTimer(this))
{
	connect(_timer, SIGNAL(timeout()), SLOT(sendMessage()));
	_timer->setSingleShot(false);
}


Command::~Command()
{
}

QWidget *Command::options()
{
	return nullptr;
}

bool Command::enable()
{
	_enabled = true;
	connect(_dirWatch, SIGNAL(changed()), SLOT(sendMessage()));
	_timer->start(TIMEOUT);

	QFile file(":/icons/command.png");
	file.open(QIODevice::ReadOnly);
	QByteArray image = file.readAll();
	_iconFactory->addIcon("command/icon",image);
	file.close();

	return _enabled;
}

bool Command::disable()
{
	_enabled = false;
	disconnect(_dirWatch);
	_timer->stop();
	return true;
}

void Command::applyOptions()
{
}

void Command::restoreOptions()
{
}

QPixmap Command::icon() const
{
	return QPixmap(":/icons/command.png");
}

QString Command::pluginInfo()
{
	return QString();
}

void Command::setApplicationInfoAccessingHost(ApplicationInfoAccessingHost *host)
{
	 _appInfo = host;
	 QString cacheDir =_appInfo->appHomeDir(ApplicationInfoAccessingHost::HomedirType::CacheLocation);
	 _dirWatch->setDirName(cacheDir + QLatin1String("/commandplugin"));
}

QList<QVariantHash> Command::getButtonParam()
{
	QList<QVariantHash> l;

	QVariantHash hash;
	hash["tooltip"] = QVariant(tr("Execute shell script"));
	hash["icon"] = QVariant(QString("command/icon"));
	hash["reciver"] = qVariantFromValue(qobject_cast<QObject *>(this));
	hash["slot"] = QVariant(SLOT(actionActivated()));
	l << hash;
	return l;
}

void Command::sendMessage()
{
	QString dirName = _dirWatch->dirName();
	QDir dir(dirName);
	QStringList fileList = dir.entryList(QStringList() << "*.txt", QDir::Filter::Files | QDir::Filter::Readable);

	for (const auto &fileName: fileList) {

		QFile file(dirName + QLatin1String("/") + fileName);
		if (!file.open(QIODevice::OpenModeFlag::ReadOnly)) {
			continue;
		}

		QString str = QString::fromUtf8(file.readAll());
		QString jid = str.section('\n', 0, 0);
		QString accountName = str.section('\n', 1, 1);
		QString text = str.section('\n', 2, -1);
		text.replace(QLatin1Char('\n'), QLatin1String("<br/>"));

		if (jid.isEmpty() || text.isEmpty()) {
			file.remove();
			continue;
		}

		int accountId = -1;
		if (accountName.trimmed().isEmpty())
			accountId = 0;
		else {
			for (int i = 0;; ++i) {
				QString name = _accountInfo->getName(i);

				if (name.isEmpty())
					break;

				if (accountName != name)
					continue;

				accountId = i;
				break;
			}
		}

		if (_accountInfo->getStatus(accountId) == QLatin1String("offline")) {
			file.close();
			continue;
		}

		file.remove();

		_accountHost->appendMsg(accountId, jid, text);
	}
}

void Command::actionActivated()
{
	QString jid = _activeTab->getYourJid();
	QString jidToSend = _activeTab->getJid();
	int accountId = 0;
	QString tmpJid;
	while (jid != (tmpJid = _accountInfo->getJid(accountId))) {
		++accountId;
		if (tmpJid == "-1") {
			return;
		}
	}

	QString command = QInputDialog::getText(nullptr, tr("Execute shell script"), tr("Script"));

	if (command.trimmed().isEmpty())
		return;

	QString scriptName = command.section(' ', 0, 0);
	QString scriptArguments = command.section(' ', 1, -1);

	QString configDir =_appInfo->appHomeDir(ApplicationInfoAccessingHost::HomedirType::ConfigLocation) + "/commandplugin-scripts/";
	QString scriptPath = configDir + scriptName;
	if (!QFile::exists(scriptPath)) {
		QMessageBox::warning(nullptr, "Command plugin", tr("No such script"));
		return;
	}

	command = QLatin1Char('\"') + scriptPath + QLatin1String("\" ") + scriptArguments;

	QProcess process;
	process.start(command);
	process.waitForFinished();

	if (process.exitCode()) {
		QMessageBox::warning(nullptr, "Command plugin", QString(tr("Script finished with error: %1")).arg(process.exitCode()));
		return;
	}


	QString text = QString::fromUtf8(process.readAllStandardOutput());
	text.replace(QLatin1Char('\n'), QLatin1String("<br/>"));

	_accountHost->appendMsg(accountId, jidToSend, text);
}
