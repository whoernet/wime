/*
 * command.h
 *
 * Copyright (C) 2016
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#pragma once

#include <psiplugin.h>
#include <applicationinfoaccessinghost.h>
#include <plugininfoprovider.h>
#include <psiaccountcontroller.h>
#include <accountinfoaccessor.h>
#include <applicationinfoaccessor.h>
#include <applicationinfoaccessinghost.h>
#include <toolbariconaccessor.h>
#include <iconfactoryaccessor.h>
#include <activetabaccessor.h>

#include <QtCrypto>
#include <QTimer>

class Command : public QObject
			  , public PsiPlugin
			  , public PluginInfoProvider
			  , public PsiAccountController
			  , public AccountInfoAccessor
			  , public ApplicationInfoAccessor
			  , public ToolbarIconAccessor
			  , public IconFactoryAccessor
			  , public ActiveTabAccessor
{
	Q_OBJECT
#ifdef HAVE_QT5
	Q_PLUGIN_METADATA(IID "com.psi-plus.Command")
#endif
	Q_INTERFACES(PsiPlugin
				 PluginInfoProvider
				 PsiAccountController
				 AccountInfoAccessor
				 ApplicationInfoAccessor
				 ToolbarIconAccessor
				 IconFactoryAccessor
				 ActiveTabAccessor)
public:
	Command();
	~Command();

	// from PsiPlugin
	QString name() const { return "Command"; }
	QString shortName() const { return "command"; }
	QString version() const { return "0.0.1"; }

	QWidget *options();
	bool enable();
	bool disable();
	void applyOptions();
	void restoreOptions();
	QPixmap icon() const;

	// from PluginInfoProvider
	QString pluginInfo();

	// from PsiAccountController
	void setPsiAccountControllingHost(PsiAccountControllingHost *host) { _accountHost = host; }

	// from AccountInfoAccessor
	void setAccountInfoAccessingHost(AccountInfoAccessingHost* host) { _accountInfo = host; }

	// from ApplicationInfoAccessor
	void setApplicationInfoAccessingHost(ApplicationInfoAccessingHost *host);

	// from ToolbarIconAccessor
	QList<QVariantHash> getButtonParam();
	QAction* getAction(QObject */*parent*/, int /*account*/, const QString &/*contact*/) { return 0; }

	// from IconFactoryAccessor
	void setIconFactoryAccessingHost(IconFactoryAccessingHost *host) { _iconFactory = host; }

	// from ActiveTabAccessor
	void setActiveTabAccessingHost(ActiveTabAccessingHost* host) { _activeTab = host; }

public slots:
	void sendMessage();
	void actionActivated();

private:

	bool _enabled;
	PsiAccountControllingHost *_accountHost;
	AccountInfoAccessingHost *_accountInfo;
	ApplicationInfoAccessingHost *_appInfo;
	IconFactoryAccessingHost *_iconFactory;
	ActiveTabAccessingHost *_activeTab;
	QCA::DirWatch *_dirWatch;
	QTimer *_timer;
};

