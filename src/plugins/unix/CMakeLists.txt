cmake_minimum_required(VERSION 2.8.11) 

set(plugins_list 
  gnome3supportplugin
)

if("${BUILD_PLUGINS}" STREQUAL "ALL")
  set(plugins ${plugins_list})
else()
  set(plugins "${BUILD_PLUGINS}")
endif()

foreach(plugin ${plugins_list})
  foreach(subdir ${plugins})
    if(${plugin} STREQUAL ${subdir})
      message("Parse subdirectory: ./${plugin}")
      add_subdirectory(${plugin})
    endif()
  endforeach()
endforeach()
