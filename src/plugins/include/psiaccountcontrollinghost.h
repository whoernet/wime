#ifndef PSIACCOUNTCONTROLLINGHOST_H
#define PSIACCOUNTCONTROLLINGHOST_H

class QString;

class PsiAccountControllingHost
{
public:
	virtual ~PsiAccountControllingHost() {}

	virtual void setStatus(int account, const QString& status, const QString& statusMessage) = 0;

	virtual bool appendSysMsg(int account, const QString& jid, const QString& message) = 0;

	/**
	 * @brief setEncrypted can be used by encrypted plugins to tell chat tab that conversation is safe
	 * @param account
	 * @param jid
	 * @param encrypted
	 */

	virtual void setEncrypted(int account, const QString &jid, bool encrypted) = 0;

	virtual bool appendMsg(int account, const QString &jid, const QString &message) = 0;
};

Q_DECLARE_INTERFACE(PsiAccountControllingHost, "org.psi-im.PsiAccountControllingHost/0.3");

#endif // PSIACCOUNTCONTROLLINGHOST_H
