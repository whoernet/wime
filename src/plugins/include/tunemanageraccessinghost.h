#pragma once

#include <QStringList>

class TuneManagerAccessingHost
{
public:
	virtual ~TuneManagerAccessingHost() {}

	virtual QStringList controllerNames() = 0;
};

Q_DECLARE_INTERFACE(TuneManagerAccessingHost, "org.psi-im.TuneManagerAccessingHost/0.1");
