#ifndef ACCOUNTINFOACCESSOR_H
#define ACCOUNTINFOACCESSOR_H

class AccountInfoAccessingHost;

class AccountInfoAccessor
{
public:
	virtual ~AccountInfoAccessor() {}

	virtual void setAccountInfoAccessingHost(AccountInfoAccessingHost* host) = 0;
	virtual void accountAdded(int /* account */) {}
};

Q_DECLARE_INTERFACE(AccountInfoAccessor, "org.psi-im.AccountInfoAccessor/0.2");


#endif // ACCOUNTINFOACCESSOR_H
