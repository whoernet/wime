#ifndef OPTIONACCESSINGHOST_H
#define OPTIONACCESSINGHOST_H

class QString;
class QAbstractItemModel;

#include <QVariant>

class OptionAccessingHost
{
public:
	virtual ~OptionAccessingHost() {}

	virtual void setPluginOption(const QString& option, const QVariant& value) = 0;
	virtual QVariant getPluginOption(const QString &option, const QVariant &defValue = QVariant::Invalid) = 0;

	virtual void setGlobalOption(const QString& option, const QVariant& value) = 0;
	virtual QVariant getGlobalOption(const QString& option) = 0;
	virtual QString getGlobalComment(const QString &option) = 0;
	virtual bool isValidGlobalName(const QString &name) = 0;
	virtual QAbstractItemModel *treeModel(QWidget *parent) = 0;
	virtual bool isInternalGlobalNode(const QString &name) = 0;
	virtual void removeGlobalOption(const QString &option, bool internal_nodes = false) = 0;
	virtual void resetGlobalOption(const QString &option) = 0;
};

Q_DECLARE_INTERFACE(OptionAccessingHost, "org.psi-im.OptionAccessingHost/0.2");

#endif
