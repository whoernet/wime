#pragma once

class QString;

class TuneManagerAccessingHost;

class TuneManagerAccessor
{
public:
	virtual ~TuneManagerAccessor() {}

	virtual void setTuneManagerAccessingHost(TuneManagerAccessingHost *host) = 0;
};

Q_DECLARE_INTERFACE(TuneManagerAccessor, "org.psi-im.TuneManagerAccessor/0.1");
