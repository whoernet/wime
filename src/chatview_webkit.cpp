/*
 * chatview_webkit.cpp - Webkit based chatview
 * Copyright (C) 2010  Rion
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "chatview_webkit_p.h"

#include "msgmle.h"
#include "psioptions.h"
#include "textutil.h"

#include <QWidget>
#include <QWebFrame>
#include <QFile>
#include <QFileInfo>
#include <QLayout>
#include <QPalette>
#include <QDesktopWidget>
#include <QApplication>

#include "webview.h"
//#include "psiapplication.h"
#include "psiaccount.h"
#include "psicontact.h"
#include "applicationinfo.h"
#include "networkaccessmanager.h"
#include "jsutil.h"
#include "messageview.h"
#include "psithememanager.h"
#include "chatviewtheme.h"



//----------------------------------------------------------------------------
// ChatViewJSObject
// object which will be embed to javascript part of view
//----------------------------------------------------------------------------
ChatViewJSObject::ChatViewJSObject(ChatView *view)
	: QObject(view)
	, _view(view)
{

}

QString ChatViewJSObject::mucNickColor(const QString &nick, bool isSelf,
									   const QStringList &validList) const
{
	return _view->getMucNickColor(nick, isSelf, validList);
}

bool ChatViewJSObject::isMuc() const
{
	return _view->isMuc_;
}

QString ChatViewJSObject::chatName() const
{
	return _view->name_;
}

QString ChatViewJSObject::jid() const
{
	return _view->jid_;
}

QString ChatViewJSObject::account() const
{
	return _view->account_->id();
}

void ChatViewJSObject::signalInited()
{
	emit inited();
}

void ChatViewJSObject::signalAuth()
{
	emit doAuth();
}

void ChatViewJSObject::signalDeny()
{
	emit doDeny();
}

void ChatViewJSObject::signalQuote(const QString &text)
{
	emit doQuote(text);
}

QString ChatViewJSObject::getFont() const
{
	QFont f = ((ChatView*)parent())->font();
	QString weight = "normal";
	switch (f.weight()) {
	case QFont::Light: weight = "lighter"; break;
	case QFont::DemiBold: weight = "bold"; break;
	case QFont::Bold: weight = "bolder"; break;
	case QFont::Black: weight = "900"; break;
	}

	// In typography 1 point (also called PostScript point)
	// is 1/72 of an inch
	const float postScriptPoint = 1 / 72.;

	// Workaround.  WebKit works only with 96dpi
	// Need to convert point size to pixel size
	int pixelSize = qRound(f.pointSize() * qApp->desktop()->logicalDpiX() * postScriptPoint);

	return QString("{fontFamily:'%1',fontSize:'%2px',fontStyle:'%3',fontVariant:'%4',fontWeight:'%5'}")
		   .arg(f.family())
		   .arg(pixelSize)
		   .arg(f.style()==QFont::StyleNormal?"normal":(f.style()==QFont::StyleItalic?"italic":"oblique"))
		   .arg(f.capitalization() == QFont::SmallCaps?"small-caps":"normal")
		   .arg(weight);
}

void ChatViewJSObject::setTransparent()
{
	QPalette palette = _view->webView->palette();
	palette.setBrush(QPalette::Base, Qt::transparent);
	_view->webView->page()->setPalette(palette);
	_view->webView->setAttribute(Qt::WA_OpaquePaintEvent, false);

	palette = _view->palette();
	palette.setBrush(QPalette::Base, Qt::transparent);
	_view->setPalette(palette);
	_view->setAttribute(Qt::WA_TranslucentBackground, true);
}

//----------------------------------------------------------------------------
// ChatView
//----------------------------------------------------------------------------
ChatView::ChatView(QWidget *parent)
	: QFrame(parent)
	, sessionReady_(false)
	, dialog_(0)
	, isMuc_(false)
	, isEncryptionEnabled_(false)
{
	jsObject = new ChatViewJSObject(this);
	webView = new WebView(this);
	webView->setFocusPolicy(Qt::NoFocus);
	webView->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);

	QVBoxLayout *layout = new QVBoxLayout;
	layout->setContentsMargins(0,0,0,0);
	layout->addWidget(webView);
	setLayout(layout);
	setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
	setLooks(webView);

#ifndef HAVE_X11	// linux has this feature built-in
	connect( PsiOptions::instance(), SIGNAL(optionChanged(QString)), SLOT(psiOptionChanged(QString)) ); //needed only for save autocopy state atm
	psiOptionChanged("options.ui.automatically-copy-selected-text"); // init autocopy connection
#endif
	connect(jsObject, SIGNAL(inited()), SLOT(sessionInited()));
	connect(jsObject, SIGNAL(doAuth()), SLOT(doAuth()));
	connect(jsObject, SIGNAL(doDeny()), SLOT(doDeny()));
	connect(jsObject, SIGNAL(doQuote(const QString&)), SLOT(doQuote(const QString&)));
	connect(webView->page()->mainFrame(),
			SIGNAL(javaScriptWindowObjectCleared()), SLOT(embedJsObject()));
}

ChatView::~ChatView()
{
}

ChatViewTheme* ChatView::currentTheme()
{
	return (ChatViewTheme *)PsiThemeManager::instance()->
			provider(isMuc_?"groupchatview":"chatview")->current();
}

// something after we know isMuc and dialog is set
void ChatView::init()
{
	ChatViewTheme *theme = currentTheme();
	QString html = theme->html(jsObject);
	//qDebug() << "Set html:" << html;
	webView->page()->mainFrame()->setHtml(
		html, theme->baseUrl()
	);
}

void ChatView::setEncryptionEnabled(bool enabled)
{
	isEncryptionEnabled_ = enabled;
}

void ChatView::embedJsObject()
{
	ChatViewTheme *theme = currentTheme();
	QWebFrame *wf = webView->page()->mainFrame();
	wf->addToJavaScriptWindowObject("chatServer", theme->jsHelper());
	wf->addToJavaScriptWindowObject("chatSession", jsObject);
	foreach (const QString &script, theme->scripts()) {
		wf->evaluateJavaScript(script);
	}
}

void ChatView::markReceived(QString id)
{
	QVariantMap m;
	m["type"] = "receipt";
	m["id"] = id;
	m["encrypted"] = isEncryptionEnabled_;
	sendJsObject(m);
}

QSize ChatView::sizeHint() const
{
	return minimumSizeHint();
}

void ChatView::setDialog(QWidget* dialog)
{
	dialog_ = dialog;
}

void ChatView::setSessionData(bool isMuc, const QString &jid, const QString name)
{
	isMuc_ = isMuc;
	jid_ = jid;
	name_ = name;
	connect(PsiThemeManager::instance()->provider(isMuc_?"groupchatview":"chatview"),
			SIGNAL(themeChanged()),
			SLOT(init()));
}

void ChatView::contextMenuEvent(QContextMenuEvent *e)
{
	QWebHitTestResult r = webView->page()->mainFrame()->hitTestContent(e->pos());
	if ( r.linkUrl().scheme() == "addnick" ) {
		showNM(r.linkUrl().path().mid(1));
		e->accept();
	}
}

bool ChatView::focusNextPrevChild(bool next)
{
	return QWidget::focusNextPrevChild(next);
}

void ChatView::changeEvent(QEvent * event)
{
	if ( event->type() == QEvent::ApplicationPaletteChange
		|| event->type() == QEvent::PaletteChange
		|| event->type() == QEvent::FontChange ) {
		QVariantMap m;
		m["type"] = "settings";
		sendJsObject(m);
	}
	QFrame::changeEvent(event);
}

void ChatView::psiOptionChanged(const QString &option)
{
	if (option == "options.ui.automatically-copy-selected-text") {
		if (PsiOptions::instance()->
			getOption("options.ui.automatically-copy-selected-text").toBool()) {
			connect(webView->page(), SIGNAL(selectionChanged()), webView, SLOT(copySelected()));
		} else {
			disconnect(webView->page(), SIGNAL(selectionChanged()), webView, SLOT(copySelected()));
		}
	}
}

void ChatView::sendJsObject(const QVariantMap &map)
{
	sendJsCommand(QString(currentTheme()->jsNamespace() + ".adapter.receiveObject(%1);")
				  .arg(JSUtil::map2json(map)));
}

void ChatView::sendJsCommand(const QString &cmd)
{
	jsBuffer_.append(cmd);
	checkJsBuffer();
}

void ChatView::checkJsBuffer()
{
	if (sessionReady_) {
		while (!jsBuffer_.isEmpty()) {
			webView->evaluateJS(jsBuffer_.takeFirst());
		}
	}
}

void ChatView::sessionInited()
{
	sessionReady_ = true;
	checkJsBuffer();
}

void ChatView::doAuth()
{
	PsiContact *contact = account_->findContact(Jid(jid_));
	if (contact && contact->inList())
		account_->actionAuth(Jid(jid_).bare());
	else
		account_->actionAdd(Jid(jid_).bare());

}

void ChatView::doDeny()
{
	account_->actionAuthRemove(Jid(jid_).bare());
}

void ChatView::doQuote(const QString &text)
{
	emit webView->quote(TextUtil::rich2plain(TextUtil::img2title(text)));
}

bool ChatView::handleCopyEvent(QObject *object, QEvent *event, ChatEdit *chatEdit) {
	if (object == chatEdit && event->type() == QEvent::ShortcutOverride &&
		((QKeyEvent*)event)->matches(QKeySequence::Copy)) {

		if (!chatEdit->textCursor().hasSelection() &&
			 !(webView->page()->selectedText().isEmpty()))
		{
			webView->copySelected();
			return true;
		}
	}

	return false;
}

// input point of all messages
void ChatView::dispatchMessage(const MessageView &mv)
{
	if ((mv.type() == MessageView::Message || mv.type() == MessageView::Subject)
			&& updateLastMsgTime(mv.dateTime()))
	{
		QVariantMap m;
		m["date"] = mv.dateTime();
		m["type"] = "message";
		m["mtype"] = "lastDate";
		sendJsObject(m);
	}
	QVariantMap vm = mv.toVariantMap(isMuc_, true);
	vm["mtype"] = vm["type"];
	vm["type"] = "message";
	vm["encrypted"] = isEncryptionEnabled_;
	sendJsObject(vm);
}

void ChatView::scrollUp()
{
	QWebFrame *f = webView->page()->mainFrame();
	f->setScrollBarValue(Qt::Vertical, f->scrollBarValue(Qt::Vertical) - 50);
}

void ChatView::scrollDown()
{
	QWebFrame *f = webView->page()->mainFrame();
	f->setScrollBarValue(Qt::Vertical, f->scrollBarValue(Qt::Vertical) + 50);
}

void ChatView::clear()
{
	QVariantMap m;
	m["type"] = "clear";
	sendJsObject(m);
}

void ChatView::doTrackBar()
{
	QVariantMap m;
	m["type"] = "trackbar";
	sendJsObject(m);
}

bool ChatView::internalFind(QString str, bool startFromBeginning)
{
	bool found = webView->page()->findText(str, startFromBeginning ?
				 QWebPage::FindWrapsAroundDocument : (QWebPage::FindFlag)0);

	if (!found && !startFromBeginning) {
		return internalFind(str, true);
	}

	return found;
}

WebView* ChatView::textWidget()
{
	return webView;
}

QWidget* ChatView::realTextWidget()
{
	return webView;
}
