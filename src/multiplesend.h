#pragma once

#include <QDialog>

class PsiAccount;
class QTreeWidgetItem;

namespace Ui { class MultipleSend; }

class MultipleSend : public QDialog
{
    Q_OBJECT

public:
    explicit MultipleSend(PsiAccount *account, QWidget *parent = 0);

	void setJids(const QStringList &jids);

    ~MultipleSend();

signals:
	void needSendMultiple(PsiAccount *account, const QStringList &jids, const QString &message);

public slots:
	void updateCounter();
	void updateContacts(bool b);
	void sendMessage();
	void updateContactsSelection(QTreeWidgetItem *item, int column);

private:
    Ui::MultipleSend *ui;
	class Private;
	Private *d;
};
