/*
 * logwidget.h
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#pragma once

#include "logmenu.h"
#include "logmodel.h"
#include "debugmessagehandler.h"

#include <QTableView>
#include <QWidget>
#include <QSortFilterProxyModel>

namespace logger
{

class LogsWidget;

class LogWidget : public QTableView
{
	Q_OBJECT

public:
	explicit LogWidget(LogsWidget *lw, bool useSubtypeColors, QWidget *parent = nullptr);
	virtual ~LogWidget();
	void handleAppend(const LogsType &logs);

protected:
	void contextMenuEvent(QContextMenuEvent *event) override;

private:
	LogTableModel *m_model;
	QSortFilterProxyModel *m_proxyModel;
	LogMenu *m_logMenu;
	LogsWidget *m_logsWidget;
};

} // namespace logger
