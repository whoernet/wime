/*
 * logevent.cpp
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "logevent.h"

#include <QThread>

namespace logger
{

#ifdef HAVE_QT5
LogEvent::LogEvent(QtMsgType type, const QMessageLogContext &cnt, const QString &msg)
#else
LogEvent::LogEvent(QtMsgType type, const QString &msg)
#endif
	: QEvent(logType)
	, m_message(msg)
	, m_type(type)
	, m_timestamp(QDateTime::currentDateTime())
	, m_thread(QString().sprintf("%p", QThread::currentThread()))
#ifdef HAVE_QT5
	, m_version(cnt.version)
	, m_line(cnt.line)
	, m_file(cnt.file)
	, m_function(cnt.function)
	, m_category(cnt.category)
#endif
{
}

LogEvent::~LogEvent()
{
}

QString LogEvent::getMessage() const
{
	return m_message;
}

QDateTime LogEvent::getTimeStamp() const
{
	return m_timestamp;
}

QString LogEvent::getThread() const
{
	return m_thread;
}

QtMsgType LogEvent::getType() const
{
	return m_type;
}

#ifdef HAVE_QT5

QString LogEvent::getFile() const
{
	return m_file;
}

QString LogEvent::getFunction() const
{
	return m_function;
}

QString LogEvent::getCategory() const
{
	return m_category;
}

int LogEvent::getLine() const
{
	return m_line;
}

int LogEvent::getVersion() const
{
	return m_version;
}

#endif

} // namespace logger
