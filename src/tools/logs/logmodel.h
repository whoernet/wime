/*
 * logmodel.h
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#pragma once

#include "debugmessagehandler.h"
#include <QAbstractTableModel>
#include <QFont>
#include <QColor>

namespace logger
{

class LogsWidget;

class LogTableModel : public QAbstractTableModel
{
	Q_OBJECT
	Q_ENUMS(ColumnIDs)

public:
	LogTableModel(LogsWidget *, bool, QObject * = 0);
	virtual ~LogTableModel() {}

	enum class ColumnIDs {
		TypeColumn = 0,
		SubTypeColumn,
		DateColumn,
		TextColumn,
		LastColumn
	};

	void clear();
	void addLogs(LogsType);
	virtual QVariant data(const QModelIndex &, int) const override;
	int rowCount(const QModelIndex & = QModelIndex()) const override;
	virtual int columnCount(const QModelIndex & = QModelIndex()) const override;
	virtual bool removeRows(int, int, const QModelIndex & = QModelIndex()) override;

private:
	LogsWidget *m_logsWidget;
	LogsType m_logs;
	QFont m_boldFont;
	QColor m_warningColor;
	QColor m_fatalColor;
	bool m_useSubtypeColors;
};

} // namespace logger
