/*
 * logswidget.h
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#pragma once

#include "logwidget.h"
#include "debugmessagehandler.h"

#include <QWidget>
#include <QMap>

namespace logger
{

namespace Ui
{
class LogsWidget;
}

class LogsWidget : public QWidget
{
	Q_OBJECT
public:
	explicit LogsWidget(QObject *logger, QWidget *parent = 0);
	virtual ~LogsWidget();

	void setColor(const QString &type, const QColor &color);
	QMap<QString, QColor> colors() const;
protected:
	void changeEvent(QEvent *event);

private slots:
	void importLogs(const logger::LogsMappedType &subtypes);

private:
	Ui::LogsWidget *_ui;
	QMap<QString, LogWidget*> _logWidgets;
	QMap<QString, QColor> _subtypesColor;
};

} // namespace logger
