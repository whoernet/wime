/*
 * logmodel.cpp
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include <QColor>
#include "logmodel.h"
#include "logswidget.h"

namespace logger
{

#define MAX_LOG_LINES 2000

LogTableModel::LogTableModel(LogsWidget *lw, bool useSubtypeColors, QObject *parent)
	: QAbstractTableModel(parent)
{
	m_logsWidget = lw;
	m_boldFont.setBold(true);
	m_useSubtypeColors = useSubtypeColors;
	m_warningColor = QColor(0xFF, 0x63, 0x47);
	m_fatalColor = QColor(Qt::red);
}

QVariant LogTableModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role == Qt::ForegroundRole) {
		QtMsgType type = m_logs[index.row()].m_qtType;

		if (type == QtMsgType::QtWarningMsg)
			return m_warningColor;

		if (type == QtMsgType::QtFatalMsg)
			return m_fatalColor;

		if (type == QtMsgType::QtCriticalMsg)
			return m_fatalColor;

		if (m_useSubtypeColors) {
			QColor color = m_logsWidget->colors().value(m_logs[index.row()].m_type);

			if (color.isValid())
				return color;
		}
	}

	ColumnIDs column = static_cast<ColumnIDs>(index.column());

	switch (column) {
	case ColumnIDs::TypeColumn:
		if (role == Qt::TextAlignmentRole)
			return Qt::AlignCenter;

		if (role == Qt::DisplayRole)
			return logQtTypeMap.value(m_logs[index.row()].m_qtType).left(1);
		else if (role == Qt::FontRole)
			return m_boldFont;

		break;

	case ColumnIDs::SubTypeColumn:
		if (role == Qt::TextAlignmentRole)
			return Qt::AlignCenter;

		if (role == Qt::DisplayRole)
			return m_logs[index.row()].m_type;

		break;

	case ColumnIDs::DateColumn:
		if (role == Qt::DisplayRole)
			return m_logs[index.row()].m_date;
		break;

	case ColumnIDs::TextColumn:
		if (role == Qt::DisplayRole)
			return m_logs[index.row()].m_message;

		break;

	default:
		break;
	}

	return QVariant();
}

int LogTableModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent)
	return m_logs.size();
}

int LogTableModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent)
	return static_cast<int>(ColumnIDs::LastColumn);
}

void LogTableModel::addLogs(LogsType l)
{
	if ((l.size() + m_logs.size()) > MAX_LOG_LINES)
		clear();

	beginInsertRows(QModelIndex(), this->m_logs.size(), this->m_logs.size() + l.size() - 1);
	m_logs.append(l);
	endInsertRows();
}

bool LogTableModel::removeRows(int position, int rows, const QModelIndex &index)
{
	Q_UNUSED(index);

	if (rows <= 0)
		return false;

	beginRemoveRows(QModelIndex(), position, position + rows - 1);

	if (position == 0 && rows == m_logs.size()) {
		m_logs.clear();
	}
	else {
		for (int row = 0; row < rows; row++) {
			m_logs.removeAt(position);
		}
	}

	endRemoveRows();
	return true;
}

void LogTableModel::clear()
{
	removeRows(0, m_logs.size());
}

} // namespace logger
