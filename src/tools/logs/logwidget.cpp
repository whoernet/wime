/*
 * logwidget.cpp
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "logwidget.h"

#include <QHeaderView>
#include <QScrollBar>
#include <QContextMenuEvent>

namespace logger
{

LogWidget::LogWidget(LogsWidget *lw, bool useSubtypeColors, QWidget *parent)
	: QTableView(parent)
{
	m_logsWidget = lw;
	setContextMenuPolicy(Qt::DefaultContextMenu);
	setEditTriggers(QAbstractItemView::NoEditTriggers);
	setAutoScroll(true);
	setTabKeyNavigation(false);
	setDropIndicatorShown(false);
	setDragDropOverwriteMode(false);
	setSelectionMode(QAbstractItemView::ContiguousSelection);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	resizeRowsToContents();
#ifdef HAVE_QT5
	verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
#else
	verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);
#endif
	setShowGrid(true);
	setGridStyle(Qt::DotLine);
	setWordWrap(true);
	setCornerButtonEnabled(false);
	horizontalHeader()->setVisible(false);
#ifdef HAVE_QT5
	horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
#else
	horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
#endif
	horizontalHeader()->setDefaultSectionSize(70);
	horizontalHeader()->setHighlightSections(false);
	horizontalHeader()->setStretchLastSection(true);
	verticalHeader()->setVisible(false);
	verticalHeader()->setDefaultSectionSize(20);
	verticalHeader()->setHighlightSections(false);
	verticalHeader()->setMinimumSectionSize(20);
	m_model = new LogTableModel(m_logsWidget, useSubtypeColors, this);
	m_proxyModel = new QSortFilterProxyModel(this);
	m_proxyModel->setFilterKeyColumn(static_cast<int>(LogTableModel::ColumnIDs::TypeColumn));
	m_proxyModel->setSourceModel(m_model);
	setModel(m_proxyModel);

	if (!useSubtypeColors)
		this->hideColumn(static_cast<int>(LogTableModel::ColumnIDs::SubTypeColumn));

	m_logMenu = new LogMenu(m_model, m_proxyModel, this);
}

LogWidget::~LogWidget()
{
}

void LogWidget::handleAppend(const LogsType &logs)
{
	bool autoscroll = (verticalScrollBar()->value() == verticalScrollBar()->maximum());
	m_model->addLogs(logs);

	if (autoscroll)
		scrollToBottom();
}

void LogWidget::contextMenuEvent(QContextMenuEvent *event)
{
	QString text = "";
	QModelIndexList rows = selectionModel()->selectedRows();
	for (const QModelIndex &index: rows) {
		if (index.isValid()) {
			QString type = m_proxyModel->index(index.row(), static_cast<int>(LogTableModel::ColumnIDs::TypeColumn)).data().toString();
			QString subtype = m_proxyModel->index(index.row(), static_cast<int>(LogTableModel::ColumnIDs::SubTypeColumn)).data().toString();
			QString date = m_proxyModel->index(index.row(), static_cast<int>(LogTableModel::ColumnIDs::DateColumn)).data().toString();
			QString msg = m_proxyModel->index(index.row(), static_cast<int>(LogTableModel::ColumnIDs::TextColumn)).data().toString();

			if (!text.isEmpty())
				text += "\n";

			text += QString("%1 %2 %3 %4").arg(type).arg(subtype).arg(date).arg(msg);
		}
	}
	m_logMenu->show(event->globalPos(), text);
}

} // namespace logger
