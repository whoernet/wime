/*
 * logevent.h
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#pragma once

#include <QEvent>
#include <QString>
#include <QDateTime>

namespace logger
{

class LogEvent : public QEvent
{
public:
	static const QEvent::Type logType = static_cast<QEvent::Type>(QEvent::Type::User + 3);

#ifdef HAVE_QT5
	LogEvent(QtMsgType type, const QMessageLogContext &cnt, const QString &msg);
#else
	LogEvent(QtMsgType type, const QString &msg);
#endif
	virtual ~LogEvent();

	QString getThread() const;
	QDateTime getTimeStamp() const;
	QString getMessage() const;
	QtMsgType getType() const;

#ifdef HAVE_QT5
	QString getFile() const;
	QString getFunction() const;
	QString getCategory() const;
	int getLine() const;
	int getVersion() const;
#endif

private:
	QString m_message;
	QtMsgType m_type;
	QDateTime m_timestamp;
	QString m_thread;
#ifdef HAVE_QT5
	int m_version;
	int m_line;
	QString m_file;
	QString m_function;
	QString m_category;
#endif
};

} // namespace logger
