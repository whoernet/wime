/*
 * debugmessagehandler.cpp
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "debugmessagehandler.h"
#include "logevent.h"
#include "defines.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QTextStream>
#include <QThread>
#include <QDir>
#include <QFile>
#include <QMap>
#include <QMessageBox>
#include <QMetaObject>

#define MAX_MESSAGE_LENGTH  300
// Max storage logs days
// After this days logs will be deleted
// 6 is mean one week. 6 days plus current day.
#define MAX_LOGS 6

namespace logger
{

QMap<QtMsgType, QString> logQtTypeMap;

static LogAccumulator *g_forDebugHandler = nullptr;
static QMutex g_stdoutMutex;

#ifdef HAVE_QT5
static void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &message);
#else
static void messageHandler(QtMsgType type, const char *messageChat);
#endif

LogThread::LogThread(QString folder, QObject *parent)
	: QThread(parent)
	, m_logAcumulator(new LogAccumulator())
	, m_logEmitter(new LogsEmitter(m_logAcumulator, folder))
{
	qRegisterMetaType<logger::LogEntity>();
	qRegisterMetaType<logger::LogsType>();
	qRegisterMetaType<logger::LogsMappedType>();

	g_forDebugHandler = m_logAcumulator;
#ifdef HAVE_QT5
	m_previousHandler = qInstallMessageHandler(logger::messageHandler);
#else
	m_previousHandler = qInstallMsgHandler(logger::messageHandler);
#endif

	start();
}

LogThread::~LogThread()
{
	exit();
	wait();
	m_logEmitter->flush();
#ifdef HAVE_QT5
	qInstallMessageHandler(m_previousHandler);
#else
	qInstallMsgHandler(m_previousHandler);
#endif
	delete m_logAcumulator;
	delete m_logEmitter;
}

LogAccumulator &LogThread::getAccumulator()
{
	return *m_logAcumulator;
}

LogsEmitter &LogThread::getEmitter()
{
	return *m_logEmitter;
}

void LogThread::onThreadStart()
{
	// move to logger thread
	m_logAcumulator->moveToThread(this);
	m_logEmitter->moveToThread(this);
	ASYNC_CALL(m_logEmitter, "start");
}

void LogThread::run()
{
	// NOTE periodicaly uncomment this line and check if all going in right way :)
	// QThread::sleep(10);
	ASYNC_CALL(this, "onThreadStart");
	exec();

	// if accumulator and emmiter was moved to this thread
	// ( it can be false in case when onThreadStart was not called
	// onThreadStart can not be called in case when logger immediatly destroyed )
	if (m_logAcumulator->thread() != thread()) {
		// process all posted log events
		QCoreApplication::sendPostedEvents(m_logAcumulator, LogEvent::logType);
		m_logEmitter->stop();
		// move back to main thread
		m_logAcumulator->moveToThread(QCoreApplication::instance()->thread());
		m_logEmitter->moveToThread(QCoreApplication::instance()->thread());
	}
}

LogAccumulator::LogAccumulator()
{
}

LogAccumulator::~LogAccumulator()
{
}

bool LogAccumulator::event(QEvent *event)
{
	if (event->type() == LogEvent::logType) {
		LogEvent *ev = (LogEvent *) event;
		m_log.append(*ev);
		event->accept();
		return true;
	}

	return QObject::event(event);
}

const QList<LogEvent> LogAccumulator::popLogs()
{
	QList<LogEvent> res = m_log;
	m_log.clear();
	return res;
}

LogsEmitter::LogsEmitter(LogAccumulator *a, const QString &folder)
	: QObject()
	, m_exportTimer(nullptr)
	, m_counter(0)
	, m_logAcumulator(a)
	, m_lastLogDate(QDate::currentDate())
	, m_folder(folder)
{
	logQtTypeMap.insert(QtMsgType::QtDebugMsg, QString("Debug"));
	logQtTypeMap.insert(QtMsgType::QtWarningMsg, QString("Warning"));
	logQtTypeMap.insert(QtMsgType::QtCriticalMsg, QString("Critical"));
	logQtTypeMap.insert(QtMsgType::QtFatalMsg, QString("Fatal"));
#ifdef HAVE_QT5
	logQtTypeMap.insert(QtMsgType::QtInfoMsg, QString("Info"));
#endif

	checkAndRotateLogs();
}

LogsEmitter::~LogsEmitter()
{
}

void LogsEmitter::start()
{
	m_counter = 0;
	m_exportTimer = new QTimer;
	connect(m_exportTimer, SIGNAL(timeout()), SLOT(exportTick()));
	m_exportTimer->start(1000);
}

void LogsEmitter::stop()
{
	m_exportTimer->stop();
	delete m_exportTimer;
}

void LogsEmitter::flush()
{
	exportTick();
}

void LogsEmitter::exportTick()
{
	const QList<LogEvent> logs = m_logAcumulator->popLogs();

	if (logs.size() > 0) {
		writeToFile(logs);
		LogsType logExport = convertForExport(logs);
		QMap<QString, LogsType> subtypes;

		for (int i = 0 ; i < logExport.size() ; i++) {
			LogsType &list = subtypes[logExport[i].m_type];
			list << logExport[i];
		}

		subtypes.insert("all", logExport);
		emit exportLogs(subtypes);
	}
}

LogsType LogsEmitter::convertForExport(const QList<LogEvent> &logs)
{
	LogsType result;

	for (const LogEvent &logEvent: logs) {
		QString message = logEvent.getMessage();
		QtMsgType type = logEvent.getType();
		QString subType;

		if (message.startsWith("[")) {
			int ind = message.indexOf("] ");

			if (ind != -1) {
				subType = message.mid(1, ind - 1);
				message = message.mid(ind + 2);
			}
		}

		if (subType.isEmpty())
			subType = "other";

		if (message.length() > MAX_MESSAGE_LENGTH) {
			message = message.left(MAX_MESSAGE_LENGTH / 2) + " ... " +
					  message.right(MAX_MESSAGE_LENGTH / 2 - 5);
		}

		LogEntity entity;
		entity.m_date = logEvent.getTimeStamp().toString("HH:mm:ss.zzz");
		entity.m_message = message;
		entity.m_qtType = type;
		entity.m_type = subType;

		result.append(entity);
	}
	return result;
}


void LogsEmitter::writeToFile(const QList<LogEvent> &logs)
{
	QMap<QString, QFile*> openedLogFiles;

	for (int i = 0; i < logs.size(); i++)
	{
		QtMsgType type = logs[i].getType();
		QString name;

		name = m_folder + '/' + logQtTypeMap.value(type) + ".log";
		QFile *currentFile = nullptr;
		const QDateTime dateTime = logs[i].getTimeStamp();
		if (dateTime.date() > m_lastLogDate) {
			rotateLogs();
			m_lastLogDate = dateTime.date();
		}

		if (openedLogFiles.contains(name)) {
			currentFile = openedLogFiles.value(name);
		}
		else {
			currentFile = new QFile(name);

			if (currentFile->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {
				openedLogFiles.insert(name, currentFile);
			}
			else {
				QString error = QString(tr("Can't open log file %1<br/>")).arg(QDir::toNativeSeparators(name)) + currentFile->errorString();
				new ErrorWindowWrapper(error);
				delete currentFile;
				currentFile = nullptr;
			}
		}

		if (currentFile != nullptr) {
			QTextStream out(currentFile);
			QString niceMessage = logs[i].getMessage();
			if (niceMessage.contains("\n")) {
				int pos = 0;
				if (niceMessage.startsWith("[")) {
					pos = niceMessage.indexOf("]") + 1;
				}

				niceMessage.insert(pos, "\n");
				niceMessage.replace("\n", "\n  ");
			}

			out << "["
				<< dateTime.toString("dd.MM.yyyy HH:mm:ss.zzz")
				<< "] ["
				<< logs[i].getThread()
				<< "] ["
				<< m_counter++
				<< "] "
				<< niceMessage
				<< endl;
			if (out.status() == QTextStream::Status::WriteFailed) {
				QString error = QString(tr("Can't write to log file %1<br/>")).arg(QDir::toNativeSeparators(name)) + currentFile->errorString();
				new ErrorWindowWrapper(error);
			}
		}
	}

	for (QFile *file: openedLogFiles.values()) {
		file->close();
		delete file;
	}
}

void LogsEmitter::rotateLogs(int days) const
{
	for (const QString &logType: logQtTypeMap) {
		for (int i = MAX_LOGS; i >= 0; i--) {
			const QString suffix = i ? QString("_%1").arg(i) : QString();
			const QString name = m_folder + "/" + logType + suffix + ".log";
			if (i + days > MAX_LOGS) {
				// Remove old logs
				if (QFile::exists(name))
					QFile::remove(name);
			}
			else {
				// Add days to logs age
				const QString newSuffix = QString("_%1").arg(i + days);
				const QString newName = m_folder + "/" + logType + newSuffix + ".log";
				QFile::rename(name, newName);
			}
		}
	}
}

void LogsEmitter::checkAndRotateLogs() const
{
	for (const QString &logType: logQtTypeMap) {
		// Try to find any existing logfile
		const QString name = m_folder + "/" + logType + ".log";
		if (!QFile::exists(name))
			continue;

		QFile file(name);
		file.open(QIODevice::ReadOnly);
		// NOTE fix this if logformat will be changed
		file.seek(1);
		const QString dateString = file.read(10);
		file.close();

		const QDate date = QDate::fromString(dateString, "dd.MM.yyyy");
		const int days = date.daysTo(QDate::currentDate());

		if (days)
			rotateLogs(days);

		break;
	}
}

ErrorWindowWrapper::ErrorWindowWrapper(const QString &error)
	: QObject()
{
	moveToThread(QCoreApplication::instance()->thread());
	m_error = error;
	QMetaObject::invokeMethod(this, "showError");
}

void ErrorWindowWrapper::showError()
{
	static bool allreadyShowed = false;
	// To prevent multiple windows show only once
	if (allreadyShowed)
		return;

	allreadyShowed = true;
	QMessageBox messageBox;
	messageBox.setIcon(QMessageBox::Icon::Warning);
	messageBox.setText(tr("Warning"));
	messageBox.setInformativeText(m_error);
#ifdef HAVE_QT5
	messageBox.setTextInteractionFlags(Qt::TextSelectableByMouse);
#endif
	messageBox.exec();
	deleteLater();
}

#ifdef HAVE_QT5
static void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &message)
#else
static void messageHandler(QtMsgType type, const char *messageChat)
#endif
{
#ifndef HAVE_QT5
	QString message = QString::fromUtf8(messageChat);
#endif

	if (type == QtMsgType::QtCriticalMsg) {
		// workaround for annoying warning https://bugreports.qt-project.org/browse/QTBUG-29716
		if (message.indexOf("BitBlt failed") != -1)
			return;
	}
	else if (type == QtMsgType::QtWarningMsg) {
		if (message == "libpng warning: iCCP: known incorrect sRGB profile")
			return;
	}

#ifdef STDOUT_DEBUG_ON
	{
		QMutexLocker locker(&g_stdoutMutex);
		std::string logtypeStd = logQtTypeMap.value(type).toStdString();
		std::string dateStd = QDateTime::currentDateTime().toString("HH:mm:ss.zzz").toStdString();
		std::string msgStd = message.toStdString();
		fprintf(stdout, "[%s] (%s) - %s\n",
				qPrintable(logtypeStd.c_str()),
				qPrintable(dateStd.c_str()),
				qPrintable(msgStd.c_str()));
		fflush(stdout);
	}
#endif

#ifdef HAVE_QT5
	QCoreApplication::postEvent(g_forDebugHandler, new LogEvent(type, context, message));
#else
	QCoreApplication::postEvent(g_forDebugHandler, new LogEvent(type, message));
#endif

	// Write logs on disk before exit
	if (type == QtMsgType::QtFatalMsg) {
		delete g_forDebugHandler->thread();
	}
}

} // namespace logger
