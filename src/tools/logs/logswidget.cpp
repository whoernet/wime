/*
 * logswidget.cpp
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "logswidget.h"
#include "debugmessagehandler.h"
#include <ui_logswidget.h>

#include <QDebug>
#include <QDesktopWidget>

namespace logger
{

LogsWidget::LogsWidget(QObject *logger, QWidget *parent)
	: QWidget(parent)
	, _ui(new Ui::LogsWidget)
{
	_ui->setupUi(this);

	if (logger) {
		LogThread *thread = qobject_cast<LogThread*> (logger);

		if (thread) {
			connect(&thread->getEmitter(), SIGNAL(exportLogs(const logger::LogsMappedType&)), SLOT(importLogs(const logger::LogsMappedType&)));
		}
		else {
			qCritical() << "Error: logger is not of class LogThread" << endl;
		}
	}
	else {
		qCritical() << "Error: LogsWidget logger is NULL" << endl;
	}

	setGeometry(QStyle::alignedRect(
					Qt::LeftToRight,
					Qt::AlignCenter,
					size(),
					qApp->desktop()->availableGeometry()));
}

LogsWidget::~LogsWidget()
{
	delete _ui;
}

void LogsWidget::changeEvent(QEvent *event)
{
	if (event->type() == QEvent::LanguageChange) {
		_ui->retranslateUi(this);
	}

	QWidget::changeEvent(event);
}

void LogsWidget::importLogs(const logger::LogsMappedType &subtypes)
{
	for (const QString &subtype: subtypes.keys()) {
		if (!_logWidgets.contains(subtype)) {
			LogWidget *logWidget = new LogWidget(this, subtype == "all", _ui->logsTabWidget);
			_ui->logsTabWidget->addTab(logWidget, subtype);
			_logWidgets.insert(subtype, logWidget);
		}

		_logWidgets.value(subtype)->handleAppend(subtypes.value(subtype));
	}
}

void LogsWidget::setColor(const QString &type, const QColor &color)
{
	_subtypesColor[type] = color;
}

QMap<QString, QColor> LogsWidget::colors() const {
	return _subtypesColor;
}

} // namespace logger
