/*
 * logmenu.cpp
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#include "logmenu.h"
#include "logmodel.h"

#include <QActionEvent>
#include <QApplication>
#include <QClipboard>
#include <QSortFilterProxyModel>

namespace logger
{

LogMenu::LogMenu(LogTableModel *model, QSortFilterProxyModel *proxyModel, QWidget *parent)
	: QMenu(parent)
	, m_model(model)
	, m_proxyModel(proxyModel)
{
	initialize();
}

LogMenu::~LogMenu()
{
}

void LogMenu::initialize()
{
	m_clearAction = addAction("");
	m_copyAction = addAction("");
	addSeparator();
	m_infoAction = addAction("");
	m_debugAction = addAction("");
	m_warningAction = addAction("");
	m_criticalAction = addAction("");
	m_fatalAction = addAction("");

	m_infoAction->setCheckable(true);
	m_debugAction->setCheckable(true);
	m_warningAction->setCheckable(true);
	m_criticalAction->setCheckable(true);
	m_fatalAction->setCheckable(true);

	connect(m_infoAction, SIGNAL(triggered()), SLOT(msgFilterTriggered()));
	connect(m_debugAction, SIGNAL(triggered()), SLOT(msgFilterTriggered()));
	connect(m_warningAction, SIGNAL(triggered()), SLOT(msgFilterTriggered()));
	connect(m_criticalAction, SIGNAL(triggered()), SLOT(msgFilterTriggered()));
	connect(m_fatalAction, SIGNAL(triggered()), SLOT(msgFilterTriggered()));
	connect(m_clearAction, SIGNAL(triggered()), SLOT(clearTriggered()));
	connect(m_copyAction, SIGNAL(triggered()), SLOT(copyTriggered()));

	retranslateUi();

	// Not used atm
	m_infoAction->setVisible(false);
}

void LogMenu::show(const QPoint &pos, const QString &msg)
{
	m_message = msg;

	if (m_message.isEmpty())
		m_copyAction->setEnabled(false);
	else
		m_copyAction->setEnabled(true);

	exec(pos);
}

void LogMenu::changeEvent(QEvent *event)
{
	if (event->type() == QEvent::LanguageChange) {
		retranslateUi();
	}

	QMenu::changeEvent(event);
}

void LogMenu::msgFilterTriggered()
{
	m_proxyModel->setFilterRegExp(QRegExp(getCurFilterRegExp()));
}

void LogMenu::clearTriggered()
{
	m_model->clear();
}

void LogMenu::copyTriggered()
{
	QApplication::clipboard()->setText(m_message);
}

QString LogMenu::getCurFilterRegExp() const
{
	QString regexp = "";

	if (m_fatalAction->isChecked())
		regexp += "F";

	if (m_debugAction->isChecked()) {
		if (!regexp.isEmpty())
			regexp += "|";

		regexp += "D";
	}

	if (m_warningAction->isChecked()) {
		if (!regexp.isEmpty())
			regexp += "|";

		regexp += "W";
	}

	if (m_criticalAction->isChecked()) {
		if (!regexp.isEmpty())
			regexp += "|";

		regexp += "C";
	}

	if (m_infoAction->isChecked()) {
		if (!regexp.isEmpty())
			regexp += "|";

		regexp += "I";
	}

	return regexp;
}

void LogMenu::retranslateUi()
{
	m_clearAction->setText(tr("Clear"));
	m_copyAction->setText(tr("Copy"));
	m_infoAction->setText(tr("Info"));
	m_debugAction->setText(tr("Debug"));
	m_warningAction->setText(tr("Warning"));
	m_criticalAction->setText(tr("Critical"));
	m_fatalAction->setText(tr("Fatal"));
}

} // namespace logger
