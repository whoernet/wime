/*
 * debugmessagehandler.h
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#pragma once

#include "logevent.h"

#include <QMetaType>
#include <QtGlobal>
#include <QThread>
#include <QMap>
#include <QList>
#include <QPair>
#include <QTimer>
#include <QDate>
#include <QMutex>

namespace logger
{

struct LogEntity {
	QtMsgType m_qtType;
	QString m_type;
	QString m_date;
	QString m_message;
};

typedef QList<logger::LogEntity> LogsType;
typedef QMap<QString, logger::LogsType> LogsMappedType;
// maps qt type to string 'Debug', 'Warning' etc.
extern QMap<QtMsgType, QString> logQtTypeMap;

class LogAccumulator : public QObject
{
	Q_OBJECT

public:
	LogAccumulator();
	virtual ~LogAccumulator();
	const QList<LogEvent> popLogs();

protected:
	bool event(QEvent *event);

private:
	QList<LogEvent> m_log;
};

class LogsEmitter : public QObject
{
	Q_OBJECT

public:
	LogsEmitter(LogAccumulator *a, const QString &folder);
	virtual ~LogsEmitter();
	void flush();
	void stop();

signals:
	void exportLogs(const logger::LogsMappedType &logs);

private slots:
	void start();

private slots:
	void exportTick();

private:
	LogsType convertForExport(const QList<LogEvent> &);
	void writeToFile(const QList<LogEvent> &);
	void rotateLogs(int days = 1) const;
	void checkAndRotateLogs() const;

	QTimer *m_exportTimer;
	uint m_counter;
	LogAccumulator *m_logAcumulator;
	QDate m_lastLogDate;
	QString m_folder;
};

class LogThread : public QThread
{
	Q_OBJECT

public:
	LogThread(QString folder, QObject *parent = 0);
	virtual ~LogThread();

	LogAccumulator  &getAccumulator();
	LogsEmitter     &getEmitter();

private slots:
	void onThreadStart();

private:
	void run() override;

#ifdef HAVE_QT5
	QtMessageHandler    m_previousHandler;
#else
	QtMsgHandler        m_previousHandler;
#endif
	LogAccumulator      *m_logAcumulator;
	LogsEmitter         *m_logEmitter;
};

class ErrorWindowWrapper : public QObject
{
	Q_OBJECT

public:
	ErrorWindowWrapper(const QString &error);

private slots:
	void showError();

private:
	QString m_error;
};

} // namespace logger

Q_DECLARE_METATYPE(logger::LogsType)
Q_DECLARE_METATYPE(logger::LogsMappedType)
Q_DECLARE_METATYPE(logger::LogEntity)
