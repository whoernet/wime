/*
 * logmenu.h
 * Copyright (C) 2016  Ivan Romanov
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 */

#pragma once

#include <QMenu>

class QSortFilterProxyModel;

namespace logger
{

class LogTableModel;

class LogMenu : public QMenu
{
	Q_OBJECT

public:
	explicit LogMenu(LogTableModel *model, QSortFilterProxyModel *proxyModel, QWidget *parent = nullptr);
	virtual ~LogMenu();
	void show(const QPoint &pos, const QString &msg);

protected:
	void changeEvent(QEvent *event);

private slots:
	void msgFilterTriggered();
	void clearTriggered();
	void copyTriggered();

private:
	void initialize();
	QString getCurFilterRegExp() const;
	void retranslateUi();

	QAction *m_clearAction;
	QAction *m_copyAction;
	QAction *m_infoAction;
	QAction *m_debugAction;
	QAction *m_warningAction;
	QAction *m_criticalAction;
	QAction *m_fatalAction;
	QString m_message;
	LogTableModel *m_model;
	QSortFilterProxyModel *m_proxyModel;
};

}
