#include "xmlhighlighter.h"

XmlHighlighter::XmlHighlighter(QObject *parent)
	: QSyntaxHighlighter(parent)
{
	setRegexes();
	setFormats();
}

XmlHighlighter::XmlHighlighter(QTextDocument *parent)
	: QSyntaxHighlighter(parent)
{
	setRegexes();
	setFormats();
}

void XmlHighlighter::highlightBlock(const QString &text)
{
	// Special treatment for xml element regex as we use captured text to emulate lookbehind
	int xmlElementIndex = _xmlElementRegex.indexIn(text);
	while (xmlElementIndex >= 0) {
		int matchedPos = _xmlElementRegex.pos(1);
		int matchedLength = _xmlElementRegex.cap(1).length();
		setFormat(matchedPos, matchedLength, _xmlElementFormat);

		xmlElementIndex = _xmlElementRegex.indexIn(text, matchedPos + matchedLength);
	}

	// Highlight xml keywords *after* xml elements to fix any occasional / captured into the enclosing element
	typedef QList<QRegExp>::const_iterator Iter;
	Iter xmlKeywordRegexesEnd = _xmlKeywordRegexes.end();
	for(Iter it = _xmlKeywordRegexes.begin(); it != xmlKeywordRegexesEnd; ++it) {
		const QRegExp & regex = *it;
		highlightByRegex(_xmlKeywordFormat, regex, text);
	}

	highlightByRegex(_xmlAttributeFormat, _xmlAttributeRegex, text);
	highlightByRegex(_xmlCommentFormat, _xmlCommentRegex, text);
	highlightByRegex(_xmlValueFormat, _xmlValueRegex, text);
}

void XmlHighlighter::highlightByRegex(const QTextCharFormat &format, const QRegExp &regex, const QString &text)
{
	int index = regex.indexIn(text);

	while (index >= 0) {
		int matchedLength = regex.matchedLength();
		setFormat(index, matchedLength, format);

		index = regex.indexIn(text, index + matchedLength);
	}
}

void XmlHighlighter::setRegexes()
{
	_xmlElementRegex.setPattern("<[\\s]*[/]?[\\s]*([^\\n][-\\w:_.]*)(?=[\\s/>])");
	_xmlAttributeRegex.setPattern("[-\\w:_.]+(?=\\=)");
	_xmlValueRegex.setPattern("\"[^\\n\"]+\"(?=[\\s/>?])");
	_xmlCommentRegex.setPattern("<!--[^\\n]*-->");

	_xmlKeywordRegexes = QList<QRegExp>() << QRegExp("<\\?") << QRegExp("/>")
										  << QRegExp(">") << QRegExp("<") << QRegExp("</")
										  << QRegExp("\\?>");
}

void XmlHighlighter::setFormats()
{
	_xmlKeywordFormat.setForeground(Qt::blue);
	_xmlElementFormat.setForeground(Qt::blue);
	_xmlAttributeFormat.setForeground(QColor(160, 82, 45));
	_xmlValueFormat.setForeground(Qt::darkRed);
	_xmlCommentFormat.setForeground(Qt::gray);
}

