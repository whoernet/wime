#include "tabmanager.h"

#include <QtAlgorithms>
#include <QUuid>

#include "tabdlg.h"
#include "tabbablewidget.h"
#include "groupchatdlg.h"
#include "chatdlg.h"
#include "psioptions.h"

TabManager::TabManager(PsiCon* psiCon, QObject *parent)
	: QObject(parent)
	, psiCon_(psiCon)
	, tabDlgDelegate_(0)
	, userManagement_(true)
	, tabSingles_(true)
	, simplifiedCaption_(false)
{
}

TabManager::~TabManager()
{
	deleteAll();
}

PsiCon* TabManager::psiCon() const
{
	return psiCon_;
}

TabDlg* TabManager::getTabs(QWidget *widget)
{
	QChar kind = tabKind(widget);
	if (preferedTabsetForKind_.contains(kind)) {
		return preferedTabsetForKind_[kind];
	} else {
		return newTabs(widget);
	}
}

QChar TabManager::tabKind(QWidget *widget) {
	QChar retval = 0;

	if (qobject_cast<ChatDlg*> (widget)) {
		retval = 'C';
	} else if (qobject_cast<GCMainDlg*> (widget)) {
		retval = 'M';
	} else {
		qDebug("Checking if widget should be tabbed: Unknown type");
	}
	return retval;
}

bool TabManager::shouldBeTabbed(QWidget *widget)
{
	if (!PsiOptions::instance()->getOption("options.ui.tabs.use-tabs").toBool()) {
		return false;
	}

	QString grouping = PsiOptions::instance()->getOption("options.ui.tabs.grouping").toString();
	if (grouping.contains(tabKind(widget))) {
		return true;
	}
	return false;
}

TabDlg* TabManager::newTabs(QWidget *widget, const QChar &defKind)
{
	QChar kind = defKind.isNull() ? tabKind(widget) : defKind;
	QString group, grouping = PsiOptions::instance()->getOption("options.ui.tabs.grouping").toString();
	foreach(QString g, grouping.split(':')) {
		if (g.contains(kind)) {
			group = g;
			break;
		}
	}

	QString geometryOption = QString("options.ui.tabs.group-state.%1.size").arg(group);

	TabDlg *tab = new TabDlg(this, geometryOption, tabDlgDelegate_);
	tab->setUserManagementEnabled(userManagement_);
	tab->setTabBarShownForSingles(tabSingles_);
	tab->setSimplifiedCaptionEnabled(simplifiedCaption_);
	tabsetToKinds_.insert(tab, group);
	for (int i=0; i < group.length(); i++) {
		QChar k = group.at(i);
		if (!preferedTabsetForKind_.contains(k)) {
			preferedTabsetForKind_.insert(k, tab);
		}
	}
	tabs_.append(tab);
	connect(tab, SIGNAL(destroyed(QObject*)), SLOT(tabDestroyed(QObject*)));
	connect(psiCon_, SIGNAL(emitOptionsUpdate()), tab, SLOT(optionsUpdate()));
	return tab;
}

void TabManager::tabDestroyed(QObject* obj)
{
	Q_ASSERT(tabs_.contains(static_cast<TabDlg*>(obj)));
	tabs_.removeAll(static_cast<TabDlg*>(obj));
	tabsetToKinds_.remove(static_cast<TabDlg*>(obj));
	QMutableMapIterator<QChar, TabDlg*> it(preferedTabsetForKind_);
	while (it.hasNext()) {
		it.next();
		if (preferedTabsetForKind_[it.key()] != obj) continue;
		bool ok = false;
		foreach(TabDlg* tabDlg, tabs_) {
			// currently destroyed tab is removed from the list a few lines above
			if (tabsetToKinds_[tabDlg].contains(it.key())) {
				preferedTabsetForKind_[it.key()] = tabDlg;
				ok = true;
				break;
			}
		}
		if (!ok) it.remove();
	}
}

TabDlg *TabManager::preferredTabsForKind(QChar kind) {
	return preferedTabsetForKind_.value(kind);
}

void TabManager::setPreferredTabsForKind(QChar kind, TabDlg *tab) {
	Q_ASSERT(tabs_.contains(tab));
	preferedTabsetForKind_[kind] = tab;
}

bool TabManager::isChatTabbed(const TabbableWidget* chat) const
{
	foreach(TabDlg* tabDlg, tabs_) {
		if (tabDlg->managesTab(chat)) {
			return true;
		}
	}
	return false;
}

TabDlg* TabManager::getManagingTabs(const TabbableWidget* chat) const
{
	//FIXME: this looks like it could be broken to me (KIS)
	//Does this mean that opening two chats to the same jid will go wrong?
	foreach(TabDlg* tabDlg, tabs_) {
		if (tabDlg->managesTab(chat)) {
			return tabDlg;
		}
	}
	return 0;
}

const QList<TabDlg*>& TabManager::tabSets()
{
	return tabs_;
}

void TabManager::deleteAll()
{
	// element from the list will be deleted in tabDestroyed
	// list must be delete from the end
	for (int i = tabs_.size() - 1; i >= 0; i--) {
		delete tabs_.at(i);
	}
	tabs_.clear();
}

void TabManager::setTabDlgDelegate(TabDlgDelegate *delegate)
{
	tabDlgDelegate_ = delegate;
}

void TabManager::setUserManagementEnabled(bool enabled)
{
	if (userManagement_ == enabled) {
		return;
	}

	userManagement_ = enabled;
	foreach (TabDlg *tab, tabs_) {
		tab->setUserManagementEnabled(enabled);
	}
}

void TabManager::setTabBarShownForSingles(bool enabled)
{
	if (tabSingles_ == enabled) {
		return;
	}

	tabSingles_ = enabled;
	foreach (TabDlg *tab, tabs_) {
		tab->setTabBarShownForSingles(enabled);
	}
}

void TabManager::setSimplifiedCaptionEnabled(bool enabled)
{
	if (simplifiedCaption_ == enabled) {
		return;
	}

	simplifiedCaption_ = enabled;
	foreach (TabDlg *tab, tabs_) {
		tab->setSimplifiedCaptionEnabled(enabled);
	}
}

void TabManager::loadTabs()
{
	if (!PsiOptions::instance()->getOption("options.ui.tabs.save-on-quit").toBool()) {
		return;
	}

	QVariantList keys = PsiOptions::instance()->mapKeyList("options.ui.save.tabs-state");

	foreach (const QVariant &key, keys) {
		QString keyStr = key.toString();
		if (keyStr.isEmpty())
			continue;

		QString kindStr = PsiOptions::instance()->mapGet("options.ui.save.tabs-state", keyStr, "kind").toString();
		QChar kind = (kindStr.size() == 1) ? kindStr.at(0) : 'C';
		bool allInOne = PsiOptions::instance()->getOption("options.ui.tabs.grouping").toString().contains('A');

		// Do not create new tab window for all in one window mode
		TabDlg *tabDlg = allInOne ? preferedTabsetForKind_['C'] : newTabs(0, kind);
		tabDlg->loadTab(keyStr);
		QRect rect = PsiOptions::instance()->mapGet("options.ui.save.tabs-state", keyStr, "rect").toRect();
		if (!allInOne && !rect.isNull()) {
			tabDlg->move(rect.topLeft());
			tabDlg->resize(rect.size());
		}
	}
}

void TabManager::saveTabs()
{
	if (!PsiOptions::instance()->getOption("options.ui.tabs.save-on-quit").toBool()) {
		return;
	}

	PsiOptions::instance()->removeOption("options.ui.save.tabs-state", true);

	foreach (TabDlg *widget, tabs_) {
		QString key = QUuid::createUuid().toString();
		QChar kind = preferedTabsetForKind_.key(widget, 'C');
		PsiOptions::instance()->mapPut("options.ui.save.tabs-state", key, "kind", QString(kind));
		QRect rect(widget->pos(), widget->size());
		PsiOptions::instance()->mapPut("options.ui.save.tabs-state", key, "rect", rect);
		widget->saveTab(key);
	}
}
