#!/usr/bin/bash

dest=src/emoji.h

wget -O emoji-data.txt http://www.unicode.org/Public/emoji/3.0/emoji-data.txt
wget -O emoji-sequences.txt http://www.unicode.org/Public/emoji/3.0/emoji-sequences.txt
wget -O emoji-zwj-sequences.txt http://www.unicode.org/Public/emoji/3.0/emoji-zwj-sequences.txt

sed -i '/^[^A-F0-9]/d' emoji-data.txt emoji-sequences.txt emoji-zwj-sequences.txt
sed -i '/^$/d' emoji-data.txt emoji-sequences.txt emoji-zwj-sequences.txt

echo '#pragma once' > $dest
echo '' >> $dest

echo '// Generated with get-emoji-data.sh' >> $dest
echo '' >> $dest

echo '#define VARIATION_SELECTOR_15 0xfe0e' >> $dest
echo '#define VARIATION_SELECTOR_16 0xfe0f' >> $dest
echo '#define COMBINING_ENCLOSING_KEYCAP 0x20e3' >> $dest
echo '#define ZERO_WIDTH_JOINER 0x200d' >> $dest
echo '#define EMOJI_MODIFIER_FITZPATRICK_TYPE_1_2 0x1f3fb' >> $dest
echo '#define EMOJI_MODIFIER_FITZPATRICK_TYPE_3 0x1f3fc' >> $dest
echo '#define EMOJI_MODIFIER_FITZPATRICK_TYPE_4 0x1f3fd' >> $dest
echo '#define EMOJI_MODIFIER_FITZPATRICK_TYPE_5 0x1f3fe' >> $dest
echo '#define EMOJI_MODIFIER_FITZPATRICK_TYPE_6 0x1f3ff' >> $dest
echo '' >> $dest

#########
# Emoji #
#########

echo 'const uint emoji[][2] = {' >> $dest

cat emoji-data.txt \
    | grep -e '^[0-9A-F].*;\W*Emoji\W*#' \
    | cut -d' ' -f 1 \
    | sed 's/^\([0-9A-F]*\)$/\1..\1/' \
    | sed 's/\.\./, 0x/' \
    | sed 's/^\(.*\)$/\t{0x\1},/' \
    | tr '[:upper:]' '[:lower:]' \
         >> $dest

echo -e '\t{0x00000, 0x00000}' >> $dest
echo '};' >> $dest
echo '' >> $dest

######################
# Emoji_Presentation #
######################

echo 'const uint emojiPresentation[][2] = {' >> $dest

cat emoji-data.txt \
    | grep -e '^[0-9A-F].*;\W*Emoji_Presentation\W*#' \
    | cut -d' ' -f 1 \
    | sed 's/^\([0-9A-F]*\)$/\1..\1/' \
    | sed 's/\.\./, 0x/' \
    | sed 's/^\(.*\)$/\t{0x\1},/' \
    | tr '[:upper:]' '[:lower:]' \
         >> $dest

echo -e '\t{0x00000, 0x00000}' >> $dest
echo '};' >> $dest
echo '' >> $dest

#######################
# Emoji_Modifier_Base #
#######################

echo 'const uint emojiModifierBase[][2] = {' >> $dest

cat emoji-data.txt \
    | grep -e '^[0-9A-F].*;\W*Emoji_Modifier_Base\W*#' \
    | cut -d' ' -f 1 \
    | sed 's/^\([0-9A-F]*\)$/\1..\1/' \
    | sed 's/\.\./, 0x/' \
    | sed 's/^\(.*\)$/\t{0x\1},/' \
    | tr '[:upper:]' '[:lower:]' \
         >> $dest

echo -e '\t{0x00000, 0x00000}' >> $dest
echo '};' >> $dest
echo '' >> $dest

############################
# Emoji_Combining_Sequence #
############################

echo 'const uint emojiCombiningSequence[][3] = {' >> $dest

cat emoji-sequences.txt \
    | grep -e '^[0-9A-F].*;\W*Emoji_Combining_Sequence\W*#' \
    | cut -d';' -f 1 \
    | sed 's/ /, 0x/g' \
    | sed 's/^\(.*\)$/\t{0x\1},/' \
    | tr '[:upper:]' '[:lower:]' \
         >> $dest

echo -e '\t{0x0000, 0x0000, 0x0000}' >> $dest
echo '};' >> $dest
echo '' >> $dest

#######################
# Emoji_Flag_Sequence #
#######################

echo 'const uint emojiFlagSequence[][2] = {' >> $dest

cat emoji-sequences.txt \
    | grep -e '^[0-9A-F].*;\W*Emoji_Flag_Sequence\W*#' \
    | sed 's/[ \t]*;.*$//' \
    | sed 's/ /, 0x/g' \
    | sed 's/^\(.*\)$/\t{0x\1},/' \
    | tr '[:upper:]' '[:lower:]' \
         >> $dest

echo -e '\t{0x00000, 0x00000}' >> $dest
echo '};' >> $dest
echo '' >> $dest

############################
# Emoji_Modifier_Sequence #
############################

echo 'const uint emojiModifierSequence[][2] = {' >> $dest

cat emoji-sequences.txt \
    | grep -e '^[0-9A-F].*;\W*Emoji_Modifier_Sequence\W*#' \
    | sed 's/[ \t]*;.*$//' \
    | sed 's/ /, 0x/g' \
    | sed 's/^\(.*\)$/\t{0x\1},/' \
    | tr '[:upper:]' '[:lower:]' \
         >> $dest

echo -e '\t{0x00000, 0x00000}' >> $dest
echo '};' >> $dest
echo '' >> $dest

######################
# Emoji_ZWJ_Sequence #
######################

echo 'const uint emojiZwjSequence[][9] = {' >> $dest

cat emoji-zwj-sequences.txt \
    | grep -e '^[0-9A-F].*;\W*Emoji_ZWJ_Sequence\W*#' \
    | sed 's/[ \t]*;.*$//' \
    | sed 's/ /, 0x/g' \
    | sed 's/^\(.*\)$/\t{0x\1, 0},/' \
    | tr '[:upper:]' '[:lower:]' \
         >> $dest

echo -e '\t{0x00000}' >> $dest
echo '};' >> $dest
echo '' >> $dest
